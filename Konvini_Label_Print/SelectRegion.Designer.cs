﻿namespace Konvini_Label_Print
{
    partial class SelectRegion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectRegion));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditRegion = new DevExpress.XtraEditors.LookUpEdit();
            this.chkSelectServer = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditRegion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectServer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(42, 29);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "지역";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(10, 71);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(200, 41);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "선택";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lookUpEditRegion
            // 
            this.lookUpEditRegion.Location = new System.Drawing.Point(71, 12);
            this.lookUpEditRegion.Name = "lookUpEditRegion";
            this.lookUpEditRegion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditRegion.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditRegion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditRegion.Properties.DropDownRows = 2;
            this.lookUpEditRegion.Properties.NullText = "";
            this.lookUpEditRegion.Size = new System.Drawing.Size(136, 30);
            this.lookUpEditRegion.TabIndex = 110;
            // 
            // chkSelectServer
            // 
            this.chkSelectServer.EditValue = "prod";
            this.chkSelectServer.Location = new System.Drawing.Point(140, 48);
            this.chkSelectServer.Name = "chkSelectServer";
            this.chkSelectServer.Properties.Caption = "개발서버";
            this.chkSelectServer.Properties.ValueChecked = "dev";
            this.chkSelectServer.Properties.ValueUnchecked = "prod";
            this.chkSelectServer.Size = new System.Drawing.Size(67, 18);
            this.chkSelectServer.TabIndex = 111;
            // 
            // SelectRegion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(219, 124);
            this.Controls.Add(this.chkSelectServer);
            this.Controls.Add(this.lookUpEditRegion);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectRegion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Konvini";
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditRegion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSelectServer.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditRegion;
        private DevExpress.XtraEditors.CheckEdit chkSelectServer;
    }
}