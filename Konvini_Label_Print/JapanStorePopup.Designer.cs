﻿namespace Konvini_Label_Print
{
    partial class JapanStorePopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JapanStorePopup));
            this.txtCopyCnt = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonStore = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonLogo = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtCopyCnt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCopyCnt
            // 
            this.txtCopyCnt.Location = new System.Drawing.Point(187, 12);
            this.txtCopyCnt.Name = "txtCopyCnt";
            this.txtCopyCnt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCopyCnt.Properties.Appearance.Options.UseFont = true;
            this.txtCopyCnt.Properties.Mask.EditMask = "n0";
            this.txtCopyCnt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCopyCnt.Size = new System.Drawing.Size(100, 64);
            this.txtCopyCnt.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(144, 29);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "プリント数量";
            // 
            // simpleButtonStore
            // 
            this.simpleButtonStore.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonStore.Appearance.Options.UseFont = true;
            this.simpleButtonStore.Location = new System.Drawing.Point(12, 82);
            this.simpleButtonStore.Name = "simpleButtonStore";
            this.simpleButtonStore.Size = new System.Drawing.Size(130, 49);
            this.simpleButtonStore.TabIndex = 2;
            this.simpleButtonStore.Text = "価格(有)";
            this.simpleButtonStore.Click += new System.EventHandler(this.simpleButtonPriceY_Click);
            // 
            // simpleButtonLogo
            // 
            this.simpleButtonLogo.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonLogo.Appearance.Options.UseFont = true;
            this.simpleButtonLogo.Location = new System.Drawing.Point(148, 82);
            this.simpleButtonLogo.Name = "simpleButtonLogo";
            this.simpleButtonLogo.Size = new System.Drawing.Size(142, 49);
            this.simpleButtonLogo.TabIndex = 3;
            this.simpleButtonLogo.Text = "価格(無)";
            this.simpleButtonLogo.Click += new System.EventHandler(this.SimpleButtonPriceN_Click);
            // 
            // JapanStorePopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 142);
            this.Controls.Add(this.simpleButtonLogo);
            this.Controls.Add(this.simpleButtonStore);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtCopyCnt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "JapanStorePopup";
            this.Text = "ラベルプリント";
            ((System.ComponentModel.ISupportInitialize)(this.txtCopyCnt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtCopyCnt;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStore;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLogo;
    }
}