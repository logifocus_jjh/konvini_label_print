﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Konvini_Label_Print.Report;
using DevExpress.XtraReports.UI;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Media;
using DevExpress.XtraEditors;
using System.Collections;

namespace Konvini_Label_Print
{
    public partial class JapanPrintForm : DevExpress.XtraEditors.XtraForm
    {
        DataTable packingItemDT = new DataTable();
        bool change_YN = false;
        string barcode = "";
        string brand_goodsnm = "";
        string mixturerate = "";
        int price = 0;
        int tax_price = 0;
        string server = "";

        int copy = 0;
        bool price_yn = false;

        public JapanPrintForm(string pSERVER)
        {
            InitializeComponent();
            server = pSERVER;
            //ClearValues();
        }

        private void PrintFormJapan_Load(object sender, EventArgs e)
        {
            CenterToScreen();
            SetupPrinterSet();

            SetGridViewItemColumns();
            SetGridViewItem2Columns();
            SetGridViewColumns_Box();
            SetGridViewColumns_Box2();
            SetGridViewColumns_Item();
            SetGridViewColumns_Item2();
            //SetGridControlDataSource_Item();

            SetLookupEditSelect();
            SetLookupEditBox();

            SetGridviewData_Box();

            dateEdit.DateTime = DateTime.Now;
            dateEditBox.DateTime = DateTime.Now;
            dateEditBox2.DateTime = DateTime.Now;

            SetSearchWeek();


            this.WindowState = FormWindowState.Maximized;

            this.txtBarcode.Focus();
        }

        private void SetLookupEditSelect()
        {
            DataTable DT = new DataTable();

            DT.Columns.Add("CODE");
            DT.Columns.Add("CODE_NAME");

            DataRow DR = DT.NewRow();

            DR["CODE"] = "パッキン移動";
            DR["CODE_NAME"] = "パッキン移動";

            DT.Rows.Add(DR);
            DataRow DR2 = DT.NewRow();

            DR2["CODE"] = "パッキンキャンセル";
            DR2["CODE_NAME"] = "パッキンキャンセル";

            DT.Rows.Add(DR2);

            Common.Method.SetLookupEdit(lookUpEditSelect, DT);
        }

        private void SetLookupEditBox()
        {
            DataTable DT = new DataTable();

            DT.Columns.Add("CODE");
            DT.Columns.Add("CODE_NAME");

            for(int i = 1; 100 >= i; i++)
            {
                DataRow DR = DT.NewRow();

                DR["CODE"] = Convert.ToString(i);
                DR["CODE_NAME"] = Convert.ToString(i);

                DT.Rows.Add(DR);
            }


            Common.Method.SetLookupEdit(lookUpEditBox, DT);
        }

        private void SetGridControlDataSource_Item()
        {
            if(gridViewPackingItem.RowCount == 0)
            {
                packingItemDT.Columns.Clear();

                packingItemDT.Columns.Add("goodsno");
                packingItemDT.Columns.Add("goodsnm");
                packingItemDT.Columns.Add("optno");
                packingItemDT.Columns.Add("barcode");
                packingItemDT.Columns.Add("color");
                packingItemDT.Columns.Add("size");
                packingItemDT.Columns.Add("STOCK");
                packingItemDT.Columns.Add("BOX_PC");

                //lblBoxItemCnt.Text = "合計数量 : 0";
                gridControlPackingItem.DataSource = packingItemDT;
            }
        }

        private void SetSearchWeek()
        {
            string date = Common.Method.getStr(dateEdit.DateTime).Substring(0, 7).Replace("/", "-");

            MySqlParameter[] param =
            {
                new MySqlParameter("@pDATE", date)
            };

            DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_CURRENT_WEEK2", param, server);

            Common.Method.SetLookupEdit(lookUpEditWeek, ResultDT, "jp");
        }

        private void SetSearchWeekBox()
        {
            string date = Common.Method.getStr(dateEditBox.DateTime).Substring(0, 7).Replace("/", "-");

            MySqlParameter[] param =
            {
                new MySqlParameter("@pDATE", date)
            };

            DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_CURRENT_WEEK", param, server);

            Common.Method.SetLookupEdit(lookUpEditWeekBox, ResultDT, "jp");
        }

        private void SetSearchWeekBox2()
        {
            string date = Common.Method.getStr(dateEditBox2.DateTime).Substring(0, 7).Replace("/", "-");

            MySqlParameter[] param =
            {
                new MySqlParameter("@pDATE", date)
            };

            DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_CURRENT_WEEK", param, server);

            Common.Method.SetLookupEdit(lookUpEditWeekBox3, ResultDT, "jp");

            Common.Method.SetLookupEdit(lookUpEditWeekBox2, ResultDT, "jp");
        }

        private void SetGridviewData_Box()
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("no");

            for (int i = 1; 100 >= i; i++)
            {
                DataRow DR = DT.NewRow();

                DR["no"] = Common.Method.getStr(i);

                DT.Rows.Add(DR);
            }

            gridControlBox.DataSource = DT;
        }



        private void ClearValues()
        {
            this.brand_name.Text = "";
            this.size.Text = "";
            this.color.Text = "";
            this.txt_goodsno.Text = "";
        }

        private void SetGridViewItemColumns()
        {
            Common.Method.GridViewRowNumber(gridViewItem);

            //Common.Method.setGridViewColumn(gridViewItem, "image", "이미지", 158, DevExpress.Utils.HorzAlignment.Center, true);
            //Common.Method.setGridViewcolumnImage(gridControlItem, gridViewItem, "image");
            //gridViewItem.Columns["image"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "item_no", "item_no", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem, "barcode", "barcode", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem, "opt_no", "optno", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem, "brand", "ブランド名", 120, DevExpress.Utils.HorzAlignment.Center, true);
            Common.Method.setGridViewColumn(gridViewItem, "goods_no", "商品番号", 60, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem.Columns["goods_no"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "goodsnmjp", "商品名", 300, DevExpress.Utils.HorzAlignment.Near, true);
            gridViewItem.Columns["goodsnmjp"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "goods_nm", "商品名", 300, DevExpress.Utils.HorzAlignment.Near, false);
            gridViewItem.Columns["goods_nm"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "opt1", "カラー", 150, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem.Columns["opt1"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "opt2", "サイズ", 100, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem.Columns["opt2"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "pc", "入庫予定数", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem.Columns["pc"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "qc", "検品数", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem.Columns["qc"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "jp_stock", "일본재고", 80, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewItem.Columns["qc"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem, "status", "状態", 120, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem.Columns["status"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            Common.Method.setGridViewColumn(gridViewItem, "change_status", "상태", 120, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem, "barcode", "바코드", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem, "mixturerate", "바코드", 60, DevExpress.Utils.HorzAlignment.Center, false);

            gridViewItem.OptionsView.ShowFooter = true;

            gridViewItem.Columns["brand"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            gridViewItem.Columns["brand"].SummaryItem.FieldName = "brand";
            gridViewItem.Columns["brand"].SummaryItem.DisplayFormat = "{0:n0}個";

            gridViewItem.Columns["opt2"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "opt2", "入庫予定総数 :");

            gridViewItem.Columns["pc"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewItem.Columns["pc"].SummaryItem.FieldName = "pc";
            gridViewItem.Columns["pc"].SummaryItem.DisplayFormat = "{0:n0}個";

            gridViewItem.Columns["qc"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewItem.Columns["qc"].SummaryItem.FieldName = "qc";
            gridViewItem.Columns["qc"].SummaryItem.DisplayFormat = "{0:n0}個";

            gridViewItem.OptionsBehavior.Editable = false;
            gridViewItem.RowHeight = 80;
        }

        private void SetGridViewItem2Columns()
        {
            Common.Method.GridViewRowNumber(gridViewItem2);

            //Common.Method.setGridViewColumn(gridViewItem2, "image", "이미지", 158, DevExpress.Utils.HorzAlignment.Center, true);
            //Common.Method.setGridViewcolumnImage(gridControlItem, gridViewItem2, "image");
            //gridViewItem2.Columns["image"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "item_no", "item_no", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem2, "barcode", "barcode", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem2, "opt_no", "optno", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem2, "brand", "ブランド", 150, DevExpress.Utils.HorzAlignment.Center, true);
            Common.Method.setGridViewColumn(gridViewItem2, "goodsno", "商品番号", 60, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem2.Columns["goodsno"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "goodsnmjp", "商品名", 300, DevExpress.Utils.HorzAlignment.Near, true);
            gridViewItem2.Columns["goodsnmjp"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "goods_nm", "商品名", 300, DevExpress.Utils.HorzAlignment.Near, false);
            gridViewItem2.Columns["goods_nm"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "opt1", "カラー", 150, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem2.Columns["opt1"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "opt2", "サイズ", 100, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem2.Columns["opt2"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "pc", "입고예정수량", 80, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewItem2.Columns["pc"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "jp_stock", "在庫数", 80, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem2, "qc", "スキャン数", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewItem2.Columns["qc"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            gridViewItem2.Columns["qc"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewItem2, "status", "상태", 120, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewItem2.Columns["status"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            Common.Method.setGridViewColumn(gridViewItem2, "change_status", "상태", 120, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem2, "barcode", "바코드", 60, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewItem2, "mixturerate", "바코드", 60, DevExpress.Utils.HorzAlignment.Center, false);

            gridViewItem2.OptionsView.ShowFooter = true;

            gridViewItem2.Columns["brand"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            gridViewItem2.Columns["brand"].SummaryItem.FieldName = "brand";
            gridViewItem2.Columns["brand"].SummaryItem.DisplayFormat = "{0:n0}개";

            gridViewItem2.Columns["pc"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewItem2.Columns["pc"].SummaryItem.FieldName = "pc";
            gridViewItem2.Columns["pc"].SummaryItem.DisplayFormat = "{0:n0}개";

            gridViewItem2.Columns["qc"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewItem2.Columns["qc"].SummaryItem.FieldName = "qc";
            gridViewItem2.Columns["qc"].SummaryItem.DisplayFormat = "{0:n0}개";

            gridViewItem2.OptionsBehavior.Editable = false;
            gridViewItem2.RowHeight = 80;
        }

        private void SetGridViewColumns_Box()
        {
            Common.Method.setGridViewColumn(gridViewBox, "no", "Box", 20, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewBox.Columns["no"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            gridViewBox.OptionsBehavior.Editable = false;
        }

        private void SetGridViewColumns_Box2()
        {
            Common.Method.setGridViewColumn(gridViewBox2, "box_no", "Box", 20, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewBox2.Columns["box_no"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            gridViewBox2.OptionsBehavior.Editable = false;
        }

        private void SetGridViewColumns_Item()
        {
            Common.Method.GridViewRowNumber(gridViewPackingItem);
            Common.Method.setGridViewColumn(gridViewPackingItem, "brand", "ブランド", 40, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem.Columns["brand"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "goodsno", "商品番号", 40, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem.Columns["goodsno"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "optno", "옵션", 60, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewPackingItem.Columns["optno"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "goodsnm", "商品名", 300, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewPackingItem.Columns["goodsnm"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "goodsnmjp", "商品名", 300, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem.Columns["goodsnmjp"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "color", "カラー", 100, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem.Columns["color"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "size", "サイズ", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem.Columns["size"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "STOCK", "在庫数", 100, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem.Columns["STOCK"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem, "BOX_PC", "スキャン数", 100, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem.Columns["BOX_PC"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            Common.Method.setGridViewColumn(gridViewPackingItem, "barcode", "바코드", 60, DevExpress.Utils.HorzAlignment.Center, false);

            gridViewPackingItem.RowHeight = 80;

            gridViewPackingItem.OptionsView.ShowFooter = true;

            gridViewPackingItem.Columns["STOCK"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewPackingItem.Columns["STOCK"].SummaryItem.FieldName = "STOCK";
            gridViewPackingItem.Columns["STOCK"].SummaryItem.DisplayFormat = "{0:n0}個";

            gridViewPackingItem.Columns["BOX_PC"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            gridViewPackingItem.Columns["BOX_PC"].SummaryItem.FieldName = "BOX_PC";
            gridViewPackingItem.Columns["BOX_PC"].SummaryItem.DisplayFormat = "{0:n0}個";
        }

        private void SetGridViewColumns_Item2()
        {
            Common.Method.GridViewRowNumber(gridViewPackingItem2);
            Common.Method.setGridViewColumn(gridViewPackingItem2, "chk", " ", 8, DevExpress.Utils.HorzAlignment.Center, true);
            Common.Method.setGridViewColumnCheckEdit(gridViewPackingItem2, "chk", "Y", "N");
            Common.Method.setGridViewColumn(gridViewPackingItem2, "box_idx", "box_idx", 40, DevExpress.Utils.HorzAlignment.Center, false);
            Common.Method.setGridViewColumn(gridViewPackingItem2, "goodsno", "商品番号", 40, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem2.Columns["goodsno"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem2, "optno", "옵션", 60, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewPackingItem2.Columns["optno"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem2, "goodsnmjp", "商品名", 300, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem2.Columns["goodsnmjp"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem2, "goodsnm", "商品名", 300, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewPackingItem2.Columns["goodsnm"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem2, "color", "カラー", 100, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem2.Columns["color"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem2, "size", "サイズ", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem2.Columns["size"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewPackingItem2, "box_pc", "数量", 100, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewPackingItem2.Columns["box_pc"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            Common.Method.setGridViewColumn(gridViewPackingItem2, "barcode", "바코드", 60, DevExpress.Utils.HorzAlignment.Center, false);

            gridViewPackingItem2.RowHeight = 80;

            gridViewPackingItem2.OptionsBehavior.Editable = true;
        }

        private string GetMixtureName(string CODENAME, string NATION)
        {
            Hashtable code_Arr = new Hashtable();
            code_Arr["DT"] = "DOTP";
            code_Arr["ABS"] = "ABS樹脂";
            code_Arr["DPM"] = "DPM";
            code_Arr["PVC"] = "PVC";
            code_Arr["TPU"] = "熱可塑性ポリウレタン";
            code_Arr["XPC"] = "X-PAC";
            code_Arr["LT"] = "革";
            code_Arr["STE"] = "スチール";
            code_Arr["RB"] = "ラバー";
            code_Arr["CP"] = "銅";
            code_Arr["GD"] = "グースダウン";
            code_Arr["WU"] = "グアナコ";
            code_Arr["ME"] = "金属繊維";
            code_Arr["PM"] = "金属性ポリエステル";
            code_Arr["AF"] = "その他の繊維";
            code_Arr["NL"] = "ナイロン";
            code_Arr["NK"] = "ニッケル";
            code_Arr["BCH"] = "竹炭";
            code_Arr["BS"] = "竹";
            code_Arr["BY"] = "竹糸";
            code_Arr["MRB"] = "大理石";
            code_Arr["DD"] = "ダックダウン";
            code_Arr["CLO"] = "カメリア油";
            code_Arr["DF"] = "ディフューザー";
            code_Arr["WL"] = "ラマ";
            code_Arr["RA"] = "ラミー";
            code_Arr["CLY"] = "リヨセル";
            code_Arr["LCR"] = "ライクラ";
            code_Arr["LR"] = "ラテックス";
            code_Arr["RY"] = "レーヨン";
            code_Arr["LI"] = "リネン";
            code_Arr["HA"] = "麻";
            code_Arr["MF"] = "マイクロファイバー";
            code_Arr["MSM"] = "MSM";
            code_Arr["MCT"] = "メントールセントラアジアティッカ";
            code_Arr["CO"] = "コットン";
            code_Arr["MO"] = "モーダル";
            code_Arr["WM"] = "モヘア";
            code_Arr["CHC"] = "木炭";
            code_Arr["FSS"] = "発酵シロキサン";
            code_Arr["WC"] = "ホワイトクレイ";
            code_Arr["WV"] = "バージンウール";
            code_Arr["VVT"] = "ベルベット";
            code_Arr["NWV"] = "不織布";
            code_Arr["BR"] = "ブラス/真鍮";
            code_Arr["BD"] = "黒染料";
            code_Arr["VY"] = "ビニール";
            code_Arr["WB"] = "ビーバー";
            code_Arr["VI"] = "ビスコース(レーヨン)";
            code_Arr["BPN"] = "ビスフェノール";
            code_Arr["WG"] = "ビクーニャ";
            code_Arr["SI"] = "サイザル";
            code_Arr["SST"] = "サージカルスチール";
            code_Arr["CL"] = "カウレザー";
            code_Arr["SLT"] = "塩";
            code_Arr["SW"] = "ソイワックス";
            code_Arr["WT"] = "カワウソ";
            code_Arr["SKW"] = "スモーキーウィック";
            code_Arr["SNL"] = "ステンレス";
            code_Arr["SSL"] = "ステンレススチール";
            code_Arr["STL"] = "スチール";
            code_Arr["SM"] = "スチールミラー";
            code_Arr["STW"] = "スチールウッド";
            code_Arr["SP"] = "スパン";
            code_Arr["SB"] = "シアバター";
            code_Arr["ST"] = "シンサレート";
            code_Arr["SLC"] = "シリコン";
            code_Arr["SV"] = "シルバー";
            code_Arr["SE"] = "シルク";
            code_Arr["AKO"] = "アルガンオイル";
            code_Arr["AO"] = "アボカドオイル";
            code_Arr["AC"] = "アセテート";
            code_Arr["ZA"] = "亜鉛合金";
            code_Arr["ACR"] = "アクリル";
            code_Arr["MA"] = "アクリル繊維";
            code_Arr["AL"] = "アルミニウム";
            code_Arr["WP"] = "アルパカ";
            code_Arr["WA"] = "アンゴラ";
            code_Arr["WY"] = "ヤク";
            code_Arr["EPX"] = "エポキシ";
            code_Arr["EL"] = "エラスタン";
            code_Arr["OO"] = "オリーブオイル";
            code_Arr["WOL"] = "ウール";
            code_Arr["SLW"] = "無垢材";
            code_Arr["WN"] = "くるみ";
            code_Arr["GL"] = "ガラス";
            code_Arr["GUV"] = "ガラスUVプリント";
            code_Arr["GM"] = "ガラス鏡";
            code_Arr["ESF"] = "乳化剤";
            code_Arr["SPL"] = "銀メッキ";
            code_Arr["LTT"] = "フェイクレザー";
            code_Arr["MG"] = "磁石";
            code_Arr["RBR"] = "赤銅";
            code_Arr["PW"] = "精製水";
            code_Arr["GS"] = "宝石";
            code_Arr["SL"] = "貝殻";
            code_Arr["PP"] = "ペーパー";
            code_Arr["PRL"] = "パール";
            code_Arr["NP"] = "天然石膏";
            code_Arr["NSW"] = "ナチュラルソイワックス";
            code_Arr["NV"] = "天然バーミキュライトストーン";
            code_Arr["WK"] = "キャメルヘア";
            code_Arr["CC"] = "カカオ";
            code_Arr["CM"] = "カラミン";
            code_Arr["WS"] = "カシミヤ";
            code_Arr["CD"] = "コーデュロイ";
            code_Arr["CRK"] = "コルク";
            code_Arr["HL"] = "コットン/リネンミックス";
            code_Arr["CCT"] = "コンクリート";
            code_Arr["CUP"] = "キュプラ";
            code_Arr["CRT"] = "クリスタル";
            code_Arr["CY"] = "クレイ";
            code_Arr["CLR"] = "クロレラ";
            code_Arr["TRP"] = "テレフタラート";
            code_Arr["TT"] = "テトロン";
            code_Arr["TC"] = "テンセル";
            code_Arr["TS"] = "トライタン";
            code_Arr["TRT"] = "トリタン";
            code_Arr["TA"] = "トリアセテート";
            code_Arr["TO"] = "ティーツリーオイル";
            code_Arr["PCT"] = "パウダーコーティングスチール";
            code_Arr["PX"] = "パームワックス";
            code_Arr["PPO"] = "ペパーミントオイル";
            code_Arr["FT"] = "フェルト";
            code_Arr["PCL"] = "ポリシクロヘキサン-1";
            code_Arr["PPR"] = "ポリプロピレン";
            code_Arr["PA"] = "ポリアミド";
            code_Arr["PAN"] = "ポリアクリル";
            code_Arr["PL"] = "ポリエステル";
            code_Arr["PU"] = "ポリウレタン";
            code_Arr["PCN"] = "ポリカーボネート";
            code_Arr["FGO"] = "フレグランスオイル";
            code_Arr["PPP"] = "プレミアムパールパウダー";
            code_Arr["FO"] = "フラミンゴオイル";
            code_Arr["PST"] = "プラスチック";
            code_Arr["PD"] = "ピンク染料";
            code_Arr["PC"] = "ピンククレイ";
            code_Arr["HJ"] = "韓紙";
            code_Arr["PR"] = "PVCラバー";
            code_Arr["SR"] = "合成樹脂";
            code_Arr["SPC"] = "スパイス";
            code_Arr["PO"] = "パフュームオイル";
            code_Arr["TR"] = "混合繊維";
            code_Arr["HPB"] = "パルファムベース";
            code_Arr["JU"] = "ジュード";
            code_Arr["HAC"] = "ヒアルロン酸";
            code_Arr["PLE"] = "柿の葉抽出物";
            code_Arr["KEX"] = "カオリン抽出物";
            code_Arr["WD"] = "ウッド";
            code_Arr["MTG"] = "メタルゴールド";
            code_Arr["MCI"] = "微生物阻害剤";
            code_Arr["NFE"] = "自然発酵エタノール";
            code_Arr["AR"] = "아크릴 수지";
            code_Arr["TTP"] = "티타늄 포스트";
            code_Arr["FLV"] = "후라보노이드";
            code_Arr["TPE"] = "TPE";
            code_Arr["MC"] = "マットコーティング";
            code_Arr["PRN"] = "パイロン";
            code_Arr["SLH"] = "合成皮革";
            code_Arr["SPX"] = "サプレックス";
            code_Arr["PTN"] = "プラチナ";
            code_Arr["TN"] = "スズ";
            code_Arr["PLT"] = "メッキ";


            string mix_name = "";
            string[] mix_name_tmp;

            try
            {
                mix_name_tmp = CODENAME.Split(new char[] { '^' });

                foreach (string item in mix_name_tmp)
                {
                    string[] tmp = item.Split(new char[] { '_' });
                    mix_name += code_Arr[tmp[0].ToUpper()] + tmp[1] + "%";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return mix_name;
        }

        private string GetCodeName(string CODENAME, string NATION)
        {
            Hashtable code_Arr = new Hashtable();
            code_Arr["CL01001"] = "white";
            code_Arr["CL01002"] = "white × blue";
            code_Arr["CL01003"] = "white × emerald green";
            code_Arr["CL01004"] = "white × gold";
            code_Arr["CL01005"] = "white × gray";
            code_Arr["CL01006"] = "white × green";
            code_Arr["CL01007"] = "white × khaki brown";
            code_Arr["CL01008"] = "white × lime";
            code_Arr["CL01009"] = "white × navy";
            code_Arr["CL01010"] = "white × pink";
            code_Arr["CL01011"] = "white × purple";
            code_Arr["CL01012"] = "white × red";
            code_Arr["CL01013"] = "white × silver";
            code_Arr["CL01014"] = "white × white";
            code_Arr["CL01015"] = "white × yellow";
            code_Arr["CL01016"] = "white gold";
            code_Arr["CL01017"] = "white x black";
            code_Arr["CL01018"] = "off white";
            code_Arr["CL01019"] = "light white";
            code_Arr["CL01020"] = "clear";
            code_Arr["CL02001"] = "black";
            code_Arr["CL02002"] = "black × blue";
            code_Arr["CL02003"] = "black × gold";
            code_Arr["CL02004"] = "black × gray";
            code_Arr["CL02005"] = "black × green";
            code_Arr["CL02006"] = "black × khaki";
            code_Arr["CL02007"] = "black × lime";
            code_Arr["CL02008"] = "black × pink";
            code_Arr["CL02009"] = "black × purple";
            code_Arr["CL02010"] = "black × red";
            code_Arr["CL02011"] = "black × silver";
            code_Arr["CL02012"] = "black × white";
            code_Arr["CL02013"] = "black × yellow";
            code_Arr["CL02014"] = "Black Mix";
            code_Arr["CL02015"] = "black x black";
            code_Arr["CL02016"] = "clear black";
            code_Arr["CL02017"] = "dark black";
            code_Arr["CL02018"] = "light black";
            code_Arr["CL03001"] = "heather gray";
            code_Arr["CL03002"] = "gray";
            code_Arr["CL03003"] = "gray × pink";
            code_Arr["CL03004"] = "gray × saxophone blue";
            code_Arr["CL03005"] = "gray × white";
            code_Arr["CL03006"] = "gray camouflage";
            code_Arr["CL03007"] = "smoke gray";
            code_Arr["CL03008"] = "ash gray";
            code_Arr["CL03009"] = "charcoal";
            code_Arr["CL03010"] = "charcoal ash";
            code_Arr["CL03011"] = "charcoal gray";
            code_Arr["CL03012"] = "charcoal gray × black";
            code_Arr["CL03013"] = "charcoal gray × purple";
            code_Arr["CL03014"] = "dark ash";
            code_Arr["CL03015"] = "heather charcoal";
            code_Arr["CL03016"] = "mix gray";
            code_Arr["CL04001"] = "light brown";
            code_Arr["CL04002"] = "clear brown";
            code_Arr["CL04003"] = "brown";
            code_Arr["CL04004"] = "brown × yellow";
            code_Arr["CL04005"] = "brown camouflage";
            code_Arr["CL04006"] = "dark brown";
            code_Arr["CL04007"] = "ash brown";
            code_Arr["CL04008"] = "terracotta";
            code_Arr["CL04009"] = "wood";
            code_Arr["CL04010"] = "woodland";
            code_Arr["CL04011"] = "brick";
            code_Arr["CL04012"] = "tan";
            code_Arr["CL04013"] = "chocolate";
            code_Arr["CL04014"] = "dark wood";
            code_Arr["CL04015"] = "khaki brown";
            code_Arr["CL04016"] = "mocha";
            code_Arr["CL05001"] = "beige";
            code_Arr["CL05002"] = "ivory";
            code_Arr["CL05003"] = "light beige";
            code_Arr["CL05004"] = "dark beige";
            code_Arr["CL05005"] = "sand beige";
            code_Arr["CL05006"] = "pink beige";
            code_Arr["CL05007"] = "grayish beige";
            code_Arr["CL05008"] = "wheat";
            code_Arr["CL05009"] = "cream";
            code_Arr["CL05010"] = "oatmeal";
            code_Arr["CL06001"] = "green";
            code_Arr["CL06002"] = "clear green";
            code_Arr["CL06003"] = "dark green";
            code_Arr["CL06004"] = "moss green";
            code_Arr["CL06005"] = "light green";
            code_Arr["CL06006"] = "green × orange";
            code_Arr["CL06007"] = "green × pink";
            code_Arr["CL06008"] = "green camouflage";
            code_Arr["CL06009"] = "safari green";
            code_Arr["CL06010"] = "sage green";
            code_Arr["CL06011"] = "real tree duck";
            code_Arr["CL06012"] = "dark khaki";
            code_Arr["CL06013"] = "dark olive";
            code_Arr["CL06014"] = "emerald";
            code_Arr["CL06015"] = "khaki";
            code_Arr["CL06016"] = "khaki olive";
            code_Arr["CL06017"] = "mint";
            code_Arr["CL06018"] = "olive";
            code_Arr["CL06019"] = "olive × black";
            code_Arr["CL06020"] = "olive drag";
            code_Arr["CL07001"] = "blue";
            code_Arr["CL07002"] = "blue gray";
            code_Arr["CL07003"] = "blue green";
            code_Arr["CL07004"] = "blue purple";
            code_Arr["CL07005"] = "bluegrass";
            code_Arr["CL07006"] = "grayish blue";
            code_Arr["CL07007"] = "light indigo blue";
            code_Arr["CL07008"] = "indigo × gold";
            code_Arr["CL07009"] = "indigo × silver";
            code_Arr["CL07010"] = "indigo blue";
            code_Arr["CL07011"] = "sax blue";
            code_Arr["CL07012"] = "light blue";
            code_Arr["CL07013"] = "light blue gray";
            code_Arr["CL07014"] = "dark indigo blue";
            code_Arr["CL07015"] = "clear blue";
            code_Arr["CL07016"] = "sky blue";
            code_Arr["CL07017"] = "cobalt blue";
            code_Arr["CL07018"] = "caribbean blue";
            code_Arr["CL07019"] = "emerald blue";
            code_Arr["CL07020"] = "dark blue";
            code_Arr["CL07021"] = "turquoise blue";
            code_Arr["CL07022"] = "royal blue";
            code_Arr["CL07023"] = "navy";
            code_Arr["CL08001"] = "purple";
            code_Arr["CL08002"] = "dark purple";
            code_Arr["CL08003"] = "light purple";
            code_Arr["CL08004"] = "red purple";
            code_Arr["CL08005"] = "violet";
            code_Arr["CL08006"] = "dark lavender";
            code_Arr["CL08007"] = "lilac";
            code_Arr["CL09001"] = "turmeric";
            code_Arr["CL09002"] = "yellow";
            code_Arr["CL09003"] = "yellow × black";
            code_Arr["CL09004"] = "yellow × gray";
            code_Arr["CL09005"] = "yellow × green";
            code_Arr["CL09006"] = "yellow gold";
            code_Arr["CL09007"] = "clear yellow";
            code_Arr["CL09008"] = "pale yellow";
            code_Arr["CL09009"] = "lemon yellow";
            code_Arr["CL09010"] = "fresh yellow";
            code_Arr["CL09011"] = "light yellow";
            code_Arr["CL09012"] = "salmon pink";
            code_Arr["CL09013"] = "smoked pink";
            code_Arr["CL09014"] = "champagne";
            code_Arr["CL09015"] = "lime";
            code_Arr["CL09016"] = "mustard";
            code_Arr["CL10001"] = "baby pink";
            code_Arr["CL10002"] = "pink × yellow";
            code_Arr["CL10003"] = "pink gold";
            code_Arr["CL10004"] = "plum";
            code_Arr["CL10005"] = "cherry blossoms";
            code_Arr["CL10006"] = "cherry pink";
            code_Arr["CL10007"] = "coral";
            code_Arr["CL10008"] = "coral pink";
            code_Arr["CL10009"] = "fuchsia pink";
            code_Arr["CL10010"] = "magenta";
            code_Arr["CL11001"] = "wine";
            code_Arr["CL11002"] = "wine-red";
            code_Arr["CL11003"] = "bordeaux";
            code_Arr["CL11004"] = "burgundy";
            code_Arr["CL11005"] = "cherry red";
            code_Arr["CL11006"] = "clear red";
            code_Arr["CL11007"] = "maroon";
            code_Arr["CL12001"] = "dark orange";
            code_Arr["CL13001"] = "gun meta";
            code_Arr["CL13002"] = "silver";
            code_Arr["CL13003"] = "silver × black";
            code_Arr["CL13004"] = "silver × black cubic";
            code_Arr["CL13005"] = "silver × blue";
            code_Arr["CL13006"] = "silver × gold";
            code_Arr["CL13007"] = "silver × onyx";
            code_Arr["CL13008"] = "silver × smoky quartz";
            code_Arr["CL13009"] = "silver × white quartz";
            code_Arr["CL13010"] = "silver gray";
            code_Arr["CL13011"] = "silver x cubic";
            code_Arr["CL14001"] = "bronze";
            code_Arr["CL14002"] = "gold";
            code_Arr["CL14003"] = "gold × brown";
            code_Arr["CL14004"] = "gold × gray";
            code_Arr["CL15001"] = "check";
            code_Arr["CL15002"] = "colorful";
            code_Arr["CL15003"] = "dot";
            code_Arr["CL15004"] = "dessert duck";
            code_Arr["CL15005"] = "hound tooth";
            code_Arr["CL15006"] = "leopard";
            code_Arr["CL15007"] = "multi";
            code_Arr["CL15008"] = "rainbow";
            code_Arr["CL15009"] = "stripe";
            code_Arr["CL15010"] = "zebra";
            code_Arr["CL15011"] = "camouflage";
            code_Arr["CL11008"] = "red";
            code_Arr["CL10011"] = "pink";
            code_Arr["CL12002"] = "orange";
            code_Arr["CL04017"] = "camel";
            code_Arr["CL07024"] = "dark navy";
            code_Arr["CL03017"] = "light gray";
            code_Arr["CL03018"] = "dark gray";
            code_Arr["CL06021"] = "light khaki";
            code_Arr["CL15012"] = "natural";
            code_Arr["CL08008"] = "lavender";
            code_Arr["CL10012"] = "salmon pink";
            code_Arr["CL10013"] = "smoked pink";
            code_Arr["CL10014"] = "champagne";
            code_Arr["CL13012"] = "antique silver";
            code_Arr["CL14005"] = "antique gold";
            code_Arr["CL14006"] = "pink gold";
            code_Arr["SZ01001"] = "FREE";
            code_Arr["SZ01002"] = "XX-SMALL";
            code_Arr["SZ01003"] = "X-SMALL";
            code_Arr["SZ01004"] = "SMALL";
            code_Arr["SZ01005"] = "MEDIUM";
            code_Arr["SZ01006"] = "LARGE";
            code_Arr["SZ01007"] = "X-LARGE";
            code_Arr["SZ01008"] = "XX-LARGE";
            code_Arr["SZ01009"] = "XXX-LARGE";
            code_Arr["SZ02001"] = "6";
            code_Arr["SZ02002"] = "7";
            code_Arr["SZ02003"] = "8";
            code_Arr["SZ02004"] = "9";
            code_Arr["SZ02005"] = "10";
            code_Arr["SZ02006"] = "11";
            code_Arr["SZ02007"] = "12";
            code_Arr["SZ02008"] = "13";
            code_Arr["SZ02009"] = "14";
            code_Arr["SZ02010"] = "15";
            code_Arr["SZ02011"] = "16";
            code_Arr["SZ02012"] = "17";
            code_Arr["SZ02013"] = "18";
            code_Arr["SZ02014"] = "19";
            code_Arr["SZ02015"] = "20";
            code_Arr["SZ02016"] = "21";
            code_Arr["SZ02017"] = "22";
            code_Arr["SZ02018"] = "23";
            code_Arr["SZ02019"] = "24";
            code_Arr["SZ02020"] = "25";
            code_Arr["SZ02021"] = "26";
            code_Arr["SZ02022"] = "27";
            code_Arr["SZ02023"] = "28";
            code_Arr["SZ02024"] = "29";
            code_Arr["SZ02025"] = "30";
            code_Arr["SZ02026"] = "31";
            code_Arr["SZ02027"] = "32";
            code_Arr["SZ02028"] = "33";
            code_Arr["SZ02029"] = "34";
            code_Arr["SZ02030"] = "35";
            code_Arr["SZ02031"] = "36";
            code_Arr["SZ02032"] = "37";
            code_Arr["SZ02033"] = "38";
            code_Arr["SZ02034"] = "39";
            code_Arr["SZ02035"] = "40";
            code_Arr["SZ02036"] = "41";
            code_Arr["SZ02037"] = "42";
            code_Arr["SZ02038"] = "43";
            code_Arr["SZ02039"] = "44";
            code_Arr["SZ02040"] = "45";
            code_Arr["SZ02041"] = "46";
            code_Arr["SZ02042"] = "47";
            code_Arr["SZ02043"] = "48";
            code_Arr["SZ02044"] = "49";
            code_Arr["SZ02045"] = "50";
            code_Arr["SZ02046"] = "5.5";
            code_Arr["SZ02047"] = "6.5";
            code_Arr["SZ02048"] = "7.5";
            code_Arr["SZ02049"] = "8.5";
            code_Arr["SZ02050"] = "9.5";
            code_Arr["SZ02051"] = "10.5";
            code_Arr["SZ02052"] = "11.5";
            code_Arr["SZ02053"] = "12.5";
            code_Arr["SZ02054"] = "13.5";
            code_Arr["SZ02055"] = "14.5";
            code_Arr["SZ02056"] = "15.5";
            code_Arr["SZ02057"] = "16.5";
            code_Arr["SZ02058"] = "17.5";
            code_Arr["SZ02059"] = "18.5";
            code_Arr["SZ02060"] = "19.5";
            code_Arr["SZ02061"] = "20.5";
            code_Arr["SZ02062"] = "21.5";
            code_Arr["SZ02063"] = "22.5";
            code_Arr["SZ02064"] = "23.5";
            code_Arr["SZ02065"] = "24.5";
            code_Arr["SZ02066"] = "25.5";
            code_Arr["SZ02067"] = "26.5";
            code_Arr["SZ02068"] = "27.5";
            code_Arr["SZ02069"] = "28.5";
            code_Arr["SZ02070"] = "29.5";
            code_Arr["SZ02071"] = "30.5";
            code_Arr["SZ02072"] = "31.5";
            code_Arr["SZ02073"] = "34.5";
            code_Arr["SZ02074"] = "35.5";
            code_Arr["SZ02075"] = "36.5";
            code_Arr["SZ02076"] = "37.5";
            code_Arr["SZ02077"] = "38.5";
            code_Arr["SZ02078"] = "39.5";
            code_Arr["SZ02079"] = "40.5";
            code_Arr["SZ02080"] = "41.5";
            code_Arr["SZ02081"] = "42.5";
            code_Arr["SZ02082"] = "43.5";
            code_Arr["SZ02083"] = "44.5";
            code_Arr["SZ02084"] = "45.5";
            code_Arr["SZ03001"] = "3inch";
            code_Arr["SZ03002"] = "4inch";
            code_Arr["SZ03003"] = "5inch";
            code_Arr["SZ03004"] = "6inch";
            code_Arr["SZ03005"] = "7inch";
            code_Arr["SZ03006"] = "8inch";
            code_Arr["SZ03007"] = "9inch";
            code_Arr["SZ03008"] = "10inch";
            code_Arr["SZ03009"] = "11inch";
            code_Arr["SZ03010"] = "12inch";
            code_Arr["SZ03011"] = "13inch";
            code_Arr["SZ03012"] = "14inch";
            code_Arr["SZ03013"] = "15inch";
            code_Arr["SZ03014"] = "18inch";
            code_Arr["SZ03015"] = "19inch";
            code_Arr["SZ03016"] = "20inch";
            code_Arr["SZ03017"] = "21inch";
            code_Arr["SZ03018"] = "22inch";
            code_Arr["SZ03019"] = "23inch";
            code_Arr["SZ03020"] = "24inch";
            code_Arr["SZ03021"] = "25inch";
            code_Arr["SZ03022"] = "26inch";
            code_Arr["SZ03023"] = "27inch";
            code_Arr["SZ03024"] = "28inch";
            code_Arr["SZ03025"] = "29inch";
            code_Arr["SZ03026"] = "30inch";
            code_Arr["SZ03027"] = "31inch";
            code_Arr["SZ03028"] = "32inch";
            code_Arr["SZ03029"] = "33inch";
            code_Arr["SZ03030"] = "34inch";
            code_Arr["SZ03031"] = "35inch";
            code_Arr["SZ03032"] = "36inch";
            code_Arr["SZ03033"] = "37inch";
            code_Arr["SZ03034"] = "38inch";
            code_Arr["SZ03035"] = "39inch";
            code_Arr["SZ03036"] = "40inch";
            code_Arr["SZ03037"] = "42inch";
            code_Arr["SZ03038"] = "44inch";
            code_Arr["SZ04001"] = "9cm";
            code_Arr["SZ04002"] = "10cm";
            code_Arr["SZ04003"] = "11cm";
            code_Arr["SZ04004"] = "11.5cm";
            code_Arr["SZ04005"] = "12cm";
            code_Arr["SZ04006"] = "12.5cm";
            code_Arr["SZ04007"] = "13cm";
            code_Arr["SZ04008"] = "13.5cm";
            code_Arr["SZ04009"] = "14cm";
            code_Arr["SZ04010"] = "14.5cm";
            code_Arr["SZ04011"] = "15cm";
            code_Arr["SZ04012"] = "15.5cm";
            code_Arr["SZ04013"] = "16cm";
            code_Arr["SZ04014"] = "16.5cm";
            code_Arr["SZ04015"] = "17cm";
            code_Arr["SZ04016"] = "17.5cm";
            code_Arr["SZ04017"] = "18cm";
            code_Arr["SZ04018"] = "18.5cm";
            code_Arr["SZ04019"] = "19cm";
            code_Arr["SZ04020"] = "19.5cm";
            code_Arr["SZ04021"] = "20cm";
            code_Arr["SZ04022"] = "20.5cm";
            code_Arr["SZ04023"] = "21cm";
            code_Arr["SZ04024"] = "22cm";
            code_Arr["SZ04025"] = "23cm";
            code_Arr["SZ04026"] = "24cm";
            code_Arr["SZ04027"] = "25cm";
            code_Arr["SZ04028"] = "26cm";
            code_Arr["SZ04029"] = "27cm";
            code_Arr["SZ04030"] = "28cm";
            code_Arr["SZ04031"] = "29cm";
            code_Arr["SZ04032"] = "30cm";
            code_Arr["SZ04033"] = "31cm";
            code_Arr["SZ04034"] = "32cm";
            code_Arr["SZ04035"] = "35cm";
            code_Arr["SZ04036"] = "36cm";
            code_Arr["SZ04037"] = "37cm";
            code_Arr["SZ04038"] = "38cm";
            code_Arr["SZ04039"] = "39cm";
            code_Arr["SZ04040"] = "40cm";
            code_Arr["SZ04041"] = "41cm";
            code_Arr["SZ04042"] = "42cm";
            code_Arr["SZ04043"] = "43cm";
            code_Arr["SZ04044"] = "44cm";
            code_Arr["SZ04045"] = "45cm";
            code_Arr["SZ04046"] = "46cm";
            code_Arr["SZ04047"] = "48cm";
            code_Arr["SZ04048"] = "49cm";
            code_Arr["SZ04049"] = "50cm";
            code_Arr["SZ05001"] = "70-80cm";
            code_Arr["SZ05002"] = "70-90cm";
            code_Arr["SZ05003"] = "74-80cm";
            code_Arr["SZ05004"] = "80-85cm";
            code_Arr["SZ05005"] = "80-95cm";
            code_Arr["SZ05006"] = "80-90cm";
            code_Arr["SZ05007"] = "85-95cm";
            code_Arr["SZ05008"] = "85-100cm";
            code_Arr["SZ05009"] = "90-95cm";
            code_Arr["SZ05010"] = "90-110cm";
            code_Arr["SZ05011"] = "95-110cm";
            code_Arr["SZ05012"] = "95-115cm";
            code_Arr["SZ05013"] = "95-120cm";
            code_Arr["SZ05014"] = "100-105cm";
            code_Arr["SZ05015"] = "100-110cm";
            code_Arr["SZ05016"] = "100-115cm";
            code_Arr["SZ05017"] = "105-115cm";
            code_Arr["SZ05018"] = "105-120cm";
            code_Arr["SZ05019"] = "110-115cm";
            code_Arr["SZ05020"] = "110-130cm";
            code_Arr["SZ05021"] = "115-135cm";
            code_Arr["SZ05022"] = "120-125cm";
            code_Arr["SZ05023"] = "120-135cm";
            code_Arr["SZ05024"] = "120-130cm";
            code_Arr["SZ05025"] = "120-140cm";
            code_Arr["SZ05026"] = "125-135cm";
            code_Arr["SZ05027"] = "125-140cm";
            code_Arr["SZ05028"] = "130-135cm";
            code_Arr["SZ05029"] = "140-145cm";
            code_Arr["SZ05030"] = "140-155cm";
            code_Arr["SZ05031"] = "150-155cm";
            code_Arr["SZ05032"] = "150-160cm";
            code_Arr["SZ05033"] = "160-165cm";
            code_Arr["SZ05034"] = "170-175cm";
            code_Arr["SZ06001"] = "90";
            code_Arr["SZ06002"] = "95";
            code_Arr["SZ06003"] = "100";
            code_Arr["SZ06004"] = "105";
            code_Arr["SZ06005"] = "110";
            code_Arr["SZ06006"] = "115";
            code_Arr["SZ06007"] = "120";
            code_Arr["SZ06008"] = "125";
            code_Arr["SZ06009"] = "130";
            code_Arr["SZ06010"] = "135";
            code_Arr["SZ06011"] = "140";
            code_Arr["SZ06012"] = "145";
            code_Arr["SZ06013"] = "150";
            code_Arr["SZ06014"] = "155";
            code_Arr["SZ06015"] = "160";
            code_Arr["SZ06016"] = "165";
            code_Arr["SZ06017"] = "170";
            code_Arr["SZ06018"] = "175";
            code_Arr["SZ06019"] = "185";
            code_Arr["SZ06020"] = "195";
            code_Arr["SZ07001"] = "iphone 5 / 5S / SE";
            code_Arr["SZ07002"] = "iphone 6 / 6S";
            code_Arr["SZ07003"] = "iphone 6+";
            code_Arr["SZ07004"] = "iphone 7/8";
            code_Arr["SZ07005"] = "iphone 7+ / 8+";
            code_Arr["SZ07006"] = "iphone X / XS";
            code_Arr["SZ07007"] = "iphone XR";
            code_Arr["SZ07008"] = "iphone XS MAX";
            code_Arr["SZ08001"] = "A65";
            code_Arr["SZ08002"] = "A70";
            code_Arr["SZ08003"] = "A75";
            code_Arr["SZ08004"] = "A80";
            code_Arr["SZ08005"] = "A85";
            code_Arr["SZ08006"] = "A90";
            code_Arr["SZ08007"] = "A95";
            code_Arr["SZ08008"] = "A100";
            code_Arr["SZ08009"] = "A105";
            code_Arr["SZ08010"] = "B65";
            code_Arr["SZ08011"] = "B70";
            code_Arr["SZ08012"] = "B75";
            code_Arr["SZ08013"] = "B80";
            code_Arr["SZ08014"] = "B85";
            code_Arr["SZ08015"] = "B90";
            code_Arr["SZ08016"] = "B95";
            code_Arr["SZ08017"] = "B100";
            code_Arr["SZ08018"] = "B105";
            code_Arr["SZ08019"] = "C65";
            code_Arr["SZ08020"] = "C70";
            code_Arr["SZ08021"] = "C75";
            code_Arr["SZ08022"] = "C80";
            code_Arr["SZ08023"] = "C85";
            code_Arr["SZ08024"] = "C90";
            code_Arr["SZ08025"] = "C95";
            code_Arr["SZ08026"] = "C100";
            code_Arr["SZ08027"] = "C105";
            code_Arr["SZ08028"] = "C110";
            code_Arr["SZ08029"] = "C120";
            code_Arr["SZ08030"] = "D65";
            code_Arr["SZ08031"] = "D70";
            code_Arr["SZ08032"] = "D75";
            code_Arr["SZ08033"] = "D80";
            code_Arr["SZ08034"] = "D85";
            code_Arr["SZ08035"] = "D90";
            code_Arr["SZ08036"] = "D95";
            code_Arr["SZ08037"] = "D100";
            code_Arr["SZ08038"] = "D110";
            code_Arr["SZ08039"] = "D120";
            code_Arr["SZ08040"] = "E65";
            code_Arr["SZ08041"] = "E70";
            code_Arr["SZ08042"] = "E75";
            code_Arr["SZ08043"] = "E80";
            code_Arr["SZ08044"] = "E85";
            code_Arr["SZ08045"] = "E90";
            code_Arr["SZ08046"] = "E95";
            code_Arr["SZ08047"] = "E100";
            code_Arr["SZ08048"] = "E110";
            code_Arr["SZ08049"] = "E120";
            code_Arr["SZ08050"] = "F65";
            code_Arr["SZ08051"] = "F70";
            code_Arr["SZ08052"] = "F75";
            code_Arr["SZ02085"] = "0";
            code_Arr["SZ02086"] = "-1.5";
            code_Arr["SZ02087"] = "-2";
            code_Arr["SZ02088"] = "-2.5";
            code_Arr["SZ02089"] = "-3";
            code_Arr["SZ02090"] = "-3.5";
            code_Arr["SZ02091"] = "-4";
            code_Arr["SZ02092"] = "-4.5";
            code_Arr["SZ02093"] = "-5";
            code_Arr["SZ02094"] = "-6";
            code_Arr["SZ02095"] = "-7";
            code_Arr["SZ04050"] = "21.5cm";
            code_Arr["SZ04051"] = "22.5cm";
            code_Arr["SZ04052"] = "23.5cm";
            code_Arr["SZ04053"] = "24.5cm";
            code_Arr["SZ04054"] = "25.5cm";
            code_Arr["SZ04055"] = "26.5cm";
            code_Arr["SZ04056"] = "27.5cm";
            code_Arr["SZ04057"] = "28.5cm";
            code_Arr["SZ04058"] = "29.5cm";
            code_Arr["SZ04059"] = "30.5cm";
            code_Arr["SZ04060"] = "31.5cm";
            code_Arr["SZ02096"] = "1";
            code_Arr["SZ02097"] = "2";
            code_Arr["SZ02098"] = "3";
            code_Arr["SZ02099"] = "4";
            code_Arr["SZ02100"] = "5";
            code_Arr["SZ02101"] = "2.5";
            code_Arr["SZ02102"] = "3.5";
            code_Arr["SZ02103"] = "4.5";
            code_Arr["SZ04061"] = "47";
            code_Arr["SZ04062"] = "47cm";
            code_Arr["SZ01010"] = "XXXX-LARGE";
            code_Arr["SZ01011"] = "XXXXX-LARGE";
            code_Arr["SZ07009"] = "air pod case";
            code_Arr["SZ02104"] = "0";
            code_Arr["SZ02105"] = "-1";
            code_Arr["SZ02106"] = "-5.5";
            code_Arr["SZ02107"] = "-6.5";
            code_Arr["SZ04063"] = "51cm";
            code_Arr["SZ04064"] = "52cm";
            code_Arr["SZ04065"] = "53cm";
            code_Arr["SZ04066"] = "54cm";
            code_Arr["SZ04067"] = "55cm";
            code_Arr["SZ04068"] = "56cm";
            code_Arr["SZ04069"] = "57cm";
            code_Arr["SZ04070"] = "58cm";
            code_Arr["SZ04071"] = "59cm";
            code_Arr["SZ04072"] = "60cm";

            string code_name = "";

            try
            {
                code_name = (string)code_Arr[CODENAME];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return code_name;
        }

        private string GetImageNo(string GOODSNO, string OPTNO)
        {
            string imageno = "";

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pGOODSNO", GOODSNO),
                    new MySqlParameter("@pOPTNO", OPTNO)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_IMAGE_NO", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    imageno = Common.Method.getStr(ResultDT.Rows[0]["imgno"]);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return imageno;
        }

        Image GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    Image img = Image.FromStream(responseStream);
                    return img;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText 
                }
                throw;
            }
        }

        private void ReportPrint(string type, int copy = 1)
        {
            string printerName = Common.Method.getStr(lookUpEditPrinter.EditValue);

            if(price_yn)
            {
                Japan_Label_Form rpt = new Japan_Label_Form();
                rpt.PrinterName = printerName;
                rpt.ShowPrintMarginsWarning = false;

                if(type == "barcode")
                {
                    rpt.DataSource = ReportData();
                }
                else
                {
                    rpt.DataSource = ReportData2();
                }

                rpt.Print();
            }
            else
            {
                Label_Form rpt = new Label_Form();
                rpt.PrinterName = printerName;
                rpt.ShowPrintMarginsWarning = false;

                if (type == "barcode")
                {
                    rpt.DataSource = ReportData3();
                }
                else
                {
                    rpt.DataSource = ReportData4();
                }

                rpt.Print();
            }
        }

        private List<Japan_Label_Form_Data> ReportData()
        {
            Japan_Label_Form_Data item = null;
            List<Japan_Label_Form_Data> data = new List<Japan_Label_Form_Data>();

            item = new Japan_Label_Form_Data();

            item.barcode = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "barcode"));
            item.goodsNo = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "goods_no"));
            item.brand = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "brand"));
            item.goodsName = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "goods_nm"));
            item.color = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "opt1"));
            item.size = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "opt2"));
            item.mixturerate = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "mixturerate"));
            item.price = Common.Method.getInt(Math.Round(Common.Method.getInt(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "price")) / 10 * 1.0185185184 / 1.1));
            item.tax_price = Common.Method.getInt(Math.Round(item.price + Convert.ToDouble(item.price * 0.1)));
            item.NumberOfCopies = Convert.ToInt16(copy);

            data.Add(item);

            return data;
        }

        private List<Japan_Label_Form_Data> ReportData2()
        {
            Japan_Label_Form_Data item = null;
            List<Japan_Label_Form_Data> data = new List<Japan_Label_Form_Data>();

            item = new Japan_Label_Form_Data();

            item.barcode = barcode;
            item.brand = brand_name.Text;
            item.goodsNo = txt_goodsno.Text;
            item.goodsName = goods_name.Text;
            item.color = color.Text;
            item.size = size.Text;
            item.mixturerate = mixturerate;
            item.price = price;
            item.tax_price = tax_price;
            item.NumberOfCopies = 1;

            data.Add(item);

            return data;
        }

        private List<Label_Form_Data> ReportData3()
        {
            Label_Form_Data item = null;
            List<Label_Form_Data> data = new List<Label_Form_Data>();

            item = new Label_Form_Data();

            item.barcode = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "barcode"));
            item.goods_no = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "goods_no"));
            item.brand = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "brand"));
            item.goods_name = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "goods_nm"));
            item.color = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "opt1"));
            item.size = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "opt2"));
            item.mixturerate = Common.Method.getStr(gridViewItem.GetRowCellValue(gridViewItem.FocusedRowHandle, "mixturerate"));
            item.NumberOfCopies = Convert.ToInt16(copy);

            data.Add(item);

            return data;
        }

        private List<Label_Form_Data> ReportData4()
        {
            Label_Form_Data item = null;
            List<Label_Form_Data> data = new List<Label_Form_Data>();

            item = new Label_Form_Data();

            item.barcode = barcode;
            item.brand = brand_name.Text;
            item.goods_no = txt_goodsno.Text;
            item.goods_name = goods_name.Text;
            item.color = color.Text;
            item.size = size.Text;
            item.mixturerate = mixturerate;
            item.NumberOfCopies = 1;

            data.Add(item);

            return data;
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if(barcode != "")
            {
                if (barcode.Length == 13)
                {
                    price_yn = true;
                    ReportPrint("scanner");
                }
            }

            this.txt_barcode.Focus();
            Cursor.Current = Cursors.Default;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string date = Common.Method.getStr(dateEdit.EditValue).Replace("/", "-");
            string week = Common.Method.getStr(lookUpEditWeek.EditValue);

            if((date != "") && (week != ""))
            {
                Cursor.Current = Cursors.WaitCursor;
                this.txtBarcode.Text = "";
                TotalBrandCnt();
                //TotalItemCnt();
                SearchItem();
                Cursor.Current = Cursors.Default;
            }
        }

        private void TotalBrandCnt()
        {
            string week = Common.Method.getStr(lookUpEditWeek.EditValue);

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pWEEK", week)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_BRAND_CNT", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    lblBrandCnt.Text = Common.Method.getStr(ResultDT.Rows[0]["cnt"]);
                }
                else
                {
                    lblBrandCnt.Text = "0";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //private void TotalItemCnt()
        //{
        //    string week = Common.Method.getStr(lookUpEditWeek.EditValue);

        //    try
        //    {
        //        MySqlParameter[] param =
        //        {
        //            new MySqlParameter("@pWEEK", week)
        //        };

        //        DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_ITEM_CNT", param, server);

        //        if (ResultDT.Rows.Count > 0)
        //        {
        //            lblTotalExpectedCnt.Text = Common.Method.getStr(ResultDT.Rows[0]["total_pc"]);
        //        }
        //        else
        //        {
        //            lblTotalExpectedCnt.Text = "";
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //    }
        //}

        private void gridViewItem_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if(e.Column.FieldName == "status")
            {
                e.Appearance.FontSizeDelta = 12;

                if(Common.Method.getStr(e.CellValue) == "入庫完了")
                {
                    e.Appearance.ForeColor = Color.Blue;
                }
                else if(Common.Method.getStr(e.CellValue) == "検品中")
                {
                    e.Appearance.ForeColor = Color.Red;
                }
            }
            else if((e.Column.FieldName == "brand") || (e.Column.FieldName == "goods_no") || (e.Column.FieldName == "goodsnmjp") || (e.Column.FieldName == "opt1") || (e.Column.FieldName == "opt2") || (e.Column.FieldName == "pc") || (e.Column.FieldName == "qc"))
            {
                e.Appearance.FontSizeDelta = 4;
            }
        }

        private void openLink(string productNo)
        {
            Process process = new Process();
            process.StartInfo.FileName = "http://konvini.jp/product/" + productNo;
            process.StartInfo.Verb = "open";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            try
            {
                process.Start();
            }
            catch { }
        }

        private void picProductImg_Click(object sender, EventArgs e)
        {
            openLink(txt_goodsno.Text);
        }

        private void txt_barcode_Enter(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (this.txtBarcode.Text.Length == 13)
            {
                price_yn = true;
                ReportPrint("scanner");
            }

            this.txt_barcode.Focus();
            Cursor.Current = Cursors.Default;
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if(xtraTabControl1.SelectedTabPageIndex == 0)
            {
                txtBarcode.Focus();
            }
            else if(xtraTabControl1.SelectedTabPageIndex == 2)
            {
                GetBoxList();
            }
        }

        private void Set_QC(DevExpress.XtraGrid.Views.Grid.GridView gridview)
        {
            int rowhandle = -1;
            int qc = 0;
            int pc = 0;
            int jp_stock = 0;
            string status = "";

            Cursor.Current = Cursors.WaitCursor;

            if (gridview.Name == "gridViewItem")
            {
                rowhandle = gridview.LocateByValue("barcode", this.txtBarcode.Text);
            }
            else
            {
                rowhandle = gridview.LocateByValue("barcode", this.txtBarcode2.Text);
            }


            //rowhandle = gridViewItem.FocusedRowHandle;

            if (rowhandle > -1)
            {
                pc = Common.Method.getInt(gridview.GetRowCellValue(rowhandle, "pc"));
                qc = Common.Method.getInt(gridview.GetRowCellValue(rowhandle, "qc"));
                status = Common.Method.getStr(gridview.GetRowCellValue(rowhandle, "status"));
                jp_stock = Common.Method.getInt(gridview.GetRowCellValue(rowhandle, "jp_stock"));

                if (pc > 0)
                {
                    if (status == "検品中")
                    {
                        gridview.FocusedRowHandle = rowhandle;
                        gridview.RefreshData();
                        qc += 1;
                        jp_stock += 1;
                        gridview.SetRowCellValue(rowhandle, "change_status", "Y");
                        change_YN = true;

                        gridview.SetRowCellValue(rowhandle, "qc", qc);
                        gridview.SetRowCellValue(rowhandle, "jp_stock", jp_stock);

                        if (pc == qc)
                        {
                            gridview.SetRowCellValue(rowhandle, "status", "入庫完了");
                        }
                    }
                    else if (status == "入庫完了")
                    {
                        XtraMessageBox.Show("検品数量を超えています。", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //MessageBox.Show("検品数量を超えています。", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SystemSounds.Beep.Play();
                    }
                }
            }
            else
            {
                if (gridview.Name == "gridViewItem2")
                {
                    string goodsno = "";
                    string optno = "";
                    string pOptno = "";

                    goodsno = txtBarcode2.Text.Substring(4, 6);
                    optno = txtBarcode2.Text.Substring(10, 3);

                    if (optno.Substring(0, 1) == "0")
                    {
                        if (optno.Substring(1, 1) == "0")
                        {
                            pOptno = optno.Substring(2, 1);
                        }
                        else
                        {
                            pOptno = optno.Substring(1, 2);
                        }
                    }
                    else
                    {
                        pOptno = optno;
                    }

                    MySqlParameter[] param =
                    {
                    new MySqlParameter("@pGOODSNO", goodsno),
                    new MySqlParameter("@pOPTNO", pOptno)
                    };

                    DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_ITEM", param, server);

                    if (ResultDT.Rows.Count > 0)
                    {
                        foreach (DataRow item in ResultDT.Rows)
                        {
                            item["barcode"] = "1000" + Common.Method.getStr(item["goodsno"]) + string.Format("{0:D3}", Common.Method.getInt(item["optno"]));
                            item["opt1"] = GetCodeName(Common.Method.getStr(item["opt1"]), "JP");
                            item["opt2"] = GetCodeName(Common.Method.getStr(item["opt2"]), "JP");
                        }

                        DataTable CurrentDT = new DataTable();

                        CurrentDT = gridControlItem2.DataSource as DataTable;

                        CurrentDT.Merge(ResultDT);

                        gridControlItem2.DataSource = CurrentDT;
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void Set_QC2(DevExpress.XtraGrid.Views.Grid.GridView gridview)
        {
            int rowhandle = -1;
            int qc = 0;
            int pc = 0;
            int jp_stock = 0;
            string status = "";

            Cursor.Current = Cursors.WaitCursor;

            if (gridview.Name == "gridViewItem")
            {
                rowhandle = gridview.LocateByValue("barcode", this.txtBarcode.Text);
            }
            else
            {
                rowhandle = gridview.LocateByValue("barcode", this.txtBarcode2.Text);
            }


            //rowhandle = gridViewItem.FocusedRowHandle;

            if (rowhandle > -1)
            {
                pc = Common.Method.getInt(gridview.GetRowCellValue(rowhandle, "pc"));
                qc = Common.Method.getInt(gridview.GetRowCellValue(rowhandle, "qc"));
                status = Common.Method.getStr(gridview.GetRowCellValue(rowhandle, "status"));
                jp_stock = Common.Method.getInt(gridview.GetRowCellValue(rowhandle, "jp_stock"));

                if (pc > 0)
                {
                    if (status == "検品中")
                    {
                        gridview.FocusedRowHandle = rowhandle;
                        gridview.RefreshData();
                        qc += 1;
                        jp_stock += 1;
                        gridview.SetRowCellValue(rowhandle, "change_status", "Y");
                        change_YN = true;

                        gridview.SetRowCellValue(rowhandle, "qc", qc);
                        gridview.SetRowCellValue(rowhandle, "jp_stock", jp_stock);

                        if (pc == qc)
                        {
                            gridview.SetRowCellValue(rowhandle, "status", "入庫完了");
                        }
                    }
                    else if (status == "入庫完了")
                    {
                        MessageBox.Show("検品数量を超えています。", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //SystemSounds.Beep.Play();
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void txtShippingBarcode_Leave(object sender, EventArgs e)
        {
            string controlName = GetFocusControl();

            if (controlName != "groupControl3")
            {
                txtBarcode.Focus();
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Winapi)]
        internal static extern IntPtr GetFocus();

        private string GetFocusControl()
        {
            Control focusControl = null;
            IntPtr focusHandle = GetFocus();
            if (focusHandle != IntPtr.Zero)
                focusControl = Control.FromHandle(focusHandle);
            if (focusControl.Name.ToString().Length == 0)
                return focusControl.Parent.Parent.Name.ToString();
            else
                return focusControl.Name.ToString();
        }

        private void txtShippingBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8 && e.KeyChar != 22) //8:백스페이스
            {
                e.Handled = true;
            }
        }

        private void SetupPrinterSet()
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("CODE");
            DT.Columns.Add("CODE_NAME");

            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                DataRow DR = DT.NewRow();
                DR["CODE"] = printer;
                DR["CODE_NAME"] = printer;
                DT.Rows.Add(DR);
            }

            Common.Method.SetLookupEdit(lookUpEditPrinter, DT, "jp");
        }

        private void SearchItem()
        {
            string week = Common.Method.getStr(lookUpEditWeek.EditValue);

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pDATEWEEK", week)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_ORDERLIST", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    foreach (DataRow item in ResultDT.Rows)
                    {
                        item["barcode"] = "1000" + Common.Method.getStr(item["goods_no"]) + string.Format("{0:D3}", Common.Method.getInt(item["opt_no"]));
                        item["opt1"] = GetCodeName(Common.Method.getStr(item["opt1"]), "JP");
                        item["opt2"] = GetCodeName(Common.Method.getStr(item["opt2"]), "JP");
                        item["mixtureRate"] = GetMixtureName(Common.Method.getStr(item["mixtureRate"]), "JP");
                    }

                    gridControlItem.DataSource = ResultDT;
                }
                else
                {
                    gridControlItem.DataSource = null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void SearchItem2()
        {
            string goodsno = txtBarcode2.Text.Substring(4, 6);
            string optno = txtBarcode2.Text.Substring(10, 3);
            string pOptno = "";

            try
            {
                if (optno.Substring(0, 1) == "0")
                {
                    if (optno.Substring(1, 1) == "0")
                    {
                        pOptno = optno.Substring(2, 1);
                    }
                    else
                    {
                        pOptno = optno.Substring(1, 2);
                    }
                }
                else
                {
                    pOptno = optno;
                }

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pGOODSNO", goodsno),
                    new MySqlParameter("@pOPTNO", pOptno)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_ITEM", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    foreach (DataRow item in ResultDT.Rows)
                    {
                        item["barcode"] = "1000" + Common.Method.getStr(item["goodsno"]) + string.Format("{0:D3}", Common.Method.getInt(item["optno"]));
                        item["opt1"] = GetCodeName(Common.Method.getStr(item["opt1"]), "JP");
                        item["opt2"] = GetCodeName(Common.Method.getStr(item["opt2"]), "JP");
                    }

                    gridControlItem2.DataSource = ResultDT;
                }
                else
                {
                    gridControlItem2.DataSource = null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void gridViewBrand_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            e.Appearance.FontSizeDelta = 5;
        }

        public void GetCopyCnt(int pCNT, bool pPrice_yn = false)
        {
            copy = pCNT;
            price_yn = pPrice_yn;
        }

        private void btnQC_Click(object sender, EventArgs e)
        {
            if (txtBarcode.Text.Length > 0)
            {
                this.txtBarcode.EditValueChanged -= txtBarcode_EditValueChanged;
                this.txtBarcode.Text = this.txtBarcode.Text.Replace("-", "");
                this.txtBarcode.Focus();
                this.txtBarcode.EditValueChanged += txtBarcode_EditValueChanged;
            }

            if ((txtBarcode.Text.Length == 13) && (gridViewItem.RowCount > 0))
            {
                Set_QC(gridViewItem);
            }

            this.txtBarcode.Text = "";
            this.txtBarcode.Refresh();
        }

        private void dateEdit_EditValueChanged(object sender, EventArgs e)
        {
            SetSearchWeek();
        }

        public void gridViewItem_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            string goodsNo = "";
            string qc = "";

            goodsNo = Common.Method.getStr(gridViewItem.GetFocusedRowCellValue("goods_no"));

            if ((e.Column.FieldName == "goods_no") || e.Column.FieldName == "goods_nm")
            {
                openLink(goodsNo);
            }

            if(e.Column.FieldName == "status")
            {
                qc = Common.Method.getStr(gridViewItem.GetFocusedRowCellValue("qc"));

                JapanStorePopup popUp = new JapanStorePopup(qc);
                popUp.parentForm = this;
                popUp.StartPosition = FormStartPosition.CenterScreen;
                popUp.ShowDialog(this);
                if (popUp.DialogResult == DialogResult.OK)
                {
                    if(copy > 0)
                    {
                        ReportPrint("barcode", copy);
                    }
                }
            }
        }

        private void txtBarcode_EditValueChanged(object sender, EventArgs e)
        {
            this.txtBarcode.EditValueChanged -= txtBarcode_EditValueChanged;

             if (txtBarcode.Text.Length > 0)
            {
                this.txtBarcode.Text = this.txtBarcode.Text.Replace("-", "");
                this.txtBarcode.Focus();
            }

            if ((txtBarcode.Text.Length == 13) && (gridViewItem.RowCount > 0))
            {
                Set_QC(gridViewItem);
                this.txtBarcode.Text = "";
                this.txtBarcode.Refresh();
            }

            this.txtBarcode.EditValueChanged += txtBarcode_EditValueChanged;
        }

        private void btnStore_Click(object sender, EventArgs e)
        {
            string change_status = "";
            bool result = false;

            if(gridViewItem.RowCount > 0)
            {
                if(change_YN)
                {
                    for (int i = 0; gridViewItem.RowCount > i; i++)
                    {
                        change_status = Common.Method.getStr(gridViewItem.GetRowCellValue(i, "change_status"));

                        if (change_status == "Y")
                        {
                            result = setStore(i);

                            if (!result)
                            {
                                MessageBox.Show("Error.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }

                    MessageBox.Show("입고 완료.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    TotalBrandCnt();
                    //TotalItemCnt();
                    SearchItem();
                }
                else
                {
                    MessageBox.Show("저장 할 입고내역이 없습니다.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("조회 후 입고 가능합니다.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool setStore(int rowhandle)
        {
            Cursor.Current = Cursors.WaitCursor;
            string barcode = "";
            int item_no = 0;
            int goods_no = 0;
            int opt_no = 0;
            int qc = 0;
            int jp_stock = 0;
            bool result = false;

            try
            {
                barcode = Common.Method.getStr(gridViewItem.GetRowCellValue(rowhandle, "barcode"));
                item_no = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "item_no"));
                goods_no = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "goods_no"));
                opt_no = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "opt_no"));
                qc = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "qc"));
                jp_stock = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "jp_stock"));

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pITEM_NO", item_no),
                    new MySqlParameter("@pBARCODE", barcode),
                    new MySqlParameter("@pGOODS_NO", goods_no),
                    new MySqlParameter("@pOPT_NO", opt_no),
                    new MySqlParameter("@pQC", qc),
                    new MySqlParameter("@pJP_STOCK", jp_stock)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_SET_JAPAN_STORE", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    if(Common.Method.getStr(ResultDT.Rows[0]["RESULT"]) == "UPDATE OK")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            return result;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            int rowhandle = -1;
            int set_value = 0;
            int jp_stock = 0;
            int pc = 0;

            if(gridViewItem.RowCount > 0)
            {
                rowhandle = gridViewItem.FocusedRowHandle;

                if(rowhandle > -1)
                {
                    set_value = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "qc")) - 1;
                    pc = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "pc"));
                    jp_stock = Common.Method.getInt(gridViewItem.GetRowCellValue(rowhandle, "jp_stock")) - 1;

                    if (set_value >= 0)
                    {
                        if (pc > set_value)
                        {
                            gridViewItem.SetRowCellValue(rowhandle, "jp_stock", jp_stock);
                            gridViewItem.SetRowCellValue(rowhandle, "qc", set_value);
                            gridViewItem.SetRowCellValue(rowhandle, "change_status", "Y");
                            gridViewItem.SetRowCellValue(rowhandle, "status", "検品中");
                        }

                        change_YN = true;
                    }
                }
            }
        }

        private void updateOrder()
        {
            string change_status = "";

            if (gridViewItem.RowCount > 0)
            {
                if (change_YN)
                {
                    for (int i = 0; gridViewItem.RowCount > i; i++)
                    {
                        change_status = Common.Method.getStr(gridViewItem.GetRowCellValue(i, "change_status"));

                        if (change_status == "Y")
                        {
                            setStore(i);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("저장 할 입고내역이 없습니다.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("조회 후 입고 가능합니다.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void gridViewBox_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lblBoxNo.Text = "BOX :" + Common.Method.getStr(gridViewBox.GetRowCellValue(gridViewBox.FocusedRowHandle, "no"));
            lblBoxItemCnt.Text = "合計数量 : 0";
            this.gridControlPackingItem.DataSource = packingItemDT;
            GetPackingItemList();
        }

        private void GetPackingItemList()
        {
            Cursor.Current = Cursors.WaitCursor;
            int boxno = 0;
            string weekdate = "";

            try
            {
                boxno = Common.Method.getInt(gridViewBox.GetRowCellValue(gridViewBox.FocusedRowHandle, "item_no"));
                weekdate = Common.Method.getStr(lookUpEditWeekBox.EditValue);

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pWEEKDATE", weekdate),
                    new MySqlParameter("@pBOXNO", boxno)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_PACKING_LIST", param, server);

                if(ResultDT.Rows.Count > 0)
                {
                    foreach (DataRow item in ResultDT.Rows)
                    {
                        item["opt1"] = GetCodeName(Common.Method.getStr(item["opt1"]), "JP");
                        item["opt2"] = GetCodeName(Common.Method.getStr(item["opt2"]), "JP");
                    }
                    gridControlPackingItem.DataSource = ResultDT;
                }
                else
                {
                    gridControlPackingItem.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void txtBarcodeBox_EditValueChanged(object sender, EventArgs e)
        {
            if(txtBarcodeBox.Text.Length == 13)
            {
                this.txtBarcodeBox.EditValueChanged -= txtBarcodeBox_EditValueChanged;
                GetItemToBarcode();
                this.txtBarcodeBox.EditValueChanged += txtBarcodeBox_EditValueChanged;
            }
        }

        private void GetItemToBarcode()
        {
            Cursor.Current = Cursors.WaitCursor;
            string goodsno = txtBarcodeBox.Text.Substring(4, 6);
            string optno = txtBarcodeBox.Text.Substring(10, 3);
            string pOptno = "";
            int box = Common.Method.getInt(gridViewBox.GetRowCellValue(gridViewBox.FocusedRowHandle, "no"));
            int rowhandle = -1;
            int currentQC = 0;
            int currentBOX_PC = 0;
            int addBOX_PC = 0;
            int totalPC = 0;

            string period = Common.Method.getStr(lookUpEditWeekBox2.EditValue);

            try
            {
                if (optno.Substring(0, 1) == "0")
                {
                    if (optno.Substring(1, 1) == "0")
                    {
                        pOptno = optno.Substring(2, 1);
                    }
                    else
                    {
                        pOptno = optno.Substring(1, 2);
                    }
                }
                else
                {
                    pOptno = optno;
                }

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pPERIOD", period),
                    new MySqlParameter("@pGOODSNO", goodsno),
                    new MySqlParameter("@pOPTNO", pOptno),
                    new MySqlParameter("@pBOX", box)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_ITEM_TO_BARCODE", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    SetGridControlDataSource_Item();

                    foreach (DataRow item in ResultDT.Rows)
                    {
                        item["barcode"] = "1" + string.Format("{0:D9}", Common.Method.getInt(item["goodsno"])) + string.Format("{0:D3}", Common.Method.getInt(item["optno"]));
                        item["color"] = GetCodeName(Common.Method.getStr(item["color"]), "JP");
                        item["size"] = GetCodeName(Common.Method.getStr(item["size"]), "JP");
                    }

                    DataTable CurrentDT = new DataTable();

                    CurrentDT = gridControlPackingItem.DataSource as DataTable;

                    if (CurrentDT.Rows.Count > 0)
                    {
                        rowhandle = gridViewPackingItem.LocateByValue("barcode", Common.Method.getStr(ResultDT.Rows[0]["barcode"]));

                        if(rowhandle > -1)
                        {
                            currentQC = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(rowhandle, "STOCK"));
                            currentBOX_PC = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(rowhandle, "BOX_PC"));

                            if (currentQC > currentBOX_PC)
                            {
                                if(currentQC != 0)
                                {
                                    addBOX_PC = currentBOX_PC + 1;
                                }

                                gridViewPackingItem.SetRowCellValue(rowhandle, "BOX_PC", addBOX_PC);
                                gridViewPackingItem.FocusedRowHandle = gridViewPackingItem.LocateByValue("barcode", txtBarcodeBox.Text);
                            }
                            else
                            {
                                gridViewPackingItem.FocusedRowHandle = gridViewPackingItem.LocateByValue("barcode", txtBarcodeBox.Text);
                                SystemSounds.Beep.Play();
                                MessageBox.Show("検品数量を超えています。", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }

                            for(int i = 0; gridViewPackingItem.RowCount > i; i++)
                            {
                                totalPC = totalPC + Common.Method.getInt(gridViewPackingItem.GetRowCellValue(i, "BOX_PC"));
                            }
                        }
                        else
                        {
                            if(Common.Method.getInt(ResultDT.Rows[0]["STOCK"]) == 0)
                            {
                                ResultDT.Rows[0]["BOX_PC"] = 0;
                            }
                            

                            CurrentDT.Merge(ResultDT);
                            gridControlPackingItem.DataSource = CurrentDT;
                            //gridViewPackingItem.FocusedRowHandle = gridViewPackingItem.RowCount - 1;
                            gridViewPackingItem.FocusedRowHandle = gridViewPackingItem.LocateByValue("barcode", txtBarcodeBox.Text);
                        }
                        
                    }
                    else
                    {
                        if(Common.Method.getInt(ResultDT.Rows[0]["STOCK"]) > 0)
                        {
                            totalPC = 1;
                        }
                        else
                        {
                            ResultDT.Rows[0]["BOX_PC"] = 0;
                        }
                        
                        gridControlPackingItem.DataSource = ResultDT;
                    }
                    
                }

            }
            catch (Exception ex)
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                lblBoxItemCnt.Text = "合計数量 : " + Common.Method.getStr(totalPC);

                this.txtBarcodeBox.Text = "";
                this.txtBarcodeBox.Refresh();
                this.gridViewPackingItem.RefreshData();

                Cursor.Current = Cursors.Default;
            }
        }

        private void btn_select_Click(object sender, EventArgs e)
        {
            this.txt_barcode.Text = "";
            this.txt_barcode.Focus();
            this.txt_goodsno.Text = "";
            this.txt_optno.Text = "";
            this.picProductImg.Image = null;
            this.brand_name.Text = "";
            this.goods_name.Text = "";
            this.color.Text = "";
            this.size.Text = "";

            barcode = "";
        }

        private void txt_barcode_EditValueChanged(object sender, EventArgs e)
        {

            barcode = Common.Method.getStr(txt_barcode.Text);

            if (barcode.Length == 13)
            {
                SelectData();
            }
        }

        private void SelectData()
        {
            string url = "";
            string imageno = "";
            string optno = "";
            string color_name = "";
            string size_name = "";
            string mix_name = "";

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = txt_barcode.Text;

                if (barcode != "")
                {
                    txt_goodsno.Text = barcode.Substring(4, 6);
                    optno = barcode.Substring(10, 3);
                    
                    if(optno.Substring(0, 1) == "0")
                    {
                        if (optno.Substring(1, 1) == "0")
                        {
                            txt_optno.Text = optno.Substring(2, 1);
                        }
                        else
                        {
                            txt_optno.Text = optno.Substring(1, 2);
                        }
                    }
                    else
                    {
                        txt_optno.Text = optno;
                    }
                }
                else
                {
                    barcode = "1" + string.Format("{0:D9}", this.txt_goodsno.Text) + string.Format("{0:D3}", this.txt_optno.Text);
                    this.txt_barcode.Text = barcode;
                }

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pGOODSNO", this.txt_goodsno.Text),
                    new MySqlParameter("@pOPTNO", this.txt_optno.Text)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_LIST_GOODSFORLABEL", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    color_name = GetCodeName(Common.Method.getStr(ResultDT.Rows[0]["opt1"]), "JP");
                    size_name = GetCodeName(Common.Method.getStr(ResultDT.Rows[0]["opt2"]), "JP");
                    mix_name = GetMixtureName(Common.Method.getStr(ResultDT.Rows[0]["mixtureRate"]), "JP");

                    barcode = txt_barcode.Text;
                    this.brand_name.Text = Common.Method.getStr(ResultDT.Rows[0]["brand"]);
                    this.goods_name.Text = Common.Method.getStr(ResultDT.Rows[0]["goodsnmjp"]);
                    brand_goodsnm = "[" + Common.Method.getStr(ResultDT.Rows[0]["brand"]) + "]" + Common.Method.getStr(ResultDT.Rows[0]["goodsnmjp"]);
                    this.color.Text = color_name;
                    this.size.Text = size_name;
                    this.txt_goodsno.Text = Common.Method.getStr(ResultDT.Rows[0]["goodsno"]);
                    mixturerate = mix_name;
                    price = Common.Method.getInt(Math.Round(Common.Method.getInt(ResultDT.Rows[0]["price"])/ 10* 1.0185185184 / 1.1));
                    tax_price =Common.Method.getInt(Math.Round(price + Convert.ToDouble(price * 0.1)));

                    imageno = GetImageNo(this.txt_goodsno.Text, this.txt_optno.Text);

                    if (imageno == "0")
                    {
                        url = "https://d2ncantiuancgo.cloudfront.net/goods/" + this.txt_goodsno.Text + ".jpg";
                    }
                    else
                    {
                        url = "https://d2ncantiuancgo.cloudfront.net/tgoods/" + this.txt_goodsno.Text + "/" + imageno + ".jpg";
                    }

                    picProductImg.Properties.ZoomPercent = 67;
                    picProductImg.Image = GET(url);

                    //ReportPrint("scanner");
                }
                else
                {
                    this.txt_goodsno.Text = "해당 바코드의 상품이 없습니다.";
                    this.txt_optno.Text = "";
                    this.brand_name.Text = "";
                    this.goods_name.Text = "";
                    this.color.Text = "";
                    this.size.Text = "";
                    barcode = "";

                    picProductImg.Image = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                this.txt_barcode.EditValueChanged -= txt_barcode_EditValueChanged;
                this.txt_barcode.Text = "";
                this.txt_barcode.EditValueChanged += txt_barcode_EditValueChanged;
                this.txt_barcode.Focus();
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            if(btnScan.Text.Length == 13)
            {
                GetItemToBarcode();
            }
        }

        private void gridViewPackingItem_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = gridViewItem;

            if (gridView.FocusedColumn.FieldName != "chk")
            {
                e.Cancel = true;
            }
        }

        private void gridViewPackingItem_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            int pc = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(e.RowHandle, "BOX_PC"));
            int stock = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(e.RowHandle, "STOCK")); ;

            if (e.Column.FieldName == "TOTAL_QC")
            {
                e.Appearance.FontSizeDelta = 12;
                e.Appearance.ForeColor = Color.Blue;
            }
            else if(e.Column.FieldName == "BOX_PC")
            {
                if(stock == pc)
                {
                    e.Appearance.FontSizeDelta = 12;
                    e.Appearance.ForeColor = Color.LightGreen;
                }
                else
                {
                    e.Appearance.FontSizeDelta = 12;
                    e.Appearance.ForeColor = Color.Red;
                }
                
            }
            else
            {
                e.Appearance.FontSizeDelta = 4;
            }
        }

        private void lookUpEditWeekBox_EditValueChanged(object sender, EventArgs e)
        {
            //lblBoxItemCnt.Text = "合計数量 : 0";
            //gridControlPackingItem.DataSource = packingItemDT;
        }

        private void dateEditBox_EditValueChanged(object sender, EventArgs e)
        {
            SetSearchWeekBox();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool result = false;
            string period = "";
            int box_no = Common.Method.getInt(gridViewBox.GetRowCellValue(gridViewBox.FocusedRowHandle, "no"));
            string barcode = "";
            int goods_no = 0;
            int opt_no = 0;
            int pc = 0;

            if(gridViewPackingItem.RowCount > 0)
            {
                for(int i = 0; gridViewPackingItem.RowCount > i; i++)
                {
                    lookUpEditWeekBox.Refresh();
                    period = Common.Method.getStr(lookUpEditWeekBox.EditValue);
                    barcode = Common.Method.getStr(gridViewPackingItem.GetRowCellValue(i, "barcode"));
                    goods_no = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(i, "goodsno"));
                    opt_no = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(i, "optno"));
                    pc = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(i, "BOX_PC"));

                    if(pc > 0)
                    {
                        if(period != "")
                        {
                            result = Set_BOX(period, box_no, barcode, goods_no, opt_no, pc);
                        }
                        else
                        {
                            MessageBox.Show("주차가 선택되어 있지 않습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }

                if (result)
                {
                    //GetItemToBarcode();

                    MessageBox.Show("저장 완료.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            
        }

        private bool Set_BOX(string period, int box_no, string barcode, int goods_no, int opt_no, int pc)
        {
            bool result = false;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pPERIOD", period),
                    new MySqlParameter("@pBOX_NO", box_no),
                    new MySqlParameter("@pBARCODE", barcode),
                    new MySqlParameter("@pGOODS_NO", goods_no),
                    new MySqlParameter("@pOPT_NO", opt_no),
                    new MySqlParameter("@pPC", pc)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_SET_JAPAN_BOX", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    if (Common.Method.getStr(ResultDT.Rows[0]["RESULT"]) != "ERROR")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            return result;
        }

        private void btnBoxCancel_Click(object sender, EventArgs e)
        {
            int rowhandle = -1;
            int set_value = 0;
            int qc = 0;
            int totalQC = 0;

            if (gridViewPackingItem.RowCount > 0)
            {
                rowhandle = gridViewPackingItem.FocusedRowHandle;

                if (rowhandle > -1)
                {
                    set_value = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(rowhandle, "BOX_PC")) - 1;
                    qc = Common.Method.getInt(gridViewPackingItem.GetRowCellValue(rowhandle, "STOCK"));

                    if (set_value >= 0)
                    {
                        if (qc > set_value)
                        {
                            gridViewPackingItem.SetRowCellValue(rowhandle, "BOX_PC", set_value);

                            for(int i = 0; gridViewPackingItem.RowCount > i; i++)
                            {
                                totalQC += Common.Method.getInt(gridViewPackingItem.GetRowCellValue(i, "BOX_PC"));
                            }

                            lblBoxItemCnt.Text = "合計数量 : " + totalQC.ToString();
                        }
                    }
                }
            }
        }

        private void lookUpEditSelect_EditValueChanged(object sender, EventArgs e)
        {
            if(Common.Method.getStr(lookUpEditSelect.EditValue) == "パッキン移動")
            {
                lblWeek.Visible = true;
                lookUpEditWeekBox2.Visible = true;
                lblBox.Visible = true;
                lookUpEditBox.Visible = true;
                btnBoxMove.Visible = true;

                btnPackingCancel.Visible = false;
            }
            else
            {
                lblWeek.Visible = false;
                lookUpEditWeekBox2.Visible = false;
                lblBox.Visible = false;
                lookUpEditBox.Visible = false;
                btnBoxMove.Visible = false;

                btnPackingCancel.Visible = true;
            }
        }

        private void dateEditBox2_EditValueChanged(object sender, EventArgs e)
        {
            SetSearchWeekBox2();
        }

        private void GetBoxList()
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pWEEKDATE", Common.Method.getStr(lookUpEditWeekBox3.EditValue))
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_BOX_LIST", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    gridControlBox2.DataSource = ResultDT;
                }
                else
                {
                    gridControlBox2.DataSource = null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void GetPackingItemList2()
        {
            string boxno = Common.Method.getStr(gridViewBox2.GetRowCellValue(gridViewBox2.FocusedRowHandle, "box_no"));
            int total = 0;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pWEEKDATE", Common.Method.getStr(lookUpEditWeekBox3.EditValue)),
                    new MySqlParameter("@pBOXNO", boxno)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_PACKING_LIST", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    foreach (DataRow item in ResultDT.Rows)
                    {
                        item["size"] = GetCodeName(Common.Method.getStr(item["size"]), "JP");
                        item["color"] = GetCodeName(Common.Method.getStr(item["color"]), "JP");
                        total += Common.Method.getInt(item["box_pc"]);
                    }

                    labelControl17.Text = "入庫予定総数 : " + total.ToString();

                    gridControlPackingItem2.DataSource = ResultDT;
                }
                else
                {
                    labelControl17.Text = "入庫予定総数 : 0";
                    gridControlPackingItem2.DataSource = null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void lookUpEditWeekBox3_EditValueChanged(object sender, EventArgs e)
        {
            GetBoxList();
            lookUpEditWeekBox2.EditValue = lookUpEditWeekBox3.EditValue;
        }

        private void gridViewBox2_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            GetPackingItemList2();
        }

        private void gridViewPackingItem2_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gridView = gridViewPackingItem2;

            if (gridView.FocusedColumn.FieldName != "chk")
            {
                e.Cancel = true;
            }
        }

        private void gridViewPackingItem2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.Column.FieldName == "box_pc")
            {
                e.Appearance.FontSizeDelta = 12;
                e.Appearance.ForeColor = Color.Blue;
            }
            else
            {
                e.Appearance.FontSizeDelta = 4;
            }
        }

        private void btnPackingCancel_Click(object sender, EventArgs e)
        {
            string message = "";

            if(gridViewPackingItem2.RowCount > 0)
            {
                if(lookUpEditWeekBox2.EditValue != null)
                {
                    if(lookUpEditBox.EditValue != null)
                    {
                        message = lookUpEditWeekBox2.Text + "주차의 " + Common.Method.getStr(gridViewBox2.GetRowCellValue(gridViewBox2.FocusedRowHandle, "box_no")) + "번 박스의 패킹을 삭제 하시겠습니까?";

                        if (DialogResult.Yes == Common.Method.getMsgBoxResult(message))
                        {
                            for (int i = 0; gridViewPackingItem2.RowCount > i; i++)
                            {
                                if (Common.Method.getStr(gridViewPackingItem2.GetRowCellValue(i, "chk")) == "Y")
                                {
                                    SetBoxCancel(i);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void btnBoxMove_Click(object sender, EventArgs e)
        {
            string message = "";
            bool chk_YN = false;

            if (gridViewPackingItem2.RowCount > 0)
            {
                if (lookUpEditWeekBox2.EditValue != null)
                {
                    if (lookUpEditBox.EditValue != null)
                    {
                        for(int i = 0; gridViewPackingItem2.RowCount > i; i++)
                        {
                            if(Common.Method.getStr(gridViewPackingItem2.GetRowCellValue(i, "chk")) == "Y")
                            {
                                chk_YN = true;
                            }
                        }

                        if(chk_YN)
                        {
                            message = "선택한 상품을 " + lookUpEditWeekBox3.Text + "주차의 " + Common.Method.getStr(gridViewBox2.GetRowCellValue(gridViewBox2.FocusedRowHandle, "box_no")) + "번 박스를\n" + Common.Method.getStr(lookUpEditWeekBox2.EditValue) + "주차의 " + Common.Method.getStr(lookUpEditBox.EditValue) + "번 박스로 이동 하시겠습니까?";

                            if (DialogResult.Yes == Common.Method.getMsgBoxResult(message))
                            {
                                for (int i = 0; gridViewPackingItem2.RowCount > i; i++)
                                {
                                    if (Common.Method.getStr(gridViewPackingItem2.GetRowCellValue(i, "chk")) == "Y")
                                    {
                                        SetBoxMove(i);
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("선택된 상품이 없습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
        }

        private void SetBoxCancel(int rowhandle)
        {
            string box_idx = Common.Method.getStr(gridViewPackingItem2.GetRowCellValue(rowhandle, "box_idx"));
            string goodsno = Common.Method.getStr(gridViewPackingItem2.GetRowCellValue(rowhandle, "goodsno"));
            string optno = Common.Method.getStr(gridViewPackingItem2.GetRowCellValue(rowhandle, "optno"));
            string barcode = "1000" + goodsno + string.Format("{0:D3}", Common.Method.getInt(optno));
            string pc = Common.Method.getStr(gridViewPackingItem2.GetRowCellValue(rowhandle, "box_pc"));

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pBOX_IDX", box_idx),
                    new MySqlParameter("@pGOODSNO", goodsno),
                    new MySqlParameter("@pOPTNO", optno),
                    new MySqlParameter("@pBARCODE", barcode),
                    new MySqlParameter("@pBOX_PC", pc)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_PACKING_CANCEL", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    if(Common.Method.getStr(ResultDT.Rows[0]["RESULT"]) == "ERROR")
                    {
                        MessageBox.Show("삭제 오류", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        GetBoxList();
                        GetPackingItemList2();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetBoxMove(int rowhandle)
        {
            int box_idx = Common.Method.getInt(gridViewPackingItem2.GetRowCellValue(rowhandle, "box_idx"));
            int box_no = Common.Method.getInt(lookUpEditBox.EditValue);
            string period = Common.Method.getStr(lookUpEditWeekBox2.EditValue);

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pBOX_IDX", box_idx),
                    new MySqlParameter("@pPERIOD", period),
                    new MySqlParameter("@pBOX_NO", box_no)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_JAPAN_PACKING_MOVE", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    if (Common.Method.getStr(ResultDT.Rows[0]["RESULT"]) == "ERROR")
                    {
                        MessageBox.Show("이동 오류", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        lookUpEditWeekBox3.EditValue = lookUpEditWeekBox2.EditValue;
                        GetBoxList();
                        gridViewPackingItem2.LocateByValue("box_idx", box_no);

                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void txtBarcode2_EditValueChanged(object sender, EventArgs e)
        {
            this.txtBarcode2.EditValueChanged -= txtBarcode2_EditValueChanged;

            if (txtBarcode2.Text.Length > 0)
            {
                this.txtBarcode2.Text = this.txtBarcode2.Text.Replace("-", "");
                this.txtBarcode2.Focus();
            }

            if (txtBarcode2.Text.Length == 13)
            {
                if(gridViewItem2.RowCount > 0)
                {
                    Set_QC(gridViewItem2);
                }
                else
                {
                    SearchItem2();
                }
                this.txtBarcode2.Text = "";
                this.txtBarcode2.Refresh();
            }

            this.txtBarcode2.EditValueChanged += txtBarcode2_EditValueChanged;
        }

        private void btnQC2_Click(object sender, EventArgs e)
        {
            if (txtBarcode2.Text.Length > 0)
            {
                this.txtBarcode2.EditValueChanged -= txtBarcode2_EditValueChanged;
                this.txtBarcode2.Text = this.txtBarcode2.Text.Replace("-", "");
                this.txtBarcode2.Focus();
                this.txtBarcode2.EditValueChanged += txtBarcode2_EditValueChanged;
            }

            if (txtBarcode2.Text.Length == 13)
            {
                if(gridViewItem2.RowCount > 0)
                {
                    Set_QC(gridViewItem2);
                }
                else
                {
                    SearchItem2();
                }
                
            }

            this.txtBarcode2.Text = "";
            this.txtBarcode2.Refresh();
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            gridControlItem2.DataSource = null;
        }

        private void gridViewItem2_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            e.Appearance.FontSizeDelta = 12;
        }

        private bool setStore2(int rowhandle)
        {
            Cursor.Current = Cursors.WaitCursor;
            int goods_no = 0;
            int opt_no = 0;
            string barcode = "";
            int qc = 0;
            int jp_stock = 0;
            bool result = false;

            try
            {
                goods_no = Common.Method.getInt(gridViewItem2.GetRowCellValue(rowhandle, "goodsno"));
                opt_no = Common.Method.getInt(gridViewItem2.GetRowCellValue(rowhandle, "optno"));
                barcode = Common.Method.getStr(gridViewItem2.GetRowCellValue(rowhandle, "barcode"));
                qc = Common.Method.getInt(gridViewItem2.GetRowCellValue(rowhandle, "qc"));
                jp_stock = Common.Method.getInt(gridViewItem2.GetRowCellValue(rowhandle, "jp_stock"));

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pGOODSNO", goods_no),
                    new MySqlParameter("@pOPTNO", opt_no),
                    new MySqlParameter("@pBARCODE", barcode),
                    new MySqlParameter("@pQC", qc),
                    new MySqlParameter("@pJP_STOCK", jp_stock)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_SET_JAPAN_STORE_ETC", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    if (Common.Method.getStr(ResultDT.Rows[0]["RESULT"]) == "UPDATE OK")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            return result;
        }

        private void btnStore2_Click(object sender, EventArgs e)
        {
            bool result = true;
            bool errorYN = false;

            if (gridViewItem2.RowCount > 0)
            {
                for (int i = 0; gridViewItem2.RowCount > i; i++)
                {
                    if(result)
                    {
                        result = setStore2(i);

                        if (!result)
                        {
                            MessageBox.Show("Error.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            errorYN = true;
                            break;
                        }
                    }
                }

                if(errorYN == false)
                {
                    MessageBox.Show("입고 완료.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("스캔 후 입고 가능합니다.", "입고", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Btn_print2_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (barcode != "")
            {
                if (barcode.Length == 13)
                {
                    price_yn = false;
                    ReportPrint("scanner");
                }
            }

            this.txt_barcode.Focus();
            Cursor.Current = Cursors.Default;
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            gridControlItem2.DataSource = null;
        }
    }
}
