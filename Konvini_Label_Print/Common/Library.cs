﻿using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Konvini_Label_Print.Common
{
    class Method
    {
        public static DataTable Sql_Sp_GetValue(string uspName, MySqlParameter[] param, string server)
        {
            MySqlConnection conn = new MySqlConnection();

            if(server == "prod")
            {
                //실서버
                conn.ConnectionString = ConstValue.DBConnection.sqlConnectionString_Prod;
            }
            else if(server == "dev")
            {
                //테스트서버
                conn.ConnectionString = ConstValue.DBConnection.sqlConnectionString_Test;
            }
            else
            {
                //실서버
                conn.ConnectionString = ConstValue.DBConnection.sqlConnectionString_Prod;
            }

            MySqlCommand cmd = new MySqlCommand();

            conn.Open();
            cmd.Connection = conn;

            cmd.CommandText = uspName;
            cmd.CommandType = CommandType.StoredProcedure;

            if (param != null)
            {
                cmd.Parameters.AddRange(param);
            }

            DataTable result = new DataTable();

            try
            {
                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                adp.Fill(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return result;
        }

        /// <summary>
        /// null값 처리 후 str로 형변환
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string getStr(object obj)
        {
            string ret = "";

            if (obj != null)
            {
                try
                {
                    ret = obj.ToString();
                }
                catch
                {

                }
            }
            return ret;
        }

        /// <summary>
        /// null값 처리 후 int로 형변환
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int getInt(object obj)
        {
            int ret = 0;

            try
            {
                ret = int.Parse(obj.ToString());
            }
            catch
            {

            }
            return ret;
        }

        /// <summary>
        /// GridView Row번호 생성
        /// </summary>
        /// <param name="grid"></param>
        public static void GridViewRowNumber(DevExpress.XtraGrid.Views.Grid.GridView grid)
        {
            grid.IndicatorWidth = 40;
            grid.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(GridView_CustomDrawRowIndicator);
        }

        /// <summary>
        /// GridViewRowNumber 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GridView_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        /// <summary>
        /// GridView 컬럼 세팅
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="sFiledName"></param>
        /// <param name="sCaption"></param>
        /// <param name="nWidth"></param>
        /// <param name="hAlign"></param>
        /// <param name="bVisible"></param>
        public static void setGridViewColumn(DevExpress.XtraGrid.Views.Grid.GridView grid
                                            , string sFiledName
                                            , string sCaption
                                            , int nWidth
                                            , DevExpress.Utils.HorzAlignment hAlign
                                            , bool bVisible
                                            )
        {
            DevExpress.XtraGrid.Columns.GridColumn grdCol = null;
            grdCol = grid.Columns.AddField(sFiledName);
            grdCol.Name = sFiledName;
            grdCol.Caption = sCaption;
            grdCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            grdCol.AppearanceCell.TextOptions.HAlignment = hAlign;
            grdCol.Width = nWidth;
            grdCol.Visible = bVisible;
            grdCol.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
        }

        public static void setGridViewcolumnImage(DevExpress.XtraGrid.GridControl gridControl1
                                                 , DevExpress.XtraGrid.Views.Grid.GridView gridView1
                                                 , string colName)
        {
            RepositoryItemPictureEdit pictureEdit = new RepositoryItemPictureEdit();
            pictureEdit.SizeMode = PictureSizeMode.Squeeze;
            pictureEdit.PictureAlignment = ContentAlignment.MiddleCenter;
            pictureEdit.PictureStoreMode = PictureStoreMode.Default;
            pictureEdit.NullText = " ";
            gridView1.Columns[colName].ColumnEdit = pictureEdit;
            gridControl1.RepositoryItems.Add(pictureEdit);
        }

        /// <summary>
        /// GridView 컬럼 CheckEdit 설정
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="sFieldName"></param>
        /// <param name="sCheck"></param>
        /// <param name="sUnChk"></param>
        public static void setGridViewColumnCheckEdit(DevExpress.XtraGrid.Views.Grid.GridView grid
                                                    , string sFieldName
                                                    , string sCheck
                                                    , string sUnChk
                                                    )
        {
            DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chk = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            chk.ValueChecked = sCheck;
            chk.ValueUnchecked = sUnChk;
            chk.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            grid.Columns[sFieldName].ColumnEdit = chk;
        }

        //
        // GridView 컬럼 설정 : ButtonEdit
        //
        public static void setGridViewColumnButtonEdit(DevExpress.XtraGrid.Views.Grid.GridView grid1
                                                        , string sFieldName
                                                        , string sCaptionName
                                                        , DevExpress.XtraEditors.Controls.ButtonPressedEventHandler objEvent
                                                        )
        {
            //if (grid1.Tag == "Set")
            //{
            //    return;
            //}
            DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit edit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            edit.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
            edit.Buttons[0].Caption = sCaptionName;
            edit.TextEditStyle = TextEditStyles.HideTextEditor;
            grid1.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            if (objEvent != null)
            {
                edit.ButtonClick += objEvent;

            }
            grid1.Columns[sFieldName].ColumnEdit = edit;
        }

        /// <summary>
        /// LookupEdit에 DataSource 및 기타 세팅
        /// </summary>
        /// <param name="CMD"></param>
        /// <param name="DT"></param>
        public static void SetLookupEdit(LookUpEdit CMD, DataTable DT, string country = "kr", Boolean bInsertNone = false, Boolean bInsertAll = false, string valueMember = "CODE", string displayMember = "CODE_NAME")
        {
            try
            {
                if (bInsertNone)
                {
                    DataRow DR = DT.NewRow();
                    DR["CODE"] = "";
                    DR["CODE_NAME"] = "없음";
                    DT.Rows.InsertAt(DR, 0);
                }

                if (bInsertAll)
                {
                    DataRow DR = DT.NewRow();
                    DR["CODE"] = "";
                    DR["CODE_NAME"] = "전체";
                    DT.Rows.InsertAt(DR, 0);
                }

                CMD.Properties.DataSource = DT;
                CMD.Properties.ValueMember = valueMember;
                CMD.Properties.DisplayMember = displayMember;

                CMD.Properties.ForceInitialize();
                CMD.Properties.PopulateColumns();
                CMD.Properties.Columns[valueMember].Visible = false;
                CMD.Properties.Columns[valueMember].Caption = "코드";
                if(country == "kor")
                {
                    CMD.Properties.Columns[displayMember].Caption = "구분";
                }
                else
                {
                    CMD.Properties.Columns[displayMember].Caption = "区分";
                }

                if (DT.Rows.Count > 0)
                {
                    CMD.ItemIndex = 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 레지스트리 등록
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public static void SetRegisterKey(string keyPath, string Key, string Value)
        {
            Microsoft.Win32.RegistryKey Regkey;
            Regkey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(keyPath, RegistryKeyPermissionCheck.ReadWriteSubTree);
            //값 저장하기
            Regkey.SetValue(Key, Value);

            //닫기
            Regkey.Close();
        }

        /// <summary>
        /// 레지스트리 키를 이용해 조회
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetRegisterKey(string keyPath, string key)
        {
            string value = "";
            Microsoft.Win32.RegistryKey Regkey;
            Regkey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(keyPath, RegistryKeyPermissionCheck.ReadWriteSubTree);

            try
            {
                value = Regkey.GetValue(key).ToString();
            }
            catch (Exception)
            {

                value = "";
            }

            //닫기
            Regkey.Close();
            return value;
        }

        /// <summary>
        /// yyyyMMdd string을 yyyy-mm-dd string으로 변환
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string getDate8ToDate10(string p1)
        {
            try
            {
                if (p1 == null)
                {
                    return "";
                }
                else
                {
                    return p1.Substring(0, 4) + "-" + p1.Substring(4, 2) + "-" + p1.Substring(6, 2);
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 메세지 다이얼로그
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="withCancel"></param>
        /// <returns></returns>
        public static DialogResult getMsgBoxResult(string msg, bool withCancel = false)
        {
            DialogResult result = new DialogResult();
            if (withCancel)
            {
                result = MessageBox.Show(msg, "확인", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }
            else
            {
                result = MessageBox.Show(msg, "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }
            Application.DoEvents();
            return result;
        }
    }
}
