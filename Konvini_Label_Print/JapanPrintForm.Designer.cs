﻿namespace Konvini_Label_Print
{
    partial class JapanPrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JapanPrintForm));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.btnStore = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlItem = new DevExpress.XtraGrid.GridControl();
            this.gridViewItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControlPrinter = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEditPrinter = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblBrandCnt = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEditWeek = new DevExpress.XtraEditors.LookUpEdit();
            this.btnQC = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit = new DevExpress.XtraEditors.DateEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.btnScan = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtBarcodeBox = new DevExpress.XtraEditors.TextEdit();
            this.btnBoxCancel = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditWeekBox = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBox = new DevExpress.XtraEditors.DateEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlPackingItem = new DevExpress.XtraGrid.GridControl();
            this.gridViewPackingItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControlBox = new DevExpress.XtraGrid.GridControl();
            this.gridViewBox = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.lblBoxItemCnt = new DevExpress.XtraEditors.LabelControl();
            this.lblBoxNo = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.btnPackingCancel = new DevExpress.XtraEditors.SimpleButton();
            this.lblBox = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEditBox = new DevExpress.XtraEditors.LookUpEdit();
            this.btnBoxMove = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditWeekBox2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lblWeek = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEditSelect = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditWeekBox3 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBox2 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlPackingItem2 = new DevExpress.XtraGrid.GridControl();
            this.gridViewPackingItem2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControlBox2 = new DevExpress.XtraGrid.GridControl();
            this.gridViewBox2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.picProductImg = new DevExpress.XtraEditors.PictureEdit();
            this.goods_name = new DevExpress.XtraEditors.LabelControl();
            this.brand_name = new DevExpress.XtraEditors.LabelControl();
            this.size = new DevExpress.XtraEditors.LabelControl();
            this.color = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btn_print2 = new DevExpress.XtraEditors.SimpleButton();
            this.txt_barcode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btn_print = new DevExpress.XtraEditors.SimpleButton();
            this.btn_select = new DevExpress.XtraEditors.SimpleButton();
            this.txt_optno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt_goodsno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnInit = new DevExpress.XtraEditors.SimpleButton();
            this.btnStore2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel2 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.lblTotalExpectedCnt2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.lblBrandCnt2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.btnQC2 = new DevExpress.XtraEditors.SimpleButton();
            this.txtBarcode2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlItem2 = new DevExpress.XtraGrid.GridControl();
            this.gridViewItem2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPrinter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeek.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcodeBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeekBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackingItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackingItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBox)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeekBox2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditSelect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeekBox3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackingItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackingItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBox2)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProductImg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_barcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_optno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_goodsno.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 12);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1340, 712);
            this.xtraTabControl1.TabIndex = 17;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage1,
            this.xtraTabPage5});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage2.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage2.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage2.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage2.Controls.Add(this.btnStore);
            this.xtraTabPage2.Controls.Add(this.btnCancel);
            this.xtraTabPage2.Controls.Add(this.gridControlItem);
            this.xtraTabPage2.Controls.Add(this.labelControlPrinter);
            this.xtraTabPage2.Controls.Add(this.lookUpEditPrinter);
            this.xtraTabPage2.Controls.Add(this.groupControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1338, 669);
            this.xtraTabPage2.TabPageWidth = 160;
            this.xtraTabPage2.Text = "入庫検品";
            // 
            // btnStore
            // 
            this.btnStore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStore.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStore.Appearance.Options.UseFont = true;
            this.btnStore.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnStore.ImageOptions.Image")));
            this.btnStore.Location = new System.Drawing.Point(1040, 115);
            this.btnStore.Name = "btnStore";
            this.btnStore.Size = new System.Drawing.Size(114, 53);
            this.btnStore.TabIndex = 19;
            this.btnStore.Text = "保存";
            this.btnStore.Click += new System.EventHandler(this.btnStore_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.ImageOptions.Image")));
            this.btnCancel.Location = new System.Drawing.Point(1160, 115);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(175, 53);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "入庫キャンセル";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridControlItem
            // 
            this.gridControlItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlItem.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlItem.Location = new System.Drawing.Point(11, 174);
            this.gridControlItem.MainView = this.gridViewItem;
            this.gridControlItem.Name = "gridControlItem";
            this.gridControlItem.Size = new System.Drawing.Size(1324, 480);
            this.gridControlItem.TabIndex = 11;
            this.gridControlItem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewItem});
            // 
            // gridViewItem
            // 
            this.gridViewItem.GridControl = this.gridControlItem;
            this.gridViewItem.Name = "gridViewItem";
            this.gridViewItem.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewItem.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewItem.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewItem.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewItem.OptionsView.ShowGroupPanel = false;
            this.gridViewItem.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridViewItem_RowCellClick);
            this.gridViewItem.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewItem_RowCellStyle);
            // 
            // labelControlPrinter
            // 
            this.labelControlPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlPrinter.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlPrinter.Appearance.Options.UseFont = true;
            this.labelControlPrinter.Location = new System.Drawing.Point(951, 11);
            this.labelControlPrinter.Name = "labelControlPrinter";
            this.labelControlPrinter.Size = new System.Drawing.Size(143, 23);
            this.labelControlPrinter.TabIndex = 8;
            this.labelControlPrinter.Text = "プリンター選択 :";
            // 
            // lookUpEditPrinter
            // 
            this.lookUpEditPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEditPrinter.Location = new System.Drawing.Point(1100, 8);
            this.lookUpEditPrinter.Name = "lookUpEditPrinter";
            this.lookUpEditPrinter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPrinter.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPrinter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPrinter.Size = new System.Drawing.Size(235, 30);
            this.lookUpEditPrinter.TabIndex = 6;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.lblBrandCnt);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.lookUpEditWeek);
            this.groupControl3.Controls.Add(this.btnQC);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.txtBarcode);
            this.groupControl3.Controls.Add(this.dateEdit);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.btnSearch);
            this.groupControl3.Location = new System.Drawing.Point(11, 44);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1324, 65);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "検索条件";
            // 
            // lblBrandCnt
            // 
            this.lblBrandCnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBrandCnt.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrandCnt.Appearance.Options.UseFont = true;
            this.lblBrandCnt.Location = new System.Drawing.Point(1265, 35);
            this.lblBrandCnt.Name = "lblBrandCnt";
            this.lblBrandCnt.Size = new System.Drawing.Size(0, 23);
            this.lblBrandCnt.TabIndex = 113;
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(1149, 34);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(108, 23);
            this.labelControl14.TabIndex = 112;
            this.labelControl14.Text = "ブランド数 :";
            // 
            // lookUpEditWeek
            // 
            this.lookUpEditWeek.Location = new System.Drawing.Point(377, 30);
            this.lookUpEditWeek.Name = "lookUpEditWeek";
            this.lookUpEditWeek.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditWeek.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditWeek.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditWeek.Properties.DropDownRows = 2;
            this.lookUpEditWeek.Properties.NullText = "";
            this.lookUpEditWeek.Size = new System.Drawing.Size(140, 30);
            this.lookUpEditWeek.TabIndex = 111;
            // 
            // btnQC
            // 
            this.btnQC.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQC.Appearance.Options.UseFont = true;
            this.btnQC.Location = new System.Drawing.Point(949, 30);
            this.btnQC.Name = "btnQC";
            this.btnQC.Size = new System.Drawing.Size(75, 30);
            this.btnQC.TabIndex = 6;
            this.btnQC.Text = "スキャン";
            this.btnQC.Click += new System.EventHandler(this.btnQC_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(324, 33);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(51, 23);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "回数 :";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(752, 30);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Size = new System.Drawing.Size(191, 30);
            this.txtBarcode.TabIndex = 5;
            this.txtBarcode.EditValueChanged += new System.EventHandler(this.txtBarcode_EditValueChanged);
            // 
            // dateEdit
            // 
            this.dateEdit.EditValue = null;
            this.dateEdit.Location = new System.Drawing.Point(178, 30);
            this.dateEdit.Name = "dateEdit";
            this.dateEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit.Properties.Appearance.Options.UseFont = true;
            this.dateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit.Properties.Mask.EditMask = "y";
            this.dateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit.Properties.VistaCalendarInitialViewStyle = DevExpress.XtraEditors.VistaCalendarInitialViewStyle.YearView;
            this.dateEdit.Properties.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearView;
            this.dateEdit.Size = new System.Drawing.Size(140, 30);
            this.dateEdit.TabIndex = 6;
            this.dateEdit.EditValueChanged += new System.EventHandler(this.dateEdit_EditValueChanged);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(644, 34);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(102, 23);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "バーコード :";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(10, 33);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(162, 23);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "検品スケジュール :";
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Location = new System.Drawing.Point(523, 30);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 30);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "検索";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage3.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage3.Controls.Add(this.btnScan);
            this.xtraTabPage3.Controls.Add(this.labelControl6);
            this.xtraTabPage3.Controls.Add(this.txtBarcodeBox);
            this.xtraTabPage3.Controls.Add(this.btnBoxCancel);
            this.xtraTabPage3.Controls.Add(this.lookUpEditWeekBox);
            this.xtraTabPage3.Controls.Add(this.labelControl7);
            this.xtraTabPage3.Controls.Add(this.dateEditBox);
            this.xtraTabPage3.Controls.Add(this.labelControl10);
            this.xtraTabPage3.Controls.Add(this.gridControlPackingItem);
            this.xtraTabPage3.Controls.Add(this.gridControlBox);
            this.xtraTabPage3.Controls.Add(this.btnSave);
            this.xtraTabPage3.Controls.Add(this.lblBoxItemCnt);
            this.xtraTabPage3.Controls.Add(this.lblBoxNo);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1338, 669);
            this.xtraTabPage3.Text = "出庫検品";
            // 
            // btnScan
            // 
            this.btnScan.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScan.Appearance.Options.UseFont = true;
            this.btnScan.Location = new System.Drawing.Point(471, 4);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(89, 42);
            this.btnScan.TabIndex = 119;
            this.btnScan.Text = "スキャン";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(166, 13);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(102, 23);
            this.labelControl6.TabIndex = 118;
            this.labelControl6.Text = "バーコード :";
            // 
            // txtBarcodeBox
            // 
            this.txtBarcodeBox.Location = new System.Drawing.Point(274, 10);
            this.txtBarcodeBox.Name = "txtBarcodeBox";
            this.txtBarcodeBox.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcodeBox.Properties.Appearance.Options.UseFont = true;
            this.txtBarcodeBox.Size = new System.Drawing.Size(191, 30);
            this.txtBarcodeBox.TabIndex = 117;
            this.txtBarcodeBox.EditValueChanged += new System.EventHandler(this.txtBarcodeBox_EditValueChanged);
            // 
            // btnBoxCancel
            // 
            this.btnBoxCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBoxCancel.Appearance.Options.UseFont = true;
            this.btnBoxCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBoxCancel.ImageOptions.Image")));
            this.btnBoxCancel.Location = new System.Drawing.Point(566, 4);
            this.btnBoxCancel.Name = "btnBoxCancel";
            this.btnBoxCancel.Size = new System.Drawing.Size(181, 42);
            this.btnBoxCancel.TabIndex = 116;
            this.btnBoxCancel.Text = "出庫キャンセル";
            this.btnBoxCancel.Click += new System.EventHandler(this.btnBoxCancel_Click);
            // 
            // lookUpEditWeekBox
            // 
            this.lookUpEditWeekBox.Location = new System.Drawing.Point(1053, 11);
            this.lookUpEditWeekBox.Name = "lookUpEditWeekBox";
            this.lookUpEditWeekBox.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditWeekBox.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditWeekBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditWeekBox.Properties.DropDownRows = 2;
            this.lookUpEditWeekBox.Properties.NullText = "";
            this.lookUpEditWeekBox.Size = new System.Drawing.Size(140, 30);
            this.lookUpEditWeekBox.TabIndex = 115;
            this.lookUpEditWeekBox.EditValueChanged += new System.EventHandler(this.lookUpEditWeekBox_EditValueChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(1000, 14);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(51, 23);
            this.labelControl7.TabIndex = 114;
            this.labelControl7.Text = "回数 :";
            // 
            // dateEditBox
            // 
            this.dateEditBox.EditValue = null;
            this.dateEditBox.Location = new System.Drawing.Point(842, 11);
            this.dateEditBox.Name = "dateEditBox";
            this.dateEditBox.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditBox.Properties.Appearance.Options.UseFont = true;
            this.dateEditBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBox.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBox.Properties.Mask.EditMask = "y";
            this.dateEditBox.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dateEditBox.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditBox.Properties.VistaCalendarInitialViewStyle = DevExpress.XtraEditors.VistaCalendarInitialViewStyle.YearView;
            this.dateEditBox.Properties.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearView;
            this.dateEditBox.Size = new System.Drawing.Size(140, 30);
            this.dateEditBox.TabIndex = 113;
            this.dateEditBox.EditValueChanged += new System.EventHandler(this.dateEditBox_EditValueChanged);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(766, 14);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(70, 23);
            this.labelControl10.TabIndex = 112;
            this.labelControl10.Text = "出庫日 :";
            // 
            // gridControlPackingItem
            // 
            this.gridControlPackingItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlPackingItem.Location = new System.Drawing.Point(93, 99);
            this.gridControlPackingItem.MainView = this.gridViewPackingItem;
            this.gridControlPackingItem.Name = "gridControlPackingItem";
            this.gridControlPackingItem.Size = new System.Drawing.Size(1242, 563);
            this.gridControlPackingItem.TabIndex = 12;
            this.gridControlPackingItem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPackingItem});
            // 
            // gridViewPackingItem
            // 
            this.gridViewPackingItem.GridControl = this.gridControlPackingItem;
            this.gridViewPackingItem.Name = "gridViewPackingItem";
            this.gridViewPackingItem.OptionsFilter.AllowFilterEditor = false;
            this.gridViewPackingItem.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewPackingItem.OptionsView.ShowGroupPanel = false;
            this.gridViewPackingItem.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewPackingItem_RowCellStyle);
            this.gridViewPackingItem.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewPackingItem_ShowingEditor);
            // 
            // gridControlBox
            // 
            this.gridControlBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gridControlBox.Location = new System.Drawing.Point(11, 99);
            this.gridControlBox.MainView = this.gridViewBox;
            this.gridControlBox.Name = "gridControlBox";
            this.gridControlBox.Size = new System.Drawing.Size(76, 563);
            this.gridControlBox.TabIndex = 11;
            this.gridControlBox.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBox});
            // 
            // gridViewBox
            // 
            this.gridViewBox.GridControl = this.gridControlBox;
            this.gridViewBox.Name = "gridViewBox";
            this.gridViewBox.OptionsView.ShowGroupPanel = false;
            this.gridViewBox.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewBox_FocusedRowChanged);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Location = new System.Drawing.Point(1236, 50);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(99, 43);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "保存";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblBoxItemCnt
            // 
            this.lblBoxItemCnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBoxItemCnt.Appearance.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoxItemCnt.Appearance.Options.UseFont = true;
            this.lblBoxItemCnt.Location = new System.Drawing.Point(1000, 50);
            this.lblBoxItemCnt.Name = "lblBoxItemCnt";
            this.lblBoxItemCnt.Size = new System.Drawing.Size(153, 39);
            this.lblBoxItemCnt.TabIndex = 9;
            this.lblBoxItemCnt.Text = "合計数量 :";
            this.lblBoxItemCnt.Visible = false;
            // 
            // lblBoxNo
            // 
            this.lblBoxNo.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoxNo.Appearance.Options.UseFont = true;
            this.lblBoxNo.Location = new System.Drawing.Point(11, 54);
            this.lblBoxNo.Name = "lblBoxNo";
            this.lblBoxNo.Size = new System.Drawing.Size(76, 33);
            this.lblBoxNo.TabIndex = 0;
            this.lblBoxNo.Text = "BOX :";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage4.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage4.Controls.Add(this.btnPackingCancel);
            this.xtraTabPage4.Controls.Add(this.lblBox);
            this.xtraTabPage4.Controls.Add(this.lookUpEditBox);
            this.xtraTabPage4.Controls.Add(this.btnBoxMove);
            this.xtraTabPage4.Controls.Add(this.lookUpEditWeekBox2);
            this.xtraTabPage4.Controls.Add(this.lblWeek);
            this.xtraTabPage4.Controls.Add(this.lookUpEditSelect);
            this.xtraTabPage4.Controls.Add(this.lookUpEditWeekBox3);
            this.xtraTabPage4.Controls.Add(this.labelControl15);
            this.xtraTabPage4.Controls.Add(this.dateEditBox2);
            this.xtraTabPage4.Controls.Add(this.labelControl16);
            this.xtraTabPage4.Controls.Add(this.gridControlPackingItem2);
            this.xtraTabPage4.Controls.Add(this.gridControlBox2);
            this.xtraTabPage4.Controls.Add(this.labelControl17);
            this.xtraTabPage4.Controls.Add(this.labelControl18);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1338, 669);
            this.xtraTabPage4.Text = "出庫状況";
            // 
            // btnPackingCancel
            // 
            this.btnPackingCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPackingCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPackingCancel.Appearance.Options.UseFont = true;
            this.btnPackingCancel.Location = new System.Drawing.Point(214, 636);
            this.btnPackingCancel.Name = "btnPackingCancel";
            this.btnPackingCancel.Size = new System.Drawing.Size(84, 30);
            this.btnPackingCancel.TabIndex = 132;
            this.btnPackingCancel.Text = "保存";
            this.btnPackingCancel.Visible = false;
            this.btnPackingCancel.Click += new System.EventHandler(this.btnPackingCancel_Click);
            // 
            // lblBox
            // 
            this.lblBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBox.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBox.Appearance.Options.UseFont = true;
            this.lblBox.Location = new System.Drawing.Point(428, 640);
            this.lblBox.Name = "lblBox";
            this.lblBox.Size = new System.Drawing.Size(89, 23);
            this.lblBox.TabIndex = 131;
            this.lblBox.Text = "パッキン :";
            this.lblBox.Visible = false;
            // 
            // lookUpEditBox
            // 
            this.lookUpEditBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lookUpEditBox.Location = new System.Drawing.Point(523, 637);
            this.lookUpEditBox.Name = "lookUpEditBox";
            this.lookUpEditBox.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditBox.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditBox.Size = new System.Drawing.Size(111, 30);
            this.lookUpEditBox.TabIndex = 130;
            this.lookUpEditBox.Visible = false;
            // 
            // btnBoxMove
            // 
            this.btnBoxMove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBoxMove.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBoxMove.Appearance.Options.UseFont = true;
            this.btnBoxMove.Location = new System.Drawing.Point(653, 637);
            this.btnBoxMove.Name = "btnBoxMove";
            this.btnBoxMove.Size = new System.Drawing.Size(84, 30);
            this.btnBoxMove.TabIndex = 129;
            this.btnBoxMove.Text = "保存";
            this.btnBoxMove.Visible = false;
            this.btnBoxMove.Click += new System.EventHandler(this.btnBoxMove_Click);
            // 
            // lookUpEditWeekBox2
            // 
            this.lookUpEditWeekBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lookUpEditWeekBox2.Location = new System.Drawing.Point(267, 636);
            this.lookUpEditWeekBox2.Name = "lookUpEditWeekBox2";
            this.lookUpEditWeekBox2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditWeekBox2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditWeekBox2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditWeekBox2.Properties.DropDownRows = 2;
            this.lookUpEditWeekBox2.Properties.NullText = "";
            this.lookUpEditWeekBox2.Size = new System.Drawing.Size(140, 30);
            this.lookUpEditWeekBox2.TabIndex = 128;
            this.lookUpEditWeekBox2.Visible = false;
            // 
            // lblWeek
            // 
            this.lblWeek.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblWeek.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeek.Appearance.Options.UseFont = true;
            this.lblWeek.Location = new System.Drawing.Point(214, 639);
            this.lblWeek.Name = "lblWeek";
            this.lblWeek.Size = new System.Drawing.Size(51, 23);
            this.lblWeek.TabIndex = 127;
            this.lblWeek.Text = "回数 :";
            this.lblWeek.Visible = false;
            // 
            // lookUpEditSelect
            // 
            this.lookUpEditSelect.AllowDrop = true;
            this.lookUpEditSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lookUpEditSelect.Location = new System.Drawing.Point(3, 636);
            this.lookUpEditSelect.Name = "lookUpEditSelect";
            this.lookUpEditSelect.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditSelect.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditSelect.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditSelect.Size = new System.Drawing.Size(205, 30);
            this.lookUpEditSelect.TabIndex = 126;
            this.lookUpEditSelect.EditValueChanged += new System.EventHandler(this.lookUpEditSelect_EditValueChanged);
            // 
            // lookUpEditWeekBox3
            // 
            this.lookUpEditWeekBox3.Location = new System.Drawing.Point(489, 9);
            this.lookUpEditWeekBox3.Name = "lookUpEditWeekBox3";
            this.lookUpEditWeekBox3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditWeekBox3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditWeekBox3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditWeekBox3.Properties.DropDownRows = 2;
            this.lookUpEditWeekBox3.Properties.NullText = "";
            this.lookUpEditWeekBox3.Size = new System.Drawing.Size(140, 30);
            this.lookUpEditWeekBox3.TabIndex = 123;
            this.lookUpEditWeekBox3.EditValueChanged += new System.EventHandler(this.lookUpEditWeekBox3_EditValueChanged);
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(436, 12);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(51, 23);
            this.labelControl15.TabIndex = 122;
            this.labelControl15.Text = "回数 :";
            // 
            // dateEditBox2
            // 
            this.dateEditBox2.EditValue = null;
            this.dateEditBox2.Location = new System.Drawing.Point(227, 9);
            this.dateEditBox2.Name = "dateEditBox2";
            this.dateEditBox2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEditBox2.Properties.Appearance.Options.UseFont = true;
            this.dateEditBox2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBox2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBox2.Properties.Mask.EditMask = "y";
            this.dateEditBox2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dateEditBox2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditBox2.Properties.VistaCalendarInitialViewStyle = DevExpress.XtraEditors.VistaCalendarInitialViewStyle.YearView;
            this.dateEditBox2.Properties.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearView;
            this.dateEditBox2.Size = new System.Drawing.Size(140, 30);
            this.dateEditBox2.TabIndex = 121;
            this.dateEditBox2.EditValueChanged += new System.EventHandler(this.dateEditBox2_EditValueChanged);
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Location = new System.Drawing.Point(151, 12);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(70, 23);
            this.labelControl16.TabIndex = 120;
            this.labelControl16.Text = "出庫日 :";
            // 
            // gridControlPackingItem2
            // 
            this.gridControlPackingItem2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlPackingItem2.Location = new System.Drawing.Point(93, 51);
            this.gridControlPackingItem2.MainView = this.gridViewPackingItem2;
            this.gridControlPackingItem2.Name = "gridControlPackingItem2";
            this.gridControlPackingItem2.Size = new System.Drawing.Size(1242, 579);
            this.gridControlPackingItem2.TabIndex = 119;
            this.gridControlPackingItem2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPackingItem2});
            // 
            // gridViewPackingItem2
            // 
            this.gridViewPackingItem2.GridControl = this.gridControlPackingItem2;
            this.gridViewPackingItem2.Name = "gridViewPackingItem2";
            this.gridViewPackingItem2.OptionsFilter.AllowFilterEditor = false;
            this.gridViewPackingItem2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewPackingItem2.OptionsView.ShowGroupPanel = false;
            this.gridViewPackingItem2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewPackingItem2_RowCellStyle);
            this.gridViewPackingItem2.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewPackingItem2_ShowingEditor);
            // 
            // gridControlBox2
            // 
            this.gridControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gridControlBox2.Location = new System.Drawing.Point(11, 51);
            this.gridControlBox2.MainView = this.gridViewBox2;
            this.gridControlBox2.Name = "gridControlBox2";
            this.gridControlBox2.Size = new System.Drawing.Size(76, 579);
            this.gridControlBox2.TabIndex = 118;
            this.gridControlBox2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBox2});
            // 
            // gridViewBox2
            // 
            this.gridViewBox2.GridControl = this.gridControlBox2;
            this.gridViewBox2.Name = "gridViewBox2";
            this.gridViewBox2.OptionsView.ShowGroupPanel = false;
            this.gridViewBox2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewBox2_FocusedRowChanged);
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.Location = new System.Drawing.Point(1038, 2);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(219, 39);
            this.labelControl17.TabIndex = 117;
            this.labelControl17.Text = "入庫予定総数 :";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Location = new System.Drawing.Point(11, 12);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(76, 33);
            this.labelControl18.TabIndex = 116;
            this.labelControl18.Text = "BOX :";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage1.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage1.AutoScroll = true;
            this.xtraTabPage1.Controls.Add(this.xtraScrollableControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1338, 669);
            this.xtraTabPage1.TabPageWidth = 160;
            this.xtraTabPage1.Text = "バーコードスキャン";
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.labelControl9);
            this.xtraScrollableControl1.Controls.Add(this.labelControl11);
            this.xtraScrollableControl1.Controls.Add(this.labelControl12);
            this.xtraScrollableControl1.Controls.Add(this.labelControl13);
            this.xtraScrollableControl1.Controls.Add(this.picProductImg);
            this.xtraScrollableControl1.Controls.Add(this.goods_name);
            this.xtraScrollableControl1.Controls.Add(this.brand_name);
            this.xtraScrollableControl1.Controls.Add(this.size);
            this.xtraScrollableControl1.Controls.Add(this.color);
            this.xtraScrollableControl1.Controls.Add(this.groupControl1);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1338, 669);
            this.xtraScrollableControl1.TabIndex = 19;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(415, 528);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(144, 58);
            this.labelControl9.TabIndex = 30;
            this.labelControl9.Text = "商品名";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(415, 445);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(192, 58);
            this.labelControl11.TabIndex = 29;
            this.labelControl11.Text = "ブランド";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(415, 694);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(144, 58);
            this.labelControl12.TabIndex = 28;
            this.labelControl12.Text = "サイズ";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(415, 611);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(135, 58);
            this.labelControl13.TabIndex = 27;
            this.labelControl13.Text = "カラー";
            // 
            // picProductImg
            // 
            this.picProductImg.Location = new System.Drawing.Point(3, 445);
            this.picProductImg.Name = "picProductImg";
            this.picProductImg.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.picProductImg.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picProductImg.Size = new System.Drawing.Size(406, 478);
            this.picProductImg.TabIndex = 26;
            // 
            // goods_name
            // 
            this.goods_name.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goods_name.Appearance.Options.UseFont = true;
            this.goods_name.Location = new System.Drawing.Point(628, 528);
            this.goods_name.Name = "goods_name";
            this.goods_name.Size = new System.Drawing.Size(0, 58);
            this.goods_name.TabIndex = 25;
            // 
            // brand_name
            // 
            this.brand_name.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brand_name.Appearance.Options.UseFont = true;
            this.brand_name.Location = new System.Drawing.Point(628, 445);
            this.brand_name.Name = "brand_name";
            this.brand_name.Size = new System.Drawing.Size(0, 58);
            this.brand_name.TabIndex = 24;
            // 
            // size
            // 
            this.size.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.size.Appearance.Options.UseFont = true;
            this.size.Location = new System.Drawing.Point(628, 694);
            this.size.Name = "size";
            this.size.Size = new System.Drawing.Size(0, 58);
            this.size.TabIndex = 23;
            // 
            // color
            // 
            this.color.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.color.Appearance.Options.UseFont = true;
            this.color.Location = new System.Drawing.Point(628, 611);
            this.color.Name = "color";
            this.color.Size = new System.Drawing.Size(0, 58);
            this.color.TabIndex = 22;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.btn_print2);
            this.groupControl1.Controls.Add(this.txt_barcode);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.btn_print);
            this.groupControl1.Controls.Add(this.btn_select);
            this.groupControl1.Controls.Add(this.txt_optno);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txt_goodsno);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1264, 436);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "入力";
            // 
            // btn_print2
            // 
            this.btn_print2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_print2.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print2.Appearance.Options.UseFont = true;
            this.btn_print2.Location = new System.Drawing.Point(901, 347);
            this.btn_print2.Name = "btn_print2";
            this.btn_print2.Size = new System.Drawing.Size(358, 84);
            this.btn_print2.TabIndex = 19;
            this.btn_print2.Text = "価格(無)";
            this.btn_print2.Click += new System.EventHandler(this.Btn_print2_Click);
            // 
            // txt_barcode
            // 
            this.txt_barcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_barcode.Location = new System.Drawing.Point(466, 24);
            this.txt_barcode.Name = "txt_barcode";
            this.txt_barcode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_barcode.Properties.Appearance.Options.UseFont = true;
            this.txt_barcode.Size = new System.Drawing.Size(793, 84);
            this.txt_barcode.TabIndex = 0;
            this.txt_barcode.EditValueChanged += new System.EventHandler(this.txt_barcode_EditValueChanged);
            this.txt_barcode.Enter += new System.EventHandler(this.txt_barcode_Enter);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(12, 24);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(296, 77);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "バーコード";
            // 
            // btn_print
            // 
            this.btn_print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_print.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print.Appearance.Options.UseFont = true;
            this.btn_print.Location = new System.Drawing.Point(537, 347);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(358, 84);
            this.btn_print.TabIndex = 4;
            this.btn_print.Text = "価格(有)";
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // btn_select
            // 
            this.btn_select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_select.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_select.Appearance.Options.UseFont = true;
            this.btn_select.Location = new System.Drawing.Point(173, 347);
            this.btn_select.Name = "btn_select";
            this.btn_select.Size = new System.Drawing.Size(358, 84);
            this.btn_select.TabIndex = 3;
            this.btn_select.Text = "クリア";
            this.btn_select.Click += new System.EventHandler(this.btn_select_Click);
            // 
            // txt_optno
            // 
            this.txt_optno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_optno.Enabled = false;
            this.txt_optno.Location = new System.Drawing.Point(466, 204);
            this.txt_optno.Name = "txt_optno";
            this.txt_optno.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_optno.Properties.Appearance.Options.UseFont = true;
            this.txt_optno.Size = new System.Drawing.Size(793, 84);
            this.txt_optno.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(12, 204);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(448, 77);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "オプション番号";
            // 
            // txt_goodsno
            // 
            this.txt_goodsno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_goodsno.Enabled = false;
            this.txt_goodsno.Location = new System.Drawing.Point(466, 114);
            this.txt_goodsno.Name = "txt_goodsno";
            this.txt_goodsno.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_goodsno.Properties.Appearance.Options.UseFont = true;
            this.txt_goodsno.Size = new System.Drawing.Size(793, 84);
            this.txt_goodsno.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 114);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(256, 77);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "商品番号";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage5.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage5.Controls.Add(this.btnClear);
            this.xtraTabPage5.Controls.Add(this.btnInit);
            this.xtraTabPage5.Controls.Add(this.btnStore2);
            this.xtraTabPage5.Controls.Add(this.btnCancel2);
            this.xtraTabPage5.Controls.Add(this.groupControl4);
            this.xtraTabPage5.Controls.Add(this.gridControlItem2);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1338, 669);
            this.xtraTabPage5.Text = "その他の入庫";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Appearance.Options.UseFont = true;
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(1221, 74);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(114, 53);
            this.btnClear.TabIndex = 23;
            this.btnClear.Text = "初期化";
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // btnInit
            // 
            this.btnInit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInit.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInit.Appearance.Options.UseFont = true;
            this.btnInit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInit.ImageOptions.Image")));
            this.btnInit.Location = new System.Drawing.Point(1362, 74);
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(114, 53);
            this.btnInit.TabIndex = 22;
            this.btnInit.Text = "初期化";
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // btnStore2
            // 
            this.btnStore2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStore2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStore2.Appearance.Options.UseFont = true;
            this.btnStore2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnStore2.ImageOptions.Image")));
            this.btnStore2.Location = new System.Drawing.Point(912, 74);
            this.btnStore2.Name = "btnStore2";
            this.btnStore2.Size = new System.Drawing.Size(114, 53);
            this.btnStore2.TabIndex = 21;
            this.btnStore2.Text = "入庫保存";
            this.btnStore2.Click += new System.EventHandler(this.btnStore2_Click);
            // 
            // btnCancel2
            // 
            this.btnCancel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel2.Appearance.Options.UseFont = true;
            this.btnCancel2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel2.ImageOptions.Image")));
            this.btnCancel2.Location = new System.Drawing.Point(1032, 74);
            this.btnCancel2.Name = "btnCancel2";
            this.btnCancel2.Size = new System.Drawing.Size(183, 53);
            this.btnCancel2.TabIndex = 20;
            this.btnCancel2.Text = "入庫キャンセル";
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.lblTotalExpectedCnt2);
            this.groupControl4.Controls.Add(this.labelControl20);
            this.groupControl4.Controls.Add(this.lblBrandCnt2);
            this.groupControl4.Controls.Add(this.labelControl22);
            this.groupControl4.Controls.Add(this.btnQC2);
            this.groupControl4.Controls.Add(this.txtBarcode2);
            this.groupControl4.Controls.Add(this.labelControl24);
            this.groupControl4.Location = new System.Drawing.Point(3, 3);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(1332, 65);
            this.groupControl4.TabIndex = 13;
            this.groupControl4.Text = "照会条件";
            // 
            // lblTotalExpectedCnt2
            // 
            this.lblTotalExpectedCnt2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalExpectedCnt2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalExpectedCnt2.Appearance.Options.UseFont = true;
            this.lblTotalExpectedCnt2.Location = new System.Drawing.Point(1305, 32);
            this.lblTotalExpectedCnt2.Name = "lblTotalExpectedCnt2";
            this.lblTotalExpectedCnt2.Size = new System.Drawing.Size(0, 23);
            this.lblTotalExpectedCnt2.TabIndex = 115;
            // 
            // labelControl20
            // 
            this.labelControl20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl20.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.Location = new System.Drawing.Point(1157, 31);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(127, 23);
            this.labelControl20.TabIndex = 114;
            this.labelControl20.Text = "スキャン総数 :";
            this.labelControl20.Visible = false;
            // 
            // lblBrandCnt2
            // 
            this.lblBrandCnt2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBrandCnt2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrandCnt2.Appearance.Options.UseFont = true;
            this.lblBrandCnt2.Location = new System.Drawing.Point(1047, 31);
            this.lblBrandCnt2.Name = "lblBrandCnt2";
            this.lblBrandCnt2.Size = new System.Drawing.Size(0, 23);
            this.lblBrandCnt2.TabIndex = 113;
            this.lblBrandCnt2.Visible = false;
            // 
            // labelControl22
            // 
            this.labelControl22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl22.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.Location = new System.Drawing.Point(946, 30);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(108, 23);
            this.labelControl22.TabIndex = 112;
            this.labelControl22.Text = "ブランド数 :";
            this.labelControl22.Visible = false;
            // 
            // btnQC2
            // 
            this.btnQC2.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQC2.Appearance.Options.UseFont = true;
            this.btnQC2.Location = new System.Drawing.Point(354, 28);
            this.btnQC2.Name = "btnQC2";
            this.btnQC2.Size = new System.Drawing.Size(85, 30);
            this.btnQC2.TabIndex = 6;
            this.btnQC2.Text = "スキャン";
            this.btnQC2.Click += new System.EventHandler(this.btnQC2_Click);
            // 
            // txtBarcode2
            // 
            this.txtBarcode2.Location = new System.Drawing.Point(157, 28);
            this.txtBarcode2.Name = "txtBarcode2";
            this.txtBarcode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode2.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode2.Size = new System.Drawing.Size(191, 30);
            this.txtBarcode2.TabIndex = 5;
            this.txtBarcode2.EditValueChanged += new System.EventHandler(this.txtBarcode2_EditValueChanged);
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.Location = new System.Drawing.Point(13, 31);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(89, 23);
            this.labelControl24.TabIndex = 4;
            this.labelControl24.Text = "入庫商品 :";
            // 
            // gridControlItem2
            // 
            this.gridControlItem2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlItem2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlItem2.Location = new System.Drawing.Point(3, 133);
            this.gridControlItem2.MainView = this.gridViewItem2;
            this.gridControlItem2.Name = "gridControlItem2";
            this.gridControlItem2.Size = new System.Drawing.Size(1332, 529);
            this.gridControlItem2.TabIndex = 12;
            this.gridControlItem2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewItem2});
            // 
            // gridViewItem2
            // 
            this.gridViewItem2.GridControl = this.gridControlItem2;
            this.gridViewItem2.Name = "gridViewItem2";
            this.gridViewItem2.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewItem2.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewItem2.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewItem2.OptionsFilter.AllowFilterEditor = false;
            this.gridViewItem2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewItem2.OptionsView.ShowGroupPanel = false;
            this.gridViewItem2.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewItem2_RowCellStyle);
            // 
            // JapanPrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 736);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JapanPrintForm";
            this.Text = "Konvini Label Print";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PrintFormJapan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPrinter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeek.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcodeBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeekBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackingItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackingItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBox)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeekBox2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditSelect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditWeekBox3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBox2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackingItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackingItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBox2)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProductImg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_barcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_optno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_goodsno.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.LabelControl labelControlPrinter;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPrinter;
        private DevExpress.XtraEditors.DateEdit dateEdit;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditWeek;
        private DevExpress.XtraEditors.SimpleButton btnQC;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtBarcode;
        private DevExpress.XtraEditors.LabelControl lblBrandCnt;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraGrid.GridControl gridControlItem;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewItem;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnStore;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.LabelControl lblBoxNo;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridControl gridControlBox;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBox;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LabelControl lblBoxItemCnt;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditWeekBox;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.DateEdit dateEditBox;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.SimpleButton btnBoxCancel;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditWeekBox3;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.DateEdit dateEditBox2;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraGrid.GridControl gridControlPackingItem2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPackingItem2;
        private DevExpress.XtraGrid.GridControl gridControlBox2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBox2;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.SimpleButton btnPackingCancel;
        private DevExpress.XtraEditors.LabelControl lblBox;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditBox;
        private DevExpress.XtraEditors.SimpleButton btnBoxMove;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditWeekBox2;
        private DevExpress.XtraEditors.LabelControl lblWeek;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditSelect;
        private DevExpress.XtraEditors.SimpleButton btnScan;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtBarcodeBox;
        private DevExpress.XtraGrid.GridControl gridControlPackingItem;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPackingItem;
        private DevExpress.XtraEditors.SimpleButton btnStore2;
        private DevExpress.XtraEditors.SimpleButton btnCancel2;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.LabelControl lblTotalExpectedCnt2;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl lblBrandCnt2;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.SimpleButton btnQC2;
        private DevExpress.XtraEditors.TextEdit txtBarcode2;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraGrid.GridControl gridControlItem2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewItem2;
        private DevExpress.XtraEditors.SimpleButton btnInit;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txt_barcode;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btn_print;
        private DevExpress.XtraEditors.SimpleButton btn_select;
        private DevExpress.XtraEditors.TextEdit txt_optno;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt_goodsno;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.PictureEdit picProductImg;
        private DevExpress.XtraEditors.LabelControl goods_name;
        private DevExpress.XtraEditors.LabelControl brand_name;
        private DevExpress.XtraEditors.LabelControl size;
        private DevExpress.XtraEditors.LabelControl color;
        private DevExpress.XtraEditors.SimpleButton btn_print2;
        private DevExpress.XtraEditors.SimpleButton btnClear;
    }
}

