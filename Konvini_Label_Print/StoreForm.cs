﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace Konvini_Label_Print
{
    public partial class StorePopup : Form
    {
        string shippingno;
        int goodsno;
        int optno;
        string barcode;
        int ea;
        int store_ea;
        int max_ea;
        string type;
        string reg_admin;
        string server;

        public PrintForm parentForm;

        public StorePopup()
        {
            InitializeComponent();
        }

        public StorePopup(string pSHIPPINGNO, string pGOODSNO, string pOPTNO, string pBARCODE, string pEA, string pSTORE_EA, string pTYPE, string pREG_ADMIN, string pSERVER)
        {
            InitializeComponent();

            shippingno = pSHIPPINGNO;
            goodsno = Common.Method.getInt(pGOODSNO);
            optno = Common.Method.getInt(pOPTNO);
            barcode = pBARCODE;
            ea = Common.Method.getInt(pEA);
            store_ea = Common.Method.getInt(pSTORE_EA);
            max_ea = ea - store_ea;
            type = pTYPE;
            reg_admin = pREG_ADMIN;
            server = pSERVER;

            txtStoreCnt.Text = Common.Method.getStr(max_ea);
        }

        private void simpleButtonStore_Click(object sender, EventArgs e)
        {
            if (txtStoreCnt.Text != "")
            {
                Store();
            }
        }

        private void Store()
        {
            Cursor.Current = Cursors.WaitCursor;
            int storeCnt = Common.Method.getInt(txtStoreCnt.Text);

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pSHIPPINGNO", shippingno),
                    new MySqlParameter("@pGOODSNO", goodsno),
                    new MySqlParameter("@pOPTNO", optno),
                    new MySqlParameter("@pBARCODE", barcode),
                    new MySqlParameter("@pEA", storeCnt),
                    new MySqlParameter("@pSTORE_TYPE", type),
                    new MySqlParameter("@pREG_ADMIN", reg_admin)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_SET_STORE", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    parentForm.GetEA(txtStoreCnt.Text);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    this.DialogResult = DialogResult.No;
                }
            }
            catch (Exception ex)
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void txtStoreCnt_Enter(object sender, EventArgs e)
        {
            if (txtStoreCnt.Text != "")
            {
                //Store();
            }
        }

        private void txtStoreCnt_EditValueChanged(object sender, EventArgs e)
        {
            int storeCnt = Common.Method.getInt(this.txtStoreCnt.Text);

            if(storeCnt > max_ea)
            {
                txtStoreCnt.EditValueChanged -= txtStoreCnt_EditValueChanged;
                MessageBox.Show("최대 입고 가능한 갯수를 초과하였습니다.", "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtStoreCnt.Text = Common.Method.getStr(max_ea);
                txtStoreCnt.Refresh();
                txtStoreCnt.EditValueChanged += txtStoreCnt_EditValueChanged;
            }
        }
    }
}
