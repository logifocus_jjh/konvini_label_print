﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Konvini_Label_Print
{
    public partial class SelectRegion : Form
    {
        string region = "";

        public SelectRegion()
        {
            InitializeComponent();

            SetLookupEdit();

            region = Common.Method.GetRegisterKey(@"SOFTWARE\Logifocus\Konvini", "REGION");

            if (region != "")
            {
                lookUpEditRegion.EditValue = region;
            }
            
        }

        private void SetLookupEdit()
        {
            DataTable DT = new DataTable();

            DT.Columns.Add("CODE");
            DT.Columns.Add("CODE_NAME");

            DataRow DR1 = DT.NewRow();

            DR1["CODE"] = "KOREA";
            DR1["CODE_NAME"] = "한국";

            DataRow DR2 = DT.NewRow();

            DR2["CODE"] = "JAPAN";
            DR2["CODE_NAME"] = "日本";

            DT.Rows.Add(DR1);
            DT.Rows.Add(DR2);

            Common.Method.SetLookupEdit(lookUpEditRegion, DT);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string region = Common.Method.getStr(lookUpEditRegion.EditValue);
            string server = Common.Method.getStr(chkSelectServer.EditValue);

            if(region == "KOREA")
            {
                Common.Method.SetRegisterKey(@"SOFTWARE\Logifocus\Konvini", "REGION", Common.Method.getStr(lookUpEditRegion.EditValue));

                PrintForm form = new PrintForm(server);
                this.WindowState = FormWindowState.Minimized;
                form.WindowState = FormWindowState.Maximized;
                form.TopLevel = true;
                form.ShowDialog();
                this.Close();
            }
            else if(region == "JAPAN")
            {
                Common.Method.SetRegisterKey(@"SOFTWARE\Logifocus\Konvini", "REGION", Common.Method.getStr(lookUpEditRegion.EditValue));

                JapanPrintForm form = new JapanPrintForm(server);
                this.WindowState = FormWindowState.Minimized;
                form.WindowState = FormWindowState.Maximized;
                form.TopLevel = true;
                form.ShowDialog();
                this.Close();
            }
            else
            {
                Common.Method.SetRegisterKey(@"SOFTWARE\Logifocus\Konvini", "REGION", Common.Method.getStr(lookUpEditRegion.EditValue));

                PrintForm form = new PrintForm(server);
                form.ShowDialog();
                form.TopLevel = true;
                this.Close();
            }
        }
    }
}
