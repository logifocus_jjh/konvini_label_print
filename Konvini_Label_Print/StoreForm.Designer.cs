﻿namespace Konvini_Label_Print
{
    partial class StorePopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StorePopup));
            this.txtStoreCnt = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonStore = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreCnt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtStoreCnt
            // 
            this.txtStoreCnt.Location = new System.Drawing.Point(110, 12);
            this.txtStoreCnt.Name = "txtStoreCnt";
            this.txtStoreCnt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStoreCnt.Properties.Appearance.Options.UseFont = true;
            this.txtStoreCnt.Properties.Mask.EditMask = "n0";
            this.txtStoreCnt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtStoreCnt.Size = new System.Drawing.Size(100, 64);
            this.txtStoreCnt.TabIndex = 0;
            this.txtStoreCnt.EditValueChanged += new System.EventHandler(this.txtStoreCnt_EditValueChanged);
            this.txtStoreCnt.Enter += new System.EventHandler(this.txtStoreCnt_Enter);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(92, 29);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "입고 수량";
            // 
            // simpleButtonStore
            // 
            this.simpleButtonStore.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonStore.Appearance.Options.UseFont = true;
            this.simpleButtonStore.Location = new System.Drawing.Point(12, 82);
            this.simpleButtonStore.Name = "simpleButtonStore";
            this.simpleButtonStore.Size = new System.Drawing.Size(198, 49);
            this.simpleButtonStore.TabIndex = 2;
            this.simpleButtonStore.Text = "입고 처리";
            this.simpleButtonStore.Click += new System.EventHandler(this.simpleButtonStore_Click);
            // 
            // StorePopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(220, 142);
            this.Controls.Add(this.simpleButtonStore);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtStoreCnt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StorePopup";
            this.Text = "입고";
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreCnt.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtStoreCnt;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStore;
    }
}