﻿namespace Konvini_Label_Print
{
    partial class PrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintForm));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlPrinter = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEditPrinter = new DevExpress.XtraEditors.LookUpEdit();
            this.gridControlOrder = new DevExpress.XtraGrid.GridControl();
            this.gridViewOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.txtShippingBarcode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlReceipt = new DevExpress.XtraGrid.GridControl();
            this.gridViewReceipt = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.lblStoreExpectedCnt = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonMemo = new DevExpress.XtraEditors.SimpleButton();
            this.memoComment = new DevExpress.XtraEditors.MemoEdit();
            this.lblShipmentDate = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lblBrandName = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lblShipper = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lblTrackingNo = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.picProductImg = new DevExpress.XtraEditors.PictureEdit();
            this.goods_name = new DevExpress.XtraEditors.LabelControl();
            this.brand_name = new DevExpress.XtraEditors.LabelControl();
            this.size = new DevExpress.XtraEditors.LabelControl();
            this.color = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txt_barcode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btn_print = new DevExpress.XtraEditors.SimpleButton();
            this.btn_select = new DevExpress.XtraEditors.SimpleButton();
            this.txt_optno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt_goodsno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.mixturerate = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPrinter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShippingBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoComment.Properties)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProductImg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_barcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_optno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_goodsno.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1481, 1001);
            this.xtraTabControl1.TabIndex = 17;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage1});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage2.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage2.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage2.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage2.Controls.Add(this.labelControlPrinter);
            this.xtraTabPage2.Controls.Add(this.lookUpEditPrinter);
            this.xtraTabPage2.Controls.Add(this.gridControlOrder);
            this.xtraTabPage2.Controls.Add(this.btnSearch);
            this.xtraTabPage2.Controls.Add(this.txtShippingBarcode);
            this.xtraTabPage2.Controls.Add(this.labelControl8);
            this.xtraTabPage2.Controls.Add(this.gridControlReceipt);
            this.xtraTabPage2.Controls.Add(this.groupControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1479, 958);
            this.xtraTabPage2.TabPageWidth = 160;
            this.xtraTabPage2.Text = "입고송장스캔";
            // 
            // labelControlPrinter
            // 
            this.labelControlPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControlPrinter.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlPrinter.Appearance.Options.UseFont = true;
            this.labelControlPrinter.Location = new System.Drawing.Point(1123, 11);
            this.labelControlPrinter.Name = "labelControlPrinter";
            this.labelControlPrinter.Size = new System.Drawing.Size(104, 23);
            this.labelControlPrinter.TabIndex = 8;
            this.labelControlPrinter.Text = "프린터 선택 :";
            // 
            // lookUpEditPrinter
            // 
            this.lookUpEditPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEditPrinter.Location = new System.Drawing.Point(1233, 8);
            this.lookUpEditPrinter.Name = "lookUpEditPrinter";
            this.lookUpEditPrinter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEditPrinter.Properties.Appearance.Options.UseFont = true;
            this.lookUpEditPrinter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditPrinter.Size = new System.Drawing.Size(235, 30);
            this.lookUpEditPrinter.TabIndex = 6;
            // 
            // gridControlOrder
            // 
            this.gridControlOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlOrder.Location = new System.Drawing.Point(1323, 178);
            this.gridControlOrder.MainView = this.gridViewOrder;
            this.gridControlOrder.Name = "gridControlOrder";
            this.gridControlOrder.Size = new System.Drawing.Size(145, 769);
            this.gridControlOrder.TabIndex = 5;
            this.gridControlOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOrder});
            // 
            // gridViewOrder
            // 
            this.gridViewOrder.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewOrder.Appearance.Row.Options.UseFont = true;
            this.gridViewOrder.GridControl = this.gridControlOrder;
            this.gridViewOrder.Name = "gridViewOrder";
            this.gridViewOrder.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewOrder.OptionsView.ShowGroupPanel = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(381, 17);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "검색";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtShippingBarcode
            // 
            this.txtShippingBarcode.Location = new System.Drawing.Point(160, 18);
            this.txtShippingBarcode.Name = "txtShippingBarcode";
            this.txtShippingBarcode.Size = new System.Drawing.Size(215, 20);
            this.txtShippingBarcode.TabIndex = 3;
            this.txtShippingBarcode.EditValueChanged += new System.EventHandler(this.txtShippingBarcode_EditValueChanged);
            this.txtShippingBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtShippingBarcode_KeyPress);
            this.txtShippingBarcode.Leave += new System.EventHandler(this.txtShippingBarcode_Leave);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(16, 15);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(138, 23);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "국내송장 바코드 :";
            // 
            // gridControlReceipt
            // 
            this.gridControlReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlReceipt.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlReceipt.Location = new System.Drawing.Point(11, 178);
            this.gridControlReceipt.MainView = this.gridViewReceipt;
            this.gridControlReceipt.Name = "gridControlReceipt";
            this.gridControlReceipt.Size = new System.Drawing.Size(1306, 769);
            this.gridControlReceipt.TabIndex = 1;
            this.gridControlReceipt.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewReceipt});
            // 
            // gridViewReceipt
            // 
            this.gridViewReceipt.GridControl = this.gridControlReceipt;
            this.gridViewReceipt.Name = "gridViewReceipt";
            this.gridViewReceipt.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewReceipt.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewReceipt.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewReceipt.OptionsView.ShowGroupPanel = false;
            this.gridViewReceipt.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridViewReceipt_RowCellClick);
            this.gridViewReceipt.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewReceipt_RowCellStyle);
            this.gridViewReceipt.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewReceipt_FocusedRowChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.lblStoreExpectedCnt);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.simpleButtonMemo);
            this.groupControl3.Controls.Add(this.memoComment);
            this.groupControl3.Controls.Add(this.lblShipmentDate);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.lblBrandName);
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Controls.Add(this.lblShipper);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.lblTrackingNo);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Location = new System.Drawing.Point(11, 44);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1457, 128);
            this.groupControl3.TabIndex = 0;
            this.groupControl3.Text = "송장 정보";
            // 
            // lblStoreExpectedCnt
            // 
            this.lblStoreExpectedCnt.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStoreExpectedCnt.Appearance.Options.UseFont = true;
            this.lblStoreExpectedCnt.Location = new System.Drawing.Point(1310, 30);
            this.lblStoreExpectedCnt.Name = "lblStoreExpectedCnt";
            this.lblStoreExpectedCnt.Size = new System.Drawing.Size(0, 23);
            this.lblStoreExpectedCnt.TabIndex = 11;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(1191, 30);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(115, 23);
            this.labelControl14.TabIndex = 10;
            this.labelControl14.Text = "입고예정수량 :";
            // 
            // simpleButtonMemo
            // 
            this.simpleButtonMemo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonMemo.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonMemo.Appearance.Options.UseFont = true;
            this.simpleButtonMemo.Location = new System.Drawing.Point(1325, 26);
            this.simpleButtonMemo.Name = "simpleButtonMemo";
            this.simpleButtonMemo.Size = new System.Drawing.Size(127, 97);
            this.simpleButtonMemo.TabIndex = 9;
            this.simpleButtonMemo.Text = "미분류 등록";
            this.simpleButtonMemo.Click += new System.EventHandler(this.simpleButtonMemo_Click);
            // 
            // memoComment
            // 
            this.memoComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoComment.Location = new System.Drawing.Point(5, 59);
            this.memoComment.Name = "memoComment";
            this.memoComment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoComment.Properties.Appearance.Options.UseFont = true;
            this.memoComment.Size = new System.Drawing.Size(1314, 64);
            this.memoComment.TabIndex = 8;
            // 
            // lblShipmentDate
            // 
            this.lblShipmentDate.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShipmentDate.Appearance.Options.UseFont = true;
            this.lblShipmentDate.Location = new System.Drawing.Point(937, 30);
            this.lblShipmentDate.Name = "lblShipmentDate";
            this.lblShipmentDate.Size = new System.Drawing.Size(0, 23);
            this.lblShipmentDate.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(833, 30);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(98, 23);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "업체출고일 :";
            // 
            // lblBrandName
            // 
            this.lblBrandName.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrandName.Appearance.Options.UseFont = true;
            this.lblBrandName.Location = new System.Drawing.Point(638, 30);
            this.lblBrandName.Name = "lblBrandName";
            this.lblBrandName.Size = new System.Drawing.Size(0, 23);
            this.lblBrandName.TabIndex = 5;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(486, 30);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(146, 23);
            this.labelControl6.TabIndex = 4;
            this.labelControl6.Text = "발송업체(브랜드) :";
            // 
            // lblShipper
            // 
            this.lblShipper.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShipper.Appearance.Options.UseFont = true;
            this.lblShipper.Location = new System.Drawing.Point(357, 30);
            this.lblShipper.Name = "lblShipper";
            this.lblShipper.Size = new System.Drawing.Size(0, 23);
            this.lblShipper.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(270, 30);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(81, 23);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "배송업체 :";
            // 
            // lblTrackingNo
            // 
            this.lblTrackingNo.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrackingNo.Appearance.Options.UseFont = true;
            this.lblTrackingNo.Location = new System.Drawing.Point(126, 30);
            this.lblTrackingNo.Name = "lblTrackingNo";
            this.lblTrackingNo.Size = new System.Drawing.Size(0, 23);
            this.lblTrackingNo.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 30);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(115, 23);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "업체송장번호 :";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.HeaderActive.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabPage1.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage1.Controls.Add(this.groupControl2);
            this.xtraTabPage1.Controls.Add(this.groupControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1479, 958);
            this.xtraTabPage1.TabPageWidth = 160;
            this.xtraTabPage1.Text = "상품바코드스캔";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.mixturerate);
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.picProductImg);
            this.groupControl2.Controls.Add(this.goods_name);
            this.groupControl2.Controls.Add(this.brand_name);
            this.groupControl2.Controls.Add(this.size);
            this.groupControl2.Controls.Add(this.color);
            this.groupControl2.Location = new System.Drawing.Point(10, 455);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1458, 492);
            this.groupControl2.TabIndex = 17;
            this.groupControl2.Text = "결과";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(374, 109);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(117, 58);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "상품명";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(374, 26);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(156, 58);
            this.labelControl11.TabIndex = 19;
            this.labelControl11.Text = "브랜드명";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(374, 275);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(117, 58);
            this.labelControl12.TabIndex = 18;
            this.labelControl12.Text = "사이즈";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(374, 192);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(78, 58);
            this.labelControl13.TabIndex = 17;
            this.labelControl13.Text = "컬러";
            // 
            // picProductImg
            // 
            this.picProductImg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.picProductImg.Location = new System.Drawing.Point(13, 26);
            this.picProductImg.Name = "picProductImg";
            this.picProductImg.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picProductImg.Size = new System.Drawing.Size(326, 460);
            this.picProductImg.TabIndex = 16;
            this.picProductImg.Click += new System.EventHandler(this.picProductImg_Click);
            // 
            // goods_name
            // 
            this.goods_name.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goods_name.Appearance.Options.UseFont = true;
            this.goods_name.Location = new System.Drawing.Point(587, 109);
            this.goods_name.Name = "goods_name";
            this.goods_name.Size = new System.Drawing.Size(0, 58);
            this.goods_name.TabIndex = 15;
            // 
            // brand_name
            // 
            this.brand_name.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brand_name.Appearance.Options.UseFont = true;
            this.brand_name.Location = new System.Drawing.Point(587, 26);
            this.brand_name.Name = "brand_name";
            this.brand_name.Size = new System.Drawing.Size(0, 58);
            this.brand_name.TabIndex = 13;
            // 
            // size
            // 
            this.size.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.size.Appearance.Options.UseFont = true;
            this.size.Location = new System.Drawing.Point(587, 275);
            this.size.Name = "size";
            this.size.Size = new System.Drawing.Size(0, 58);
            this.size.TabIndex = 12;
            // 
            // color
            // 
            this.color.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.color.Appearance.Options.UseFont = true;
            this.color.Location = new System.Drawing.Point(587, 192);
            this.color.Name = "color";
            this.color.Size = new System.Drawing.Size(0, 58);
            this.color.TabIndex = 11;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.txt_barcode);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.btn_print);
            this.groupControl1.Controls.Add(this.btn_select);
            this.groupControl1.Controls.Add(this.txt_optno);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txt_goodsno);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(11, 13);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1457, 436);
            this.groupControl1.TabIndex = 18;
            this.groupControl1.Text = "입력";
            // 
            // txt_barcode
            // 
            this.txt_barcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_barcode.Location = new System.Drawing.Point(325, 24);
            this.txt_barcode.Name = "txt_barcode";
            this.txt_barcode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_barcode.Properties.Appearance.Options.UseFont = true;
            this.txt_barcode.Size = new System.Drawing.Size(1127, 84);
            this.txt_barcode.TabIndex = 0;
            this.txt_barcode.EditValueChanged += new System.EventHandler(this.Txt_barcode_EditValueChanged);
            this.txt_barcode.Enter += new System.EventHandler(this.txt_barcode_Enter);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(12, 24);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(168, 77);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "바코드";
            // 
            // btn_print
            // 
            this.btn_print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_print.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print.Appearance.Options.UseFont = true;
            this.btn_print.Location = new System.Drawing.Point(1094, 344);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(358, 84);
            this.btn_print.TabIndex = 4;
            this.btn_print.Text = "출력";
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // btn_select
            // 
            this.btn_select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_select.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_select.Appearance.Options.UseFont = true;
            this.btn_select.Location = new System.Drawing.Point(730, 344);
            this.btn_select.Name = "btn_select";
            this.btn_select.Size = new System.Drawing.Size(358, 84);
            this.btn_select.TabIndex = 3;
            this.btn_select.Text = "다음 상품 조회";
            this.btn_select.Click += new System.EventHandler(this.btn_Select_Click);
            // 
            // txt_optno
            // 
            this.txt_optno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_optno.Enabled = false;
            this.txt_optno.Location = new System.Drawing.Point(325, 204);
            this.txt_optno.Name = "txt_optno";
            this.txt_optno.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_optno.Properties.Appearance.Options.UseFont = true;
            this.txt_optno.Size = new System.Drawing.Size(1127, 84);
            this.txt_optno.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(12, 204);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(224, 77);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "옵션번호";
            // 
            // txt_goodsno
            // 
            this.txt_goodsno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_goodsno.Enabled = false;
            this.txt_goodsno.Location = new System.Drawing.Point(325, 114);
            this.txt_goodsno.Name = "txt_goodsno";
            this.txt_goodsno.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_goodsno.Properties.Appearance.Options.UseFont = true;
            this.txt_goodsno.Size = new System.Drawing.Size(1127, 84);
            this.txt_goodsno.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 114);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(224, 77);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "상품번호";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // mixturerate
            // 
            this.mixturerate.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mixturerate.Appearance.Options.UseFont = true;
            this.mixturerate.Location = new System.Drawing.Point(383, 374);
            this.mixturerate.Name = "mixturerate";
            this.mixturerate.Size = new System.Drawing.Size(0, 58);
            this.mixturerate.TabIndex = 22;
            // 
            // PrintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1481, 1001);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrintForm";
            this.Text = "Konvini Label Print";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PrintForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditPrinter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShippingBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoComment.Properties)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picProductImg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_barcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_optno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_goodsno.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl goods_name;
        private DevExpress.XtraEditors.LabelControl brand_name;
        private DevExpress.XtraEditors.LabelControl size;
        private DevExpress.XtraEditors.LabelControl color;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txt_barcode;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btn_print;
        private DevExpress.XtraEditors.SimpleButton btn_select;
        private DevExpress.XtraEditors.TextEdit txt_optno;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt_goodsno;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControlReceipt;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReceipt;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl lblShipmentDate;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lblBrandName;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl lblShipper;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lblTrackingNo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.TextEdit txtShippingBarcode;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PictureEdit picProductImg;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMemo;
        private DevExpress.XtraEditors.MemoEdit memoComment;
        private DevExpress.XtraEditors.LabelControl lblStoreExpectedCnt;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraGrid.GridControl gridControlOrder;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOrder;
        private DevExpress.XtraEditors.LabelControl labelControlPrinter;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditPrinter;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.LabelControl mixturerate;
    }
}

