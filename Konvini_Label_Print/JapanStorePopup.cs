﻿using System;
using System.Windows.Forms;

namespace Konvini_Label_Print
{
    public partial class JapanStorePopup : Form
    {

        public JapanPrintForm parentForm;

        public JapanStorePopup()
        {
            InitializeComponent();
        }

        public JapanStorePopup(string qc)
        {
            InitializeComponent();

            txtCopyCnt.Text = qc;
        }

        private void simpleButtonPriceY_Click(object sender, EventArgs e)
        {
            int cnt = Common.Method.getInt(txtCopyCnt.Text);

            if ((txtCopyCnt.Text != "") || (cnt > 0))
            {
                parentForm.GetCopyCnt(cnt, true);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }

        private void SimpleButtonPriceN_Click(object sender, EventArgs e)
        {
            int cnt = Common.Method.getInt(txtCopyCnt.Text);

            if ((txtCopyCnt.Text != "") || (cnt > 0))
            {
                parentForm.GetCopyCnt(cnt);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                parentForm.GetCopyCnt(cnt);
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }
    }
}
