﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Konvini_Label_Print.Report;
using DevExpress.XtraReports.UI;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Collections;

namespace Konvini_Label_Print
{
    public partial class PrintForm : DevExpress.XtraEditors.XtraForm
    {
        int rowHandle = -1;
        int ea;
        string barcode = "";
        string shippingno;
        string server = "";

        public PrintForm(string pSERVER)
        {
            InitializeComponent();
            server = pSERVER;
            //ClearValues();
        }

        private void PrintForm_Load(object sender, EventArgs e)
        {
            CenterToScreen();
            SetupPrinterSet();

            SetGridViewOrderColumns();
        }

        private void ClearValues()
        {
            this.brand_name.Text = "";
            this.size.Text = "";
            this.color.Text = "";
            this.txt_goodsno.Text = "";
        }

        private void SetGridViewReceiptColumns(bool pIMAGE_YN)
        {
            Common.Method.GridViewRowNumber(gridViewReceipt);

            if(pIMAGE_YN)
            {
                Common.Method.setGridViewColumn(gridViewReceipt, "image", "이미지", 158, DevExpress.Utils.HorzAlignment.Center, true);
                Common.Method.setGridViewcolumnImage(gridControlReceipt, gridViewReceipt, "image");
                gridViewReceipt.Columns["image"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

                gridViewReceipt.RowHeight = 320;
            }
            else
            {
                gridViewReceipt.RowHeight = 100;
            }

            Common.Method.setGridViewColumn(gridViewReceipt, "type", "구분", 20, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewReceipt.Columns["type"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "goodsno", "상품번호", 60, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewReceipt.Columns["goodsno"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "goodsnm", "상품명", 300, DevExpress.Utils.HorzAlignment.Near, true);
            gridViewReceipt.Columns["goodsnm"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "opt1", "컬러", 150, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewReceipt.Columns["opt1"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "opt2", "사이즈", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewReceipt.Columns["opt2"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "mixturerate", "혼용율", 80, DevExpress.Utils.HorzAlignment.Center, false);
            gridViewReceipt.Columns["mixturerate"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "ea", "입고예정수량", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewReceipt.Columns["ea"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "store_ea", "입고수량", 80, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewReceipt.Columns["store_ea"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            Common.Method.setGridViewColumn(gridViewReceipt, "store", " ", 120, DevExpress.Utils.HorzAlignment.Center, true);
            gridViewReceipt.Columns["store"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            gridViewReceipt.OptionsBehavior.Editable = false;
        }

        private void SetGridViewOrderColumns()
        {
            Common.Method.setGridViewColumn(gridViewOrder, "ordno", "주문번호", 20, DevExpress.Utils.HorzAlignment.Near, true);
            gridViewOrder.Columns["ordno"].OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;

            gridViewReceipt.OptionsBehavior.Editable = false;
        }

        private void SelectData()
        {
            string url = "";
            string imageno = "";
            string color_name = "";
            string size_name = "";
            string mix_name = "";
            string optno = "";

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = txt_barcode.Text;

                if (barcode != "")
                {
                    txt_goodsno.Text = barcode.Substring(4, 6);
                    optno = barcode.Substring(10, 3);

                    if (optno.Substring(0, 1) == "0")
                    {
                        if (optno.Substring(1, 1) == "0")
                        {
                            txt_optno.Text = optno.Substring(2, 1);
                        }
                        else
                        {
                            txt_optno.Text = optno.Substring(1, 2);
                        }
                    }
                    else
                    {
                        txt_optno.Text = optno;
                    }
                }
                else
                {
                    barcode = "1" + string.Format("{0:D9}", this.txt_goodsno.Text) + string.Format("{0:D3}", this.txt_optno.Text);
                    this.txt_barcode.Text = barcode;
                }

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pGOODSNO", this.txt_goodsno.Text),
                    new MySqlParameter("@pOPTNO", this.txt_optno.Text)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_LIST_GOODSFORLABEL", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    mix_name = GetMixtureName(Common.Method.getStr(ResultDT.Rows[0]["mixtureRate"]), "KR");
                    color_name = GetCodeName(Common.Method.getStr(ResultDT.Rows[0]["opt1"]),"KR");
                    size_name = GetCodeName(Common.Method.getStr(ResultDT.Rows[0]["opt2"]),"KR");
                    barcode = txt_barcode.Text;
                    this.brand_name.Text = Common.Method.getStr(ResultDT.Rows[0]["brand"]);
                    this.goods_name.Text = Common.Method.getStr(ResultDT.Rows[0]["goodsnm"]);
                    this.color.Text = color_name;
                    this.size.Text = size_name;
                    this.txt_goodsno.Text = Common.Method.getStr(ResultDT.Rows[0]["goodsno"]);
                    this.mixturerate.Text = mix_name;
                    barcode = txt_barcode.Text;

                    imageno = GetImageNo(this.txt_goodsno.Text, this.txt_optno.Text);

                    if (imageno == "0")
                    {
                        url = "https://d2ncantiuancgo.cloudfront.net/goods/" + this.txt_goodsno.Text + ".jpg";
                    }
                    else
                    {
                        url = "https://d2ncantiuancgo.cloudfront.net/tgoods/" + this.txt_goodsno.Text + "/" + imageno + ".jpg";
                    }

                    picProductImg.Properties.ZoomPercent = 67;
                    picProductImg.Image = GET(url);

                    //ReportPrint("scanner");
                }
                else
                {
                    this.txt_goodsno.Text = "해당 바코드의 상품이 없습니다.";
                    this.txt_optno.Text = "";
                    this.brand_name.Text = "";
                    this.goods_name.Text = "";
                    this.color.Text = "";
                    this.size.Text = "";
                    barcode = "";

                    picProductImg.Image = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                this.txt_barcode.EditValueChanged -= Txt_barcode_EditValueChanged;
                this.txt_barcode.Text = "";
                this.txt_barcode.EditValueChanged += Txt_barcode_EditValueChanged;
                this.txt_barcode.Focus();
                Cursor.Current = Cursors.Default;
            }
        }

        private void SelectData_ShippingData()
        {
            string url = "";
            string imageno = "";
            int StoreExpectedCnt = 0;
            string color_name = "";
            string size_name = "";
            string mix_name = "";

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                gridViewReceipt.Columns.Clear();

                MySqlParameter[] param =
                {
                    new MySqlParameter("@pSHIPPINGCODE", shippingno)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_ITEMLIST_TO_SHIPPINGCODE", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    foreach (DataRow row in ResultDT.Rows)
                    {
                        color_name = GetCodeName(Common.Method.getStr(row["opt1"]), "KR");
                        if(Common.Method.getStr(row["brand_barcode"]) != "")
                        {
                            color_name = color_name + " (" + row["brand_barcode"] + ")"; 
                        }
                        size_name = GetCodeName(Common.Method.getStr(row["opt2"]), "KR");
                        mix_name = GetMixtureName(Common.Method.getStr(row["mixtureRate"]), "KR");

                        row["opt1"] = color_name;
                        row["opt2"] = size_name;
                        row["mixtureRate"] = mix_name;
                    }

                    if (ResultDT.Rows.Count > 50)
                    {
                        SetGridViewReceiptColumns(false);
                    }
                    else
                    {
                        SetGridViewReceiptColumns(true);

                        ResultDT.Columns.Add("image", typeof(Image));

                        foreach (DataRow row in ResultDT.Rows)
                        {
                            imageno = GetImageNo(Common.Method.getStr(row["goodsno"]), Common.Method.getStr(row["optno"]));

                            if (imageno == "0")
                            {
                                url = "https://d2ncantiuancgo.cloudfront.net/goods/" + Common.Method.getStr(row["goodsno"]) + ".jpg";
                            }
                            else
                            {
                                url = "https://d2ncantiuancgo.cloudfront.net/tgoods/" + Common.Method.getStr(row["goodsno"]) + "/" + imageno + ".jpg";
                            }

                            StoreExpectedCnt = StoreExpectedCnt + Common.Method.getInt(row["ea"]);

                            row["image"] = GET(url);
                            //row["image"] = Image.FromFile(@"C:\Users\jaehoon\Pictures\134105.jpg"); 
                        }
                    }

                    this.lblShipper.Text = Common.Method.getStr(ResultDT.Rows[0]["shippingcomp"]);
                    this.lblTrackingNo.Text = Common.Method.getStr(ResultDT.Rows[0]["shippingcode"]);
                    this.lblBrandName.Text = Common.Method.getStr(ResultDT.Rows[0]["pid"]);
                    this.lblShipmentDate.Text = Common.Method.getStr(ResultDT.Rows[0]["shippingdt"]);
                    this.memoComment.Text = Common.Method.getStr(ResultDT.Rows[0]["memo"]);
                    this.lblStoreExpectedCnt.Text = Common.Method.getStr(StoreExpectedCnt);

                    this.gridControlReceipt.DataSource = null;
                    rowHandle = -1;
                    this.gridControlReceipt.DataSource = ResultDT;

                }
                else
                {
                    this.lblShipper.Text = "";
                    this.lblTrackingNo.Text = "";
                    this.lblBrandName.Text = "";
                    this.lblShipmentDate.Text = "";
                    this.memoComment.Text = "";
                    this.lblStoreExpectedCnt.Text = "0";
                    this.gridControlReceipt.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                txtShippingBarcode.EditValueChanged -= txtShippingBarcode_EditValueChanged;
                //this.txtShippingBarcode.Text = "";
                this.memoComment.Focus();
                txtShippingBarcode.EditValueChanged += txtShippingBarcode_EditValueChanged;
                this.txtShippingBarcode.Focus();

                Cursor.Current = Cursors.Default;
            }
        }

        private string GetMixtureName(string CODENAME, string NATION)
        {
            Hashtable code_Arr = new Hashtable();
            code_Arr["DT"] = "4- dimethylene terephthalate";
            code_Arr["ABS"] = "ABS";
            code_Arr["DPM"] = "DPM";
            code_Arr["PVC"] = "PVC";
            code_Arr["TPU"] = "TPU";
            code_Arr["XPC"] = "X-PAC";
            code_Arr["LT"] = "leather";
            code_Arr["STE"] = "steel";
            code_Arr["RB"] = "Rubber";
            code_Arr["CP"] = "Copper";
            code_Arr["GD"] = "goose down";
            code_Arr["WU"] = "guanaco";
            code_Arr["ME"] = "metallic fibre";
            code_Arr["PM"] = "metallised polyester";
            code_Arr["AF"] = "other fibres";
            code_Arr["NL"] = "nylon";
            code_Arr["NK"] = "nickel";
            code_Arr["BCH"] = "Bamboo charcoal";
            code_Arr["BS"] = "Bamboo subfamily";
            code_Arr["BY"] = "Bamboo yarn";
            code_Arr["MRB"] = "marble";
            code_Arr["DD"] = "duck down";
            code_Arr["CLO"] = "Camellia oil";
            code_Arr["DF"] = "defuser";
            code_Arr["WL"] = "lama";
            code_Arr["RA"] = "ramie";
            code_Arr["CLY"] = "lyocell";
            code_Arr["LCR"] = "lycra";
            code_Arr["LR"] = "Latex rubber";
            code_Arr["RY"] = "Rayon";
            code_Arr["LI"] = "linen";
            code_Arr["HA"] = "hemp";
            code_Arr["MF"] = "Microfiber";
            code_Arr["MSM"] = "Methyl sulfonyl methane";
            code_Arr["MCT"] = "Menthol / Centra Asia Tikka";
            code_Arr["CO"] = "cotton";
            code_Arr["MO"] = "modal (modal cotton)";
            code_Arr["WM"] = "mohair";
            code_Arr["CHC"] = "Charcoal";
            code_Arr["FSS"] = "Fermented spirit siloxane";
            code_Arr["WC"] = "White clay";
            code_Arr["WV"] = "virginwool";
            code_Arr["VVT"] = "velvet";
            code_Arr["NWV"] = "non-woven";
            code_Arr["BR"] = "Brass";
            code_Arr["BD"] = "black dye";
            code_Arr["VY"] = "vinyl";
            code_Arr["WB"] = "beaver";
            code_Arr["VI"] = "viscose (rayon)";
            code_Arr["BPN"] = "Bisphenol";
            code_Arr["WG"] = "vicuna";
            code_Arr["SI"] = "sisal / sisal hemp";
            code_Arr["SST"] = "Surgical Steel ";
            code_Arr["CL"] = "Cow leather";
            code_Arr["SLT"] = "salt";
            code_Arr["SW"] = "Soy Wax";
            code_Arr["WT"] = "otter";
            code_Arr["SKW"] = "Smoky wick";
            code_Arr["SNL"] = "stainless";
            code_Arr["SSL"] = "Stainless steel";
            code_Arr["STL"] = "steal";
            code_Arr["SM"] = "Steel mirror";
            code_Arr["STW"] = "steel wood";
            code_Arr["SP"] = "Span";
            code_Arr["SB"] = "Shea butter";
            code_Arr["ST"] = "Synthrate";
            code_Arr["SLC"] = "silicon";
            code_Arr["SV"] = "silver";
            code_Arr["SE"] = "silk";
            code_Arr["AKO"] = "Argan kernel oil";
            code_Arr["AO"] = "Avocado oil";
            code_Arr["AC"] = "acetate";
            code_Arr["ZA"] = "Zinc alloy";
            code_Arr["ACR"] = "acryl";
            code_Arr["MA"] = "modacrylic";
            code_Arr["AL"] = "aluminum";
            code_Arr["WP"] = "alpaca";
            code_Arr["WA"] = "angora";
            code_Arr["WY"] = "yack";
            code_Arr["EPX"] = "Epoxy";
            code_Arr["EL"] = "elastane";
            code_Arr["OO"] = "Olive oil";
            code_Arr["WOL"] = "wool";
            code_Arr["SLW"] = "Solid wood ";
            code_Arr["WN"] = "Walnut";
            code_Arr["GL"] = "Glass";
            code_Arr["GUV"] = "Glass UV printing";
            code_Arr["GM"] = "Glass mirror";
            code_Arr["ESF"] = "Emulsifier";
            code_Arr["SPL"] = "Silver plating";
            code_Arr["LTT"] = "Leatherette";
            code_Arr["MG"] = "magnet";
            code_Arr["RBR"] = "Red brass";
            code_Arr["PW"] = "Purified water";
            code_Arr["GS"] = "gemstone";
            code_Arr["SL"] = "Shell";
            code_Arr["PP"] = "paper";
            code_Arr["PRL"] = "Pearl";
            code_Arr["NP"] = "Natural plaster";
            code_Arr["NSW"] = "Natural soy wax";
            code_Arr["NV"] = "Natural vermiculite";
            code_Arr["WK"] = "camel hair";
            code_Arr["CC"] = "cacao";
            code_Arr["CM"] = "Cala Min";
            code_Arr["WS"] = "cashmere";
            code_Arr["CD"] = "corduroy";
            code_Arr["CRK"] = "CORK";
            code_Arr["HL"] = "cotton / linen mix";
            code_Arr["CCT"] = "concrete";
            code_Arr["CUP"] = "cupra";
            code_Arr["CRT"] = "crystal";
            code_Arr["CY"] = "Clay";
            code_Arr["CLR"] = "Chlorella";
            code_Arr["TRP"] = "terephthalate";
            code_Arr["TT"] = "Tetron";
            code_Arr["TC"] = "TENCEL";
            code_Arr["TS"] = "Toraitan Silicone";
            code_Arr["TRT"] = "Toritan";
            code_Arr["TA"] = "triacetate";
            code_Arr["TO"] = "Tea tree oil";
            code_Arr["PCT"] = "Powder coated steel";
            code_Arr["PX"] = "Palm wax";
            code_Arr["PPO"] = "Peppermint Oil";
            code_Arr["FT"] = "felt";
            code_Arr["PCL"] = "Polycyclohexane-1";
            code_Arr["PPR"] = "Polypropylene";
            code_Arr["PA"] = "polyamide";
            code_Arr["PAN"] = "polyacrylic";
            code_Arr["PL"] = "polyester";
            code_Arr["PU"] = "polyurethane";
            code_Arr["PCN"] = "polycarbonate";
            code_Arr["FGO"] = "Fragrance Oil";
            code_Arr["PPP"] = "Premium Pearl Powder";
            code_Arr["FO"] = "Flamingo oil";
            code_Arr["PST"] = "plastic";
            code_Arr["PD"] = "Pink dye";
            code_Arr["PC"] = "Pink clay";
            code_Arr["HJ"] = "hanji";
            code_Arr["PR"] = "Pvc Rubber";
            code_Arr["SR"] = "Synthetic resin";
            code_Arr["SPC"] = "Spices";
            code_Arr["PO"] = "Perfume oils";
            code_Arr["TR"] = "mixed fibers / unspecified textile composition";
            code_Arr["HPB"] = "Home Parfum Base";
            code_Arr["JU"] = "jute";
            code_Arr["HAC"] = "Hyaluronic acid";
            code_Arr["PLE"] = "persimmon leaf extract";
            code_Arr["KEX"] = "kaolin extract";
            code_Arr["WD"] = "wood";
            code_Arr["MTG"] = "metal gold";
            code_Arr["MCI"] = "microbial inhibitor";
            code_Arr["NFE"] = "natural fermentation ethanol";
            code_Arr["AR"] = "acrylic resin";
            code_Arr["TTP"] = "titanium post";
            code_Arr["FLV"] = "flavonoid";
            code_Arr["TPE"] = "TPE";
            code_Arr["MC"] = "Matte Coating";
            code_Arr["PRN"] = "pyron";
            code_Arr["SLH"] = "synthetic leather";
            code_Arr["SPX"] = "supplex";
            code_Arr["PTN"] = "Platiunm";
            code_Arr["TN"] = "TIN";
            code_Arr["PLT"] = "plating";

            string mix_name = "";
            string[] mix_name_tmp;

            try
            {
                mix_name_tmp = CODENAME.Split(new char[] { '^' });

                foreach (string item in mix_name_tmp)
                {
                    string[] tmp = item.Split(new char[] { '_' });
                    mix_name += code_Arr[tmp[0].ToUpper()] + tmp[1] + "%";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return mix_name;
        }

        private string GetCodeName(string CODENAME, string NATION)
        {
            Hashtable code_Arr = new Hashtable();
            code_Arr["CL01001"] = "white";
            code_Arr["CL01002"] = "white × blue";
            code_Arr["CL01003"] = "white × emerald green";
            code_Arr["CL01004"] = "white × gold";
            code_Arr["CL01005"] = "white × gray";
            code_Arr["CL01006"] = "white × green";
            code_Arr["CL01007"] = "white × khaki brown";
            code_Arr["CL01008"] = "white × lime";
            code_Arr["CL01009"] = "white × navy";
            code_Arr["CL01010"] = "white × pink";
            code_Arr["CL01011"] = "white × purple";
            code_Arr["CL01012"] = "white × red";
            code_Arr["CL01013"] = "white × silver";
            code_Arr["CL01014"] = "white × white";
            code_Arr["CL01015"] = "white × yellow";
            code_Arr["CL01016"] = "white gold";
            code_Arr["CL01017"] = "white x black";
            code_Arr["CL01018"] = "off white";
            code_Arr["CL01019"] = "light white";
            code_Arr["CL01020"] = "clear";
            code_Arr["CL02001"] = "black";
            code_Arr["CL02002"] = "black × blue";
            code_Arr["CL02003"] = "black × gold";
            code_Arr["CL02004"] = "black × gray";
            code_Arr["CL02005"] = "black × green";
            code_Arr["CL02006"] = "black × khaki";
            code_Arr["CL02007"] = "black × lime";
            code_Arr["CL02008"] = "black × pink";
            code_Arr["CL02009"] = "black × purple";
            code_Arr["CL02010"] = "black × red";
            code_Arr["CL02011"] = "black × silver";
            code_Arr["CL02012"] = "black × white";
            code_Arr["CL02013"] = "black × yellow";
            code_Arr["CL02014"] = "Black Mix";
            code_Arr["CL02015"] = "black x black";
            code_Arr["CL02016"] = "clear black";
            code_Arr["CL02017"] = "dark black";
            code_Arr["CL02018"] = "light black";
            code_Arr["CL03001"] = "heather gray";
            code_Arr["CL03002"] = "gray";
            code_Arr["CL03003"] = "gray × pink";
            code_Arr["CL03004"] = "gray × saxophone blue";
            code_Arr["CL03005"] = "gray × white";
            code_Arr["CL03006"] = "gray camouflage";
            code_Arr["CL03007"] = "smoke gray";
            code_Arr["CL03008"] = "ash gray";
            code_Arr["CL03009"] = "charcoal";
            code_Arr["CL03010"] = "charcoal ash";
            code_Arr["CL03011"] = "charcoal gray";
            code_Arr["CL03012"] = "charcoal gray × black";
            code_Arr["CL03013"] = "charcoal gray × purple";
            code_Arr["CL03014"] = "dark ash";
            code_Arr["CL03015"] = "heather charcoal";
            code_Arr["CL03016"] = "mix gray";
            code_Arr["CL04001"] = "light brown";
            code_Arr["CL04002"] = "clear brown";
            code_Arr["CL04003"] = "brown";
            code_Arr["CL04004"] = "brown × yellow";
            code_Arr["CL04005"] = "brown camouflage";
            code_Arr["CL04006"] = "dark brown";
            code_Arr["CL04007"] = "ash brown";
            code_Arr["CL04008"] = "terracotta";
            code_Arr["CL04009"] = "wood";
            code_Arr["CL04010"] = "woodland";
            code_Arr["CL04011"] = "brick";
            code_Arr["CL04012"] = "tan";
            code_Arr["CL04013"] = "chocolate";
            code_Arr["CL04014"] = "dark wood";
            code_Arr["CL04015"] = "khaki brown";
            code_Arr["CL04016"] = "mocha";
            code_Arr["CL05001"] = "beige";
            code_Arr["CL05002"] = "ivory";
            code_Arr["CL05003"] = "light beige";
            code_Arr["CL05004"] = "dark beige";
            code_Arr["CL05005"] = "sand beige";
            code_Arr["CL05006"] = "pink beige";
            code_Arr["CL05007"] = "grayish beige";
            code_Arr["CL05008"] = "wheat";
            code_Arr["CL05009"] = "cream";
            code_Arr["CL05010"] = "oatmeal";
            code_Arr["CL06001"] = "green";
            code_Arr["CL06002"] = "clear green";
            code_Arr["CL06003"] = "dark green";
            code_Arr["CL06004"] = "moss green";
            code_Arr["CL06005"] = "light green";
            code_Arr["CL06006"] = "green × orange";
            code_Arr["CL06007"] = "green × pink";
            code_Arr["CL06008"] = "green camouflage";
            code_Arr["CL06009"] = "safari green";
            code_Arr["CL06010"] = "sage green";
            code_Arr["CL06011"] = "real tree duck";
            code_Arr["CL06012"] = "dark khaki";
            code_Arr["CL06013"] = "dark olive";
            code_Arr["CL06014"] = "emerald";
            code_Arr["CL06015"] = "khaki";
            code_Arr["CL06016"] = "khaki olive";
            code_Arr["CL06017"] = "mint";
            code_Arr["CL06018"] = "olive";
            code_Arr["CL06019"] = "olive × black";
            code_Arr["CL06020"] = "olive drag";
            code_Arr["CL07001"] = "blue";
            code_Arr["CL07002"] = "blue gray";
            code_Arr["CL07003"] = "blue green";
            code_Arr["CL07004"] = "blue purple";
            code_Arr["CL07005"] = "bluegrass";
            code_Arr["CL07006"] = "grayish blue";
            code_Arr["CL07007"] = "light indigo blue";
            code_Arr["CL07008"] = "indigo × gold";
            code_Arr["CL07009"] = "indigo × silver";
            code_Arr["CL07010"] = "indigo blue";
            code_Arr["CL07011"] = "sax blue";
            code_Arr["CL07012"] = "light blue";
            code_Arr["CL07013"] = "light blue gray";
            code_Arr["CL07014"] = "dark indigo blue";
            code_Arr["CL07015"] = "clear blue";
            code_Arr["CL07016"] = "sky blue";
            code_Arr["CL07017"] = "cobalt blue";
            code_Arr["CL07018"] = "caribbean blue";
            code_Arr["CL07019"] = "emerald blue";
            code_Arr["CL07020"] = "dark blue";
            code_Arr["CL07021"] = "turquoise blue";
            code_Arr["CL07022"] = "royal blue";
            code_Arr["CL07023"] = "navy";
            code_Arr["CL08001"] = "purple";
            code_Arr["CL08002"] = "dark purple";
            code_Arr["CL08003"] = "light purple";
            code_Arr["CL08004"] = "red purple";
            code_Arr["CL08005"] = "violet";
            code_Arr["CL08006"] = "dark lavender";
            code_Arr["CL08007"] = "lilac";
            code_Arr["CL09001"] = "turmeric";
            code_Arr["CL09002"] = "yellow";
            code_Arr["CL09003"] = "yellow × black";
            code_Arr["CL09004"] = "yellow × gray";
            code_Arr["CL09005"] = "yellow × green";
            code_Arr["CL09006"] = "yellow gold";
            code_Arr["CL09007"] = "clear yellow";
            code_Arr["CL09008"] = "pale yellow";
            code_Arr["CL09009"] = "lemon yellow";
            code_Arr["CL09010"] = "fresh yellow";
            code_Arr["CL09011"] = "light yellow";
            code_Arr["CL09012"] = "salmon pink";
            code_Arr["CL09013"] = "smoked pink";
            code_Arr["CL09014"] = "champagne";
            code_Arr["CL09015"] = "lime";
            code_Arr["CL09016"] = "mustard";
            code_Arr["CL10001"] = "baby pink";
            code_Arr["CL10002"] = "pink × yellow";
            code_Arr["CL10003"] = "pink gold";
            code_Arr["CL10004"] = "plum";
            code_Arr["CL10005"] = "cherry blossoms";
            code_Arr["CL10006"] = "cherry pink";
            code_Arr["CL10007"] = "coral";
            code_Arr["CL10008"] = "coral pink";
            code_Arr["CL10009"] = "fuchsia pink";
            code_Arr["CL10010"] = "magenta";
            code_Arr["CL11001"] = "wine";
            code_Arr["CL11002"] = "wine-red";
            code_Arr["CL11003"] = "bordeaux";
            code_Arr["CL11004"] = "burgundy";
            code_Arr["CL11005"] = "cherry red";
            code_Arr["CL11006"] = "clear red";
            code_Arr["CL11007"] = "maroon";
            code_Arr["CL12001"] = "dark orange";
            code_Arr["CL13001"] = "gun meta";
            code_Arr["CL13002"] = "silver";
            code_Arr["CL13003"] = "silver × black";
            code_Arr["CL13004"] = "silver × black cubic";
            code_Arr["CL13005"] = "silver × blue";
            code_Arr["CL13006"] = "silver × gold";
            code_Arr["CL13007"] = "silver × onyx";
            code_Arr["CL13008"] = "silver × smoky quartz";
            code_Arr["CL13009"] = "silver × white quartz";
            code_Arr["CL13010"] = "silver gray";
            code_Arr["CL13011"] = "silver x cubic";
            code_Arr["CL14001"] = "bronze";
            code_Arr["CL14002"] = "gold";
            code_Arr["CL14003"] = "gold × brown";
            code_Arr["CL14004"] = "gold × gray";
            code_Arr["CL15001"] = "check";
            code_Arr["CL15002"] = "colorful";
            code_Arr["CL15003"] = "dot";
            code_Arr["CL15004"] = "dessert duck";
            code_Arr["CL15005"] = "hound tooth";
            code_Arr["CL15006"] = "leopard";
            code_Arr["CL15007"] = "multi";
            code_Arr["CL15008"] = "rainbow";
            code_Arr["CL15009"] = "stripe";
            code_Arr["CL15010"] = "zebra";
            code_Arr["CL15011"] = "camouflage";
            code_Arr["CL11008"] = "red";
            code_Arr["CL10011"] = "pink";
            code_Arr["CL12002"] = "orange";
            code_Arr["CL04017"] = "camel";
            code_Arr["CL07024"] = "dark navy";
            code_Arr["CL03017"] = "light gray";
            code_Arr["CL03018"] = "dark gray";
            code_Arr["CL06021"] = "light khaki";
            code_Arr["CL15012"] = "natural";
            code_Arr["CL08008"] = "lavender";
            code_Arr["CL10012"] = "salmon pink";
            code_Arr["CL10013"] = "smoked pink";
            code_Arr["CL10014"] = "champagne";
            code_Arr["CL13012"] = "antique silver";
            code_Arr["CL14005"] = "antique gold";
            code_Arr["CL14006"] = "pink gold";
            code_Arr["SZ01001"] = "FREE";
            code_Arr["SZ01002"] = "XX-SMALL";
            code_Arr["SZ01003"] = "X-SMALL";
            code_Arr["SZ01004"] = "SMALL";
            code_Arr["SZ01005"] = "MEDIUM";
            code_Arr["SZ01006"] = "LARGE";
            code_Arr["SZ01007"] = "X-LARGE";
            code_Arr["SZ01008"] = "XX-LARGE";
            code_Arr["SZ01009"] = "XXX-LARGE";
            code_Arr["SZ02001"] = "6";
            code_Arr["SZ02002"] = "7";
            code_Arr["SZ02003"] = "8";
            code_Arr["SZ02004"] = "9";
            code_Arr["SZ02005"] = "10";
            code_Arr["SZ02006"] = "11";
            code_Arr["SZ02007"] = "12";
            code_Arr["SZ02008"] = "13";
            code_Arr["SZ02009"] = "14";
            code_Arr["SZ02010"] = "15";
            code_Arr["SZ02011"] = "16";
            code_Arr["SZ02012"] = "17";
            code_Arr["SZ02013"] = "18";
            code_Arr["SZ02014"] = "19";
            code_Arr["SZ02015"] = "20";
            code_Arr["SZ02016"] = "21";
            code_Arr["SZ02017"] = "22";
            code_Arr["SZ02018"] = "23";
            code_Arr["SZ02019"] = "24";
            code_Arr["SZ02020"] = "25";
            code_Arr["SZ02021"] = "26";
            code_Arr["SZ02022"] = "27";
            code_Arr["SZ02023"] = "28";
            code_Arr["SZ02024"] = "29";
            code_Arr["SZ02025"] = "30";
            code_Arr["SZ02026"] = "31";
            code_Arr["SZ02027"] = "32";
            code_Arr["SZ02028"] = "33";
            code_Arr["SZ02029"] = "34";
            code_Arr["SZ02030"] = "35";
            code_Arr["SZ02031"] = "36";
            code_Arr["SZ02032"] = "37";
            code_Arr["SZ02033"] = "38";
            code_Arr["SZ02034"] = "39";
            code_Arr["SZ02035"] = "40";
            code_Arr["SZ02036"] = "41";
            code_Arr["SZ02037"] = "42";
            code_Arr["SZ02038"] = "43";
            code_Arr["SZ02039"] = "44";
            code_Arr["SZ02040"] = "45";
            code_Arr["SZ02041"] = "46";
            code_Arr["SZ02042"] = "47";
            code_Arr["SZ02043"] = "48";
            code_Arr["SZ02044"] = "49";
            code_Arr["SZ02045"] = "50";
            code_Arr["SZ02046"] = "5.5";
            code_Arr["SZ02047"] = "6.5";
            code_Arr["SZ02048"] = "7.5";
            code_Arr["SZ02049"] = "8.5";
            code_Arr["SZ02050"] = "9.5";
            code_Arr["SZ02051"] = "10.5";
            code_Arr["SZ02052"] = "11.5";
            code_Arr["SZ02053"] = "12.5";
            code_Arr["SZ02054"] = "13.5";
            code_Arr["SZ02055"] = "14.5";
            code_Arr["SZ02056"] = "15.5";
            code_Arr["SZ02057"] = "16.5";
            code_Arr["SZ02058"] = "17.5";
            code_Arr["SZ02059"] = "18.5";
            code_Arr["SZ02060"] = "19.5";
            code_Arr["SZ02061"] = "20.5";
            code_Arr["SZ02062"] = "21.5";
            code_Arr["SZ02063"] = "22.5";
            code_Arr["SZ02064"] = "23.5";
            code_Arr["SZ02065"] = "24.5";
            code_Arr["SZ02066"] = "25.5";
            code_Arr["SZ02067"] = "26.5";
            code_Arr["SZ02068"] = "27.5";
            code_Arr["SZ02069"] = "28.5";
            code_Arr["SZ02070"] = "29.5";
            code_Arr["SZ02071"] = "30.5";
            code_Arr["SZ02072"] = "31.5";
            code_Arr["SZ02073"] = "34.5";
            code_Arr["SZ02074"] = "35.5";
            code_Arr["SZ02075"] = "36.5";
            code_Arr["SZ02076"] = "37.5";
            code_Arr["SZ02077"] = "38.5";
            code_Arr["SZ02078"] = "39.5";
            code_Arr["SZ02079"] = "40.5";
            code_Arr["SZ02080"] = "41.5";
            code_Arr["SZ02081"] = "42.5";
            code_Arr["SZ02082"] = "43.5";
            code_Arr["SZ02083"] = "44.5";
            code_Arr["SZ02084"] = "45.5";
            code_Arr["SZ03001"] = "3inch";
            code_Arr["SZ03002"] = "4inch";
            code_Arr["SZ03003"] = "5inch";
            code_Arr["SZ03004"] = "6inch";
            code_Arr["SZ03005"] = "7inch";
            code_Arr["SZ03006"] = "8inch";
            code_Arr["SZ03007"] = "9inch";
            code_Arr["SZ03008"] = "10inch";
            code_Arr["SZ03009"] = "11inch";
            code_Arr["SZ03010"] = "12inch";
            code_Arr["SZ03011"] = "13inch";
            code_Arr["SZ03012"] = "14inch";
            code_Arr["SZ03013"] = "15inch";
            code_Arr["SZ03014"] = "18inch";
            code_Arr["SZ03015"] = "19inch";
            code_Arr["SZ03016"] = "20inch";
            code_Arr["SZ03017"] = "21inch";
            code_Arr["SZ03018"] = "22inch";
            code_Arr["SZ03019"] = "23inch";
            code_Arr["SZ03020"] = "24inch";
            code_Arr["SZ03021"] = "25inch";
            code_Arr["SZ03022"] = "26inch";
            code_Arr["SZ03023"] = "27inch";
            code_Arr["SZ03024"] = "28inch";
            code_Arr["SZ03025"] = "29inch";
            code_Arr["SZ03026"] = "30inch";
            code_Arr["SZ03027"] = "31inch";
            code_Arr["SZ03028"] = "32inch";
            code_Arr["SZ03029"] = "33inch";
            code_Arr["SZ03030"] = "34inch";
            code_Arr["SZ03031"] = "35inch";
            code_Arr["SZ03032"] = "36inch";
            code_Arr["SZ03033"] = "37inch";
            code_Arr["SZ03034"] = "38inch";
            code_Arr["SZ03035"] = "39inch";
            code_Arr["SZ03036"] = "40inch";
            code_Arr["SZ03037"] = "42inch";
            code_Arr["SZ03038"] = "44inch";
            code_Arr["SZ04001"] = "9cm";
            code_Arr["SZ04002"] = "10cm";
            code_Arr["SZ04003"] = "11cm";
            code_Arr["SZ04004"] = "11.5cm";
            code_Arr["SZ04005"] = "12cm";
            code_Arr["SZ04006"] = "12.5cm";
            code_Arr["SZ04007"] = "13cm";
            code_Arr["SZ04008"] = "13.5cm";
            code_Arr["SZ04009"] = "14cm";
            code_Arr["SZ04010"] = "14.5cm";
            code_Arr["SZ04011"] = "15cm";
            code_Arr["SZ04012"] = "15.5cm";
            code_Arr["SZ04013"] = "16cm";
            code_Arr["SZ04014"] = "16.5cm";
            code_Arr["SZ04015"] = "17cm";
            code_Arr["SZ04016"] = "17.5cm";
            code_Arr["SZ04017"] = "18cm";
            code_Arr["SZ04018"] = "18.5cm";
            code_Arr["SZ04019"] = "19cm";
            code_Arr["SZ04020"] = "19.5cm";
            code_Arr["SZ04021"] = "20cm";
            code_Arr["SZ04022"] = "20.5cm";
            code_Arr["SZ04023"] = "21cm";
            code_Arr["SZ04024"] = "22cm";
            code_Arr["SZ04025"] = "23cm";
            code_Arr["SZ04026"] = "24cm";
            code_Arr["SZ04027"] = "25cm";
            code_Arr["SZ04028"] = "26cm";
            code_Arr["SZ04029"] = "27cm";
            code_Arr["SZ04030"] = "28cm";
            code_Arr["SZ04031"] = "29cm";
            code_Arr["SZ04032"] = "30cm";
            code_Arr["SZ04033"] = "31cm";
            code_Arr["SZ04034"] = "32cm";
            code_Arr["SZ04035"] = "35cm";
            code_Arr["SZ04036"] = "36cm";
            code_Arr["SZ04037"] = "37cm";
            code_Arr["SZ04038"] = "38cm";
            code_Arr["SZ04039"] = "39cm";
            code_Arr["SZ04040"] = "40cm";
            code_Arr["SZ04041"] = "41cm";
            code_Arr["SZ04042"] = "42cm";
            code_Arr["SZ04043"] = "43cm";
            code_Arr["SZ04044"] = "44cm";
            code_Arr["SZ04045"] = "45cm";
            code_Arr["SZ04046"] = "46cm";
            code_Arr["SZ04047"] = "48cm";
            code_Arr["SZ04048"] = "49cm";
            code_Arr["SZ04049"] = "50cm";
            code_Arr["SZ05001"] = "70-80cm";
            code_Arr["SZ05002"] = "70-90cm";
            code_Arr["SZ05003"] = "74-80cm";
            code_Arr["SZ05004"] = "80-85cm";
            code_Arr["SZ05005"] = "80-95cm";
            code_Arr["SZ05006"] = "80-90cm";
            code_Arr["SZ05007"] = "85-95cm";
            code_Arr["SZ05008"] = "85-100cm";
            code_Arr["SZ05009"] = "90-95cm";
            code_Arr["SZ05010"] = "90-110cm";
            code_Arr["SZ05011"] = "95-110cm";
            code_Arr["SZ05012"] = "95-115cm";
            code_Arr["SZ05013"] = "95-120cm";
            code_Arr["SZ05014"] = "100-105cm";
            code_Arr["SZ05015"] = "100-110cm";
            code_Arr["SZ05016"] = "100-115cm";
            code_Arr["SZ05017"] = "105-115cm";
            code_Arr["SZ05018"] = "105-120cm";
            code_Arr["SZ05019"] = "110-115cm";
            code_Arr["SZ05020"] = "110-130cm";
            code_Arr["SZ05021"] = "115-135cm";
            code_Arr["SZ05022"] = "120-125cm";
            code_Arr["SZ05023"] = "120-135cm";
            code_Arr["SZ05024"] = "120-130cm";
            code_Arr["SZ05025"] = "120-140cm";
            code_Arr["SZ05026"] = "125-135cm";
            code_Arr["SZ05027"] = "125-140cm";
            code_Arr["SZ05028"] = "130-135cm";
            code_Arr["SZ05029"] = "140-145cm";
            code_Arr["SZ05030"] = "140-155cm";
            code_Arr["SZ05031"] = "150-155cm";
            code_Arr["SZ05032"] = "150-160cm";
            code_Arr["SZ05033"] = "160-165cm";
            code_Arr["SZ05034"] = "170-175cm";
            code_Arr["SZ06001"] = "90";
            code_Arr["SZ06002"] = "95";
            code_Arr["SZ06003"] = "100";
            code_Arr["SZ06004"] = "105";
            code_Arr["SZ06005"] = "110";
            code_Arr["SZ06006"] = "115";
            code_Arr["SZ06007"] = "120";
            code_Arr["SZ06008"] = "125";
            code_Arr["SZ06009"] = "130";
            code_Arr["SZ06010"] = "135";
            code_Arr["SZ06011"] = "140";
            code_Arr["SZ06012"] = "145";
            code_Arr["SZ06013"] = "150";
            code_Arr["SZ06014"] = "155";
            code_Arr["SZ06015"] = "160";
            code_Arr["SZ06016"] = "165";
            code_Arr["SZ06017"] = "170";
            code_Arr["SZ06018"] = "175";
            code_Arr["SZ06019"] = "185";
            code_Arr["SZ06020"] = "195";
            code_Arr["SZ07001"] = "iphone 5 / 5S / SE";
            code_Arr["SZ07002"] = "iphone 6 / 6S";
            code_Arr["SZ07003"] = "iphone 6+";
            code_Arr["SZ07004"] = "iphone 7/8";
            code_Arr["SZ07005"] = "iphone 7+ / 8+";
            code_Arr["SZ07006"] = "iphone X / XS";
            code_Arr["SZ07007"] = "iphone XR";
            code_Arr["SZ07008"] = "iphone XS MAX";
            code_Arr["SZ08001"] = "A65";
            code_Arr["SZ08002"] = "A70";
            code_Arr["SZ08003"] = "A75";
            code_Arr["SZ08004"] = "A80";
            code_Arr["SZ08005"] = "A85";
            code_Arr["SZ08006"] = "A90";
            code_Arr["SZ08007"] = "A95";
            code_Arr["SZ08008"] = "A100";
            code_Arr["SZ08009"] = "A105";
            code_Arr["SZ08010"] = "B65";
            code_Arr["SZ08011"] = "B70";
            code_Arr["SZ08012"] = "B75";
            code_Arr["SZ08013"] = "B80";
            code_Arr["SZ08014"] = "B85";
            code_Arr["SZ08015"] = "B90";
            code_Arr["SZ08016"] = "B95";
            code_Arr["SZ08017"] = "B100";
            code_Arr["SZ08018"] = "B105";
            code_Arr["SZ08019"] = "C65";
            code_Arr["SZ08020"] = "C70";
            code_Arr["SZ08021"] = "C75";
            code_Arr["SZ08022"] = "C80";
            code_Arr["SZ08023"] = "C85";
            code_Arr["SZ08024"] = "C90";
            code_Arr["SZ08025"] = "C95";
            code_Arr["SZ08026"] = "C100";
            code_Arr["SZ08027"] = "C105";
            code_Arr["SZ08028"] = "C110";
            code_Arr["SZ08029"] = "C120";
            code_Arr["SZ08030"] = "D65";
            code_Arr["SZ08031"] = "D70";
            code_Arr["SZ08032"] = "D75";
            code_Arr["SZ08033"] = "D80";
            code_Arr["SZ08034"] = "D85";
            code_Arr["SZ08035"] = "D90";
            code_Arr["SZ08036"] = "D95";
            code_Arr["SZ08037"] = "D100";
            code_Arr["SZ08038"] = "D110";
            code_Arr["SZ08039"] = "D120";
            code_Arr["SZ08040"] = "E65";
            code_Arr["SZ08041"] = "E70";
            code_Arr["SZ08042"] = "E75";
            code_Arr["SZ08043"] = "E80";
            code_Arr["SZ08044"] = "E85";
            code_Arr["SZ08045"] = "E90";
            code_Arr["SZ08046"] = "E95";
            code_Arr["SZ08047"] = "E100";
            code_Arr["SZ08048"] = "E110";
            code_Arr["SZ08049"] = "E120";
            code_Arr["SZ08050"] = "F65";
            code_Arr["SZ08051"] = "F70";
            code_Arr["SZ08052"] = "F75";
            code_Arr["SZ02085"] = "0";
            code_Arr["SZ02086"] = "-1.5";
            code_Arr["SZ02087"] = "-2";
            code_Arr["SZ02088"] = "-2.5";
            code_Arr["SZ02089"] = "-3";
            code_Arr["SZ02090"] = "-3.5";
            code_Arr["SZ02091"] = "-4";
            code_Arr["SZ02092"] = "-4.5";
            code_Arr["SZ02093"] = "-5";
            code_Arr["SZ02094"] = "-6";
            code_Arr["SZ02095"] = "-7";
            code_Arr["SZ04050"] = "21.5cm";
            code_Arr["SZ04051"] = "22.5cm";
            code_Arr["SZ04052"] = "23.5cm";
            code_Arr["SZ04053"] = "24.5cm";
            code_Arr["SZ04054"] = "25.5cm";
            code_Arr["SZ04055"] = "26.5cm";
            code_Arr["SZ04056"] = "27.5cm";
            code_Arr["SZ04057"] = "28.5cm";
            code_Arr["SZ04058"] = "29.5cm";
            code_Arr["SZ04059"] = "30.5cm";
            code_Arr["SZ04060"] = "31.5cm";
            code_Arr["SZ02096"] = "1";
            code_Arr["SZ02097"] = "2";
            code_Arr["SZ02098"] = "3";
            code_Arr["SZ02099"] = "4";
            code_Arr["SZ02100"] = "5";
            code_Arr["SZ02101"] = "2.5";
            code_Arr["SZ02102"] = "3.5";
            code_Arr["SZ02103"] = "4.5";
            code_Arr["SZ04061"] = "47";
            code_Arr["SZ04062"] = "47cm";
            code_Arr["SZ01010"] = "XXXX-LARGE";
            code_Arr["SZ01011"] = "XXXXX-LARGE";
            code_Arr["SZ07009"] = "air pod case";
            code_Arr["SZ02104"] = "0";
            code_Arr["SZ02105"] = "-1";
            code_Arr["SZ02106"] = "-5.5";
            code_Arr["SZ02107"] = "-6.5";
            code_Arr["SZ04063"] = "51cm";
            code_Arr["SZ04064"] = "52cm";
            code_Arr["SZ04065"] = "53cm";
            code_Arr["SZ04066"] = "54cm";
            code_Arr["SZ04067"] = "55cm";
            code_Arr["SZ04068"] = "56cm";
            code_Arr["SZ04069"] = "57cm";
            code_Arr["SZ04070"] = "58cm";
            code_Arr["SZ04071"] = "59cm";
            code_Arr["SZ04072"] = "60cm";

            string code_name = "";

            try
            {
                code_name = (string)code_Arr[CODENAME];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return code_name;
        }

        private string GetImageNo(string GOODSNO, string OPTNO)
        {
            string imageno = "";

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pGOODSNO", GOODSNO),
                    new MySqlParameter("@pOPTNO", OPTNO)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_IMAGE_NO", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    imageno = Common.Method.getStr(ResultDT.Rows[0]["imgno"]);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return imageno;
        }

        Image GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    Image img = Image.FromStream(responseStream);
                    return img;
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText 
                }
                throw;
            }
        }

        private void ReportPrint(string type, int copy = 1)
        {
            string printerName = Common.Method.getStr(lookUpEditPrinter.EditValue);
            Label_Form rpt = new Label_Form();
            rpt.PrinterName = printerName;
            rpt.ShowPrintMarginsWarning = false;

            if (type == "scanner")
            {
                rpt.DataSource = ReportData();
                rpt.Print();
            }
            else
            {
                if(gridViewReceipt.RowCount > 0)
                {
                    rpt.DataSource = ReportData2(copy);
                    rpt.Print();
                }
            }
        }

        private List<Label_Form_Data> ReportData()
        {
            Label_Form_Data item = null;
            List<Label_Form_Data> data = new List<Label_Form_Data>();

            item = new Label_Form_Data();

            item.barcode = barcode;
            item.brand = brand_name.Text;
            item.goods_no = txt_goodsno.Text;
            item.goods_name = goods_name.Text;
            item.color = color.Text;
            item.size = size.Text;
            item.mixturerate = mixturerate.Text;

            data.Add(item);

            return data;
        }

        private List<Label_Form_Data> ReportData2(int ea)
        {
            Label_Form_Data item = null;
            List<Label_Form_Data> data = new List<Label_Form_Data>();

            string barcode = "1" + string.Format("{0:D9}", gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "goodsno")) + string.Format("{0:D3}", gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "optno"));
            string brandName = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "pid"));
            string goodsNo = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "goodsno"));
            string goodsName = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "goodsnm"));
            string color = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "opt1"));
            string size = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "opt2"));
            string mixturerate = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "mixturerate"));
            short numberofcopies = Convert.ToInt16(ea);

            item = new Label_Form_Data();

            item.NumberOfCopies = numberofcopies;
            item.barcode = barcode;
            item.brand = brandName;
            item.goods_no = goodsNo;
            item.goods_name = goodsName;
            item.color = color;
            item.size = size;
            item.mixturerate = mixturerate;

            data.Add(item);

            return data;
        }

        private void btn_Select_Click(object sender, EventArgs e)
        {
            this.txt_barcode.Text = "";
            this.txt_barcode.Focus();
            this.txt_goodsno.Text = "";
            this.txt_optno.Text = "";
            this.picProductImg.Image = null;
            this.brand_name.Text = "";
            this.goods_name.Text = "";
            this.color.Text = "";
            this.size.Text = "";

            barcode = "";


            //string goodsno = "";
            //string optno = "";

            //goodsno = Common.Method.getStr(txt_goodsno.Text);
            //optno = Common.Method.getStr(txt_optno.Text);

            //SelectData();
        }

        private void Txt_barcode_EditValueChanged(object sender, EventArgs e)
        {
            barcode = Common.Method.getStr(txt_barcode.Text);

            if (barcode.Length == 13)
            {
                SelectData();
            }
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if(barcode != "")
            {
                if (barcode.Length == 13)
                {
                    ReportPrint("scanner");
                }
            }

            this.txt_barcode.Focus();
            Cursor.Current = Cursors.Default;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if(this.txtShippingBarcode.Text != "")
            {
                SelectData_ShippingData();
            }
        }

        public void gridPrint(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            ReportPrint("shippingcode");
        }

        private void gridViewReceipt_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            string productNo = "";
            string goodsNo = "";
            string optNo = "";
            string barCode = "";
            string EA = "";
            string STORE_EA = "";
            string type = "";
            string regAdmin = "";
            int selectedRow = -1;
            ea = 1;

            if(e.Column.FieldName == "store" && e.CellValue.ToString() == "입고")
            {
                goodsNo = Common.Method.getStr(gridViewReceipt.GetFocusedRowCellValue("goodsno"));
                optNo = Common.Method.getStr(gridViewReceipt.GetFocusedRowCellValue("optno"));
                barCode = barcode = "1" + string.Format("{0:D9}", gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "goodsno")) + string.Format("{0:D3}", gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "optno"));
                EA = Common.Method.getStr(gridViewReceipt.GetFocusedRowCellValue("ea"));
                STORE_EA = Common.Method.getStr(gridViewReceipt.GetFocusedRowCellValue("store_ea"));
                type = Common.Method.getStr(gridViewReceipt.GetFocusedRowCellValue("type"));
                regAdmin = "gp_amdin";

                selectedRow = gridViewReceipt.FocusedRowHandle;

                StorePopup popUp = new StorePopup(shippingno, goodsNo, optNo, barCode, EA, STORE_EA, type, regAdmin, server);
                popUp.parentForm = this;
                popUp.StartPosition = FormStartPosition.CenterScreen;
                popUp.ShowDialog(this);
                if (popUp.DialogResult == DialogResult.OK)
                {
                    ReportPrint("shippingcode", ea);
                    SelectData_ShippingData();

                    gridViewReceipt.FocusedRowHandle = selectedRow;
                }
                
                rowHandle = e.RowHandle;
            }

            if(e.Column.FieldName == "image")
            {
                productNo = Common.Method.getStr(gridViewReceipt.GetRowCellValue(e.RowHandle, "goodsno"));

                openLink(productNo);
            }
        }

        public void GetEA(string pEA)
        {
            ea = Common.Method.getInt(pEA);
        }

        private void gridViewReceipt_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if(e.Column.FieldName == "store")
            {
                e.Appearance.FontSizeDelta = 30;

                if(e.CellValue.ToString() == "입고완료")
                {
                    e.Appearance.ForeColor = Color.Blue;
                }
            }
            else if((e.Column.FieldName == "type") || (e.Column.FieldName == "goodsno") || (e.Column.FieldName == "goodsnm") || (e.Column.FieldName == "opt1") || (e.Column.FieldName == "opt2") || (e.Column.FieldName == "ea") || (e.Column.FieldName == "store_ea"))
            {
                e.Appearance.FontSizeDelta = 9;
            }
        }

        private void openLink(string productNo)
        {
            Process process = new Process();
            process.StartInfo.FileName = "http://konvini.jp/product/" + productNo;
            process.StartInfo.Verb = "open";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            try
            {
                process.Start();
            }
            catch { }
        }

        private void picProductImg_Click(object sender, EventArgs e)
        {
            openLink(txt_goodsno.Text);
        }

        private void txtShippingBarcode_EditValueChanged(object sender, EventArgs e)
        {
            if(txtShippingBarcode.Text.Length > 0)
            {
                this.txtShippingBarcode.EditValueChanged -= txtShippingBarcode_EditValueChanged;
                this.txtShippingBarcode.Text = this.txtShippingBarcode.Text.Replace("-", "");
                this.txtShippingBarcode.Refresh();
                this.txtShippingBarcode.EditValueChanged += txtShippingBarcode_EditValueChanged;
            }

            if (txtShippingBarcode.Text.Length >= 10)
            {
                shippingno = this.txtShippingBarcode.Text;
                SelectData_ShippingData();
            }
        }

        private void txt_barcode_Enter(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (this.txtShippingBarcode.Text.Length == 13)
            {
                ReportPrint("scanner");
            }

            this.txt_barcode.Focus();
            Cursor.Current = Cursors.Default;
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if(xtraTabControl1.SelectedTabPageIndex == 0)
            {
                txtShippingBarcode.Focus();
            }
            else
            {
                txt_barcode.Focus();
            }
        }

        private void txtShippingBarcode_Leave(object sender, EventArgs e)
        {
            string controlName = GetFocusControl();

            if (controlName != "groupControl3")
            {
                txtShippingBarcode.Focus();
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Winapi)]
        internal static extern IntPtr GetFocus();

        private string GetFocusControl()
        {
            Control focusControl = null;
            IntPtr focusHandle = GetFocus();
            if (focusHandle != IntPtr.Zero)
                focusControl = Control.FromHandle(focusHandle);
            if (focusControl.Name.ToString().Length == 0)
                return focusControl.Parent.Parent.Name.ToString();
            else
                return focusControl.Name.ToString();
        }

        private void txtShippingBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(Char.IsDigit(e.KeyChar)) && e.KeyChar != 8 && e.KeyChar != 22) //8:백스페이스
            {
                e.Handled = true;
            }
        }

        private void SetMemo()
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pSHIPPINGNO", shippingno),
                    new MySqlParameter("@pMEMO", this.memoComment.Text)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_SET_MEMO", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    if(Common.Method.getStr(ResultDT.Rows[0]["RESULT"]) != "ERROR")
                    {
                        MessageBox.Show("저장 완료", "알림", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("오류", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetOrder()
        {
            Cursor.Current = Cursors.WaitCursor;

            string goodsno = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "goodsno"));
            string optno = Common.Method.getStr(gridViewReceipt.GetRowCellValue(gridViewReceipt.FocusedRowHandle, "optno"));

            try
            {
                MySqlParameter[] param =
                {
                    new MySqlParameter("@pSHIPPINGCODE", shippingno),
                    new MySqlParameter("@pGOODSNO", goodsno),
                    new MySqlParameter("@pOPTNO", optno)
                };

                DataTable ResultDT = Common.Method.Sql_Sp_GetValue("USP_GET_ORDER_TO_SHIPPINGCODE", param, server);

                if (ResultDT.Rows.Count > 0)
                {
                    gridControlOrder.DataSource = ResultDT;
                }
                else
                {
                    gridControlOrder.DataSource = null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "경고!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void simpleButtonMemo_Click(object sender, EventArgs e)
        {
            if(shippingno != "")
            {
                SetMemo();
            }
        }

        private void gridViewReceipt_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SetOrder();
        }

        private void SetupPrinterSet()
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("CODE");
            DT.Columns.Add("CODE_NAME");

            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                DataRow DR = DT.NewRow();
                DR["CODE"] = printer;
                DR["CODE_NAME"] = printer;
                DT.Rows.Add(DR);
            }

            Common.Method.SetLookupEdit(lookUpEditPrinter, DT);
        }
    }
}
