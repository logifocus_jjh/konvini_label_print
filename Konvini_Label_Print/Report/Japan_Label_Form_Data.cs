﻿namespace Konvini_Label_Print.Report
{
    class Japan_Label_Form_Data
    {
        public string brand { get; set; }
        public string goodsName { get; set; }
        public string goodsNo { get; set; }
        public string barcode { get; set; }
        public string color { get; set; }
        public string size { get; set; }
        public string mixturerate { get; set; }
        public int price { get; set; }
        public int tax_price { get; set; }
        public short NumberOfCopies { get; set; }
    }
}
