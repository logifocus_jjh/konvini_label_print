﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konvini_Label_Print.Report
{
    class Label_Form_Data
    {
        public string brand { get; set; }
        public string goods_no { get; set; }
        public string goods_name { get; set; }
        public string color { get; set; }
        public string size { get; set; }
        public string barcode { get; set; }
        public string mixturerate { get; set; }
        public short NumberOfCopies { get; set; }
    }
}
