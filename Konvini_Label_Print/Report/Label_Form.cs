﻿using DevExpress.XtraReports.UI;

namespace Konvini_Label_Print.Report
{
    public partial class Label_Form : DevExpress.XtraReports.UI.XtraReport
    {
        public Label_Form()
        {
            InitializeComponent();
        }

        private void XtraReport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            Label_Form_Data item = (Label_Form_Data)GetCurrentRow();

            this.barcode.Text = Common.Method.getStr(item.barcode);

            XtraReport report = (XtraReport)sender;
            report.PrintingSystem.StartPrint += PrintingSystem_StartPrint;
        }

        private void PrintingSystem_StartPrint(object sender, DevExpress.XtraPrinting.PrintDocumentEventArgs e)
        {
            Label_Form_Data item = (Label_Form_Data)GetCurrentRow();

            e.PrintDocument.PrinterSettings.Copies = item.NumberOfCopies;
        }
    }
}
